var _ = require('underscore');
var express = require('express');
var bodyParser = require("body-parser");
var bunyan = require('bunyan');

var request = require('../middlewares/request');
var response = require('../middlewares/response');
var logger = require('../middlewares/logger');

var schema = require('../models/schema/schema');
var lookup = require('../models/schema/lookup');

// init
var log = bunyan.createLogger({
  name: 'business',
  level: 'debug'
});

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
var server = app.listen(8082, function () {
  var host = server.address().address;
  var port = server.address().port;

  log.info("Meta data REST interface listening on http://%s:%s", host, port);
});

app.get('/rule', function (req, res) {
  var tenant = req.headers['soap-tenant'];
  
  // Return list of rules with namespace and version
});

app.get('/rule/:path', function (req, res) {
  var tenant = req.headers['soap-tenant'];
  var version = req.headers['soap-version'];

  var type = req.params.type;
  
  // Return the rule
});

app.post('/rule/:path', function (req, res) {
  var tenant = req.headers['soap-tenant'];
  var version = req.headers['soap-version'];

  var type = req.params.type;
  
  // Create/update rule
});

app.get('/schema/:path', function (req, res) {
  logger.start(log, req);
  
  request.required(req, {'headers': ['sopa-tenant', 'sopa-namespace'], 'params': ['path']}).then(function(params) {
    if (!_.isUndefined(req.headers['sopa-version'])) params.params['sopa-version'] = req.headers['sopa-version'];

    schema.getByPath(params).then(function(result) {
      res.statusCode = result.statusCode;
      res.send(result.entity);

      logger.success(log, req, result);
    }, function(error) {
      response.sendError(res, error);
      logger.error(log, req, error);
    });
  }, function(error) {
    response.sendError(res, error);
    logger.error(log, req, error);
  });  
});

// List all schemas
app.get('/schema', function (req, res) {
  logger.start(log, req);
  
  request.required(req, {'headers': ['sopa-tenant'], 'params': []}).then(function(params) {
    if (!_.isUndefined(req.headers['sopa-version'])) params.params['sopa-version'] = req.headers['sopa-version'];
    if (!_.isUndefined(req.headers['sopa-namespace'])) params.params['sopa-namespace'] = req.headers['sopa-namespace'];
    
    schema.list(params).then(function(result) {
      res.statusCode = result.statusCode;
      res.send(result.entity);

      logger.success(log, req, result);
    }, function(error) {
      response.sendError(res, error);
      logger.error(log, req, error);
    });
  }, function(error) {
    response.sendError(res, error);
    logger.error(log, req, error);
  });  
});

app.post('/schema/:path', function (req, res) {
  logger.start(log, req);
  
  request.required(req, {'headers': ['sopa-tenant', 'sopa-version', 'sopa-namespace'], 'params': ['path']}).then(function(params) {
    schema.update(params, req.body).then(function(result) {

      res.statusCode = result.statusCode;
      
//      response.sendLocation(res, params.type, result.uuid);

      logger.success(log, req, result);
    }, function(error) {
      response.sendError(res, error);
      logger.error(log, req, error);
    });
  }, function(error) {
    response.sendError(res, error);
    logger.error(log, req, error);
  });  
});

app.get('/lookup', function (req, res) {
  logger.start(log, req);

  request.required(req, {'headers': ['sopa-tenant'], 'params': []}).then(function(params) {
    lookup.get(params).then(function(result) {

      res.statusCode = result.statusCode;
      res.send(JSON.stringify(result.entity, null, 2));

      logger.success(log, req, result);
    }, function(error) {
      response.sendError(res, error);
      logger.error(log, req, error);
    });
  }, function(error) {
    response.sendError(res, error);
    logger.error(log, req, error);
  });
});

app.get('/:type/lookup', function (req, res) {
  logger.start(log, req);

  request.required(req, {'headers': ['sopa-tenant'], 'params': ['type']}).then(function(params) {
    lookup.get(params).then(function(result) {

      res.statusCode = result.statusCode;
      res.send(JSON.stringify(result.entity, null, 2));

      logger.success(log, req, result);
    }, function(error) {
      response.sendError(res, error);
      logger.error(log, req, error);
    });
  }, function(error) {
    response.sendError(res, error);
    logger.error(log, req, error);
  });
});

app.post('/lookup/:path', function (req, res) {
  logger.start(log, req);
  
  request.required(req, {'headers': ['sopa-tenant'], 'params': ['path']}).then(function(params) {
    lookup.update(params).then(function(result) {

      res.statusCode = result.statusCode;

      logger.success(log, req, result);
    }, function(error) {
      response.sendError(res, error);
      logger.error(log, req, error);
    });
  }, function(error) {
    response.sendError(res, error);
    logger.error(log, req, error);
  });
});

app.post('/:type/lookup/:schema/:rules/:path', function (req, res) {
  logger.start(log, req);
  
  request.required(req, {'headers': ['sopa-tenant'], 'params': ['type', 'schema', 'rules']}).then(function(params) {
    lookup.update(params).then(function(result) {

      res.statusCode = result.statusCode;

      logger.success(log, req, result);
    }, function(error) {
      response.sendError(res, error);
      logger.error(log, req, error);
    });
  }, function(error) {
    response.sendError(res, error);
    logger.error(log, req, error);
  });
});