var _ = require('underscore');
var express = require('express');
var bodyParser = require("body-parser");
var bunyan = require('bunyan');

var amqp = require('../helpers/amqp');
var db = require('../helpers/mongodb');
var mysql = require('../helpers/mysql');
var themis = require('../helpers/themis');

var request = require('../middlewares/request');
var response = require('../middlewares/response');
var logger = require('../middlewares/logger');

var create = require('../models/business/create');
var _import = require('../models/business/import');
var update = require('../models/business/update');
var passivate = require('../models/business/passivate');
var remove = require('../models/business/remove');
var _delete = require('../models/business/delete');
var trace = require('../models/business/trace');
var history = require('../models/business/history');
var evolution = require('../models/business/evolution');
var get = require('../models/business/get');
var search = require('../models/business/search');


// init
var log = bunyan.createLogger({
  name: 'business',
  level: 'debug'
});

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
var server = app.listen(8081, function () {
  var host = server.address().address;
  var port = server.address().port;

  log.info("Business object REST interface listening on http://%s:%s", host, port);
});

/**
 * @api {post} /:type/create Create
 * @apiName create
 * @apiGroup business
 * @apiVersion 1.0.0
 * @apiDescription
 * Creates a new object of the given type. For successful creation, the posted data has to validate against the schema and rules for the type.
 *
 * @apiExample {curl} curl
 *     curl -i http://localhost/person/create 
 * @apiExample {wget} wget
 *     curl -i http://localhost/person/create 
 * @apiExample {javascript} javascript
 *     curl -i http://localhost/person/create 
 * @apiExample {java} java
 *     curl -i http://localhost/person/create 
 * @apiExample {.net} .net
 *     curl -i http://localhost/person/create 
 *
 * @apiSampleRequest  http://localhost:8081/:type/create
 *
 * @apiHeader         {String}  sopa-version        Version of schema/rules to use for validation
 * @apiHeader         {String}  sopa-namespace      Namespace of schema/rules to use. This overrules lookup path for object type and tenant
 * @apiHeaderExample  {String}  sopa-version
 *                              1.2
 * @apiHeaderExample  {String}  sopa-namespace
 *                              kl.dk
 *
 * @apiParam          {String}  type                Type of object eg. Person, Unit, Function, Class ...
 * @apiParam          {json}    object              Create JSON data for Person, Unit, Function, Class ...
 *
 * @apiSuccess (201)  {header}  uuid                Uuid of new object in response location header.
 * @apiSuccess (201)  {json}    paths               List of services that can be called on the created object.
 * @apiSuccessExample {header}  location:
 *                              <uuid>
 * @apiSuccessExample {json}    Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *       "firstname": "John",
 *       "lastname": "Doe"
 *     } 
 */
app.post('/:type/create', function (req, res) {
  logger.start(log, req);
  
  request.required(req, {'headers': ['sopa-tenant', 'sopa-version', 'sopa-namespace'], 'params': ['type']}).then(function(params) {
    create.execute(params, req.body).then(function(result) {

      res.statusCode = result.statusCode;
      response.sendLocation(res, params.type, result.uuid);

      logger.success(log, req, result);
    }, function(error) {
      response.sendError(res, error);
      logger.error(log, req, error);
    });
  }, function(error) {
    response.sendError(res, error);
    logger.error(log, req, error);
  });
});

/**
 * @api {put} /:type/import Import
 * @apiName import
 * @apiGroup business
 * @apiVersion 1.0.0
 * @apiDescription
 * bla bla
 *
 * @apiExample {curl} Example usage:
 *     curl -i http://localhost/person/create 
 *
 * @apiSampleRequest  http://localhost:8081/:type/create
 *
 * @apiHeader         {String}  sopa-version        Version of schema/rules to use for validation
 * @apiHeader         {String}  sopa-namespace      Namespace of schema/rules to use. This overrules lookup path for object type and tenant
 * @apiHeaderExample  {String}  sopa-version
 *                              1.0.2
 * @apiHeaderExample  {String}  sopa-namespace
 *                              kl
 *
 * @apiParam          {String}  type                Type of object eg. Person, Unit, Function, Class ...
 * @apiParam          {json}    object              Create JSON data for Person, Unit, Function, Class ...
 *
 * @apiSuccess (201)  {header}  uuid                Uuid of new object in response location header.
 * @apiSuccess (201)  {json}    paths               List of services that can be called on the created object.
 * @apiSuccessExample {header}  location:
 *                              /person/trace/
 * @apiSuccessExample {json}    Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *       "firstname": "John",
 *       "lastname": "Doe"
 *     } 
 */
app.put('/:type/import', function (req, res) {
  logger.start(log, req);

  request.required(req, {'headers': ['sopa-tenant', 'sopa-version', 'sopa-namespace'], 'params': ['type']}).then(function(params) {
    _import.execute(params, req.body).then(function(result) {
      res.statusCode = result.statusCode;
      response.sendLocation(res, params.type, result.uuid);

      logger.success(log, req, result);
    }, function(error) {
      response.sendError(res, error);
      logger.error(log, req, error);
    });
  }, function(error) {
    response.sendError(res, error);
    logger.error(log, req, error);
  });
});

/**
 * @api {patch} /:type/update/:uuid Update
 * @apiName update
 * @apiGroup business
 * @apiVersion 1.0.0
 * @apiDescription
 * bla bla
 *
 * @apiExample {curl} Example usage:
 *     curl -i http://localhost/person/create 
 *
 * @apiSampleRequest  http://localhost:8081/:type/create
 *
 * @apiHeader         {String}  sopa-version        Version of schema/rules to use for validation
 * @apiHeader         {String}  sopa-namespace      Namespace of schema/rules to use. This overrules lookup path for object type and tenant
 * @apiHeaderExample  {String}  sopa-version
 *                              1.0.2
 * @apiHeaderExample  {String}  sopa-namespace
 *                              kl
 *
 * @apiParam          {String}  type                Type of object eg. Person, Unit, Function, Class ...
 * @apiParam          {json}    object              Create JSON data for Person, Unit, Function, Class ...
 *
 * @apiSuccess (201)  {header}  uuid                Uuid of new object in response location header.
 * @apiSuccess (201)  {json}    paths               List of services that can be called on the created object.
 * @apiSuccessExample {header}  location:
 *                              /person/trace/
 * @apiSuccessExample {json}    Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *       "firstname": "John",
 *       "lastname": "Doe"
 *     } 
 */
app.patch('/:type/update/:uuid', function (req, res) {
  logger.start(log, req);

  request.required(req, {'headers': ['sopa-tenant', 'sopa-version', 'sopa-namespace'], 'params': ['type', 'uuid']}).then(function(params) {
    update.execute(params, req.body).then(function(result) {

      res.statusCode = result.statusCode;
      res.send();

      logger.success(log, req, result);
    }, function(error) {
      response.sendError(res, error);
      logger.error(log, req, error);
    });
  }, function(error) {
    response.sendError(res, error);
    logger.error(log, req, error);
  });
});

/**
 * @api {patch} /:type/discard/:uuid Discard
 * @apiName discard
 * @apiGroup business
 * @apiVersion 1.0.0
 * @apiDescription
 * bla bla
 *
 * @apiExample {curl} Example usage:
 *     curl -i http://localhost/person/create 
 *
 * @apiSampleRequest  http://localhost:8081/:type/create
 *
 * @apiHeader         {String}  sopa-version        Version of schema/rules to use for validation
 * @apiHeader         {String}  sopa-namespace      Namespace of schema/rules to use. This overrules lookup path for object type and tenant
 * @apiHeaderExample  {String}  sopa-version
 *                              1.0.2
 * @apiHeaderExample  {String}  sopa-namespace
 *                              kl
 *
 * @apiParam          {String}  type                Type of object eg. Person, Unit, Function, Class ...
 * @apiParam          {json}    object              Create JSON data for Person, Unit, Function, Class ...
 *
 * @apiSuccess (201)  {header}  uuid                Uuid of new object in response location header.
 * @apiSuccess (201)  {json}    paths               List of services that can be called on the created object.
 * @apiSuccessExample {header}  location:
 *                              /person/trace/
 * @apiSuccessExample {json}    Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *       "firstname": "John",
 *       "lastname": "Doe"
 *     } 
 */
app.patch('/:type/discard/:uuid', function (req, res) {
  logger.start(log, req);

  request.required(req, {'headers': ['sopa-tenant', 'sopa-version', 'sopa-namespace'], 'params': ['type', 'uuid']}).then(function(params) {
    discard.execute(params, req.body).then(function(result) {

      res.statusCode = result.statusCode;
      res.send();

      logger.success(log, req, result);
    }, function(error) {
      response.sendError(res, error);
      logger.error(log, req, error);
    });
  }, function(error) {
    response.sendError(res, error);
    logger.error(log, req, error);
  });
});

/**
 * @api {patch} /:type/passivate/:uuid Passivate
 * @apiName passivate
 * @apiGroup business
 * @apiVersion 1.0.0
 * @apiDescription
 * bla bla
 *
 * @apiExample {curl} Example usage:
 *     curl -i http://localhost/person/create 
 *
 * @apiSampleRequest  http://localhost:8081/:type/create
 *
 * @apiHeader         {String}  sopa-version        Version of schema/rules to use for validation
 * @apiHeader         {String}  sopa-namespace      Namespace of schema/rules to use. This overrules lookup path for object type and tenant
 * @apiHeaderExample  {String}  sopa-version
 *                              1.0.2
 * @apiHeaderExample  {String}  sopa-namespace
 *                              kl
 *
 * @apiParam          {String}  type                Type of object eg. Person, Unit, Function, Class ...
 * @apiParam          {json}    object              Create JSON data for Person, Unit, Function, Class ...
 *
 * @apiSuccess (201)  {header}  uuid                Uuid of new object in response location header.
 * @apiSuccess (201)  {json}    paths               List of services that can be called on the created object.
 * @apiSuccessExample {header}  location:
 *                              /person/trace/
 * @apiSuccessExample {json}    Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *       "firstname": "John",
 *       "lastname": "Doe"
 *     } 
 */
app.patch('/:type/passivate/:uuid', function (req, res) {
  logger.start(log, req);

  request.required(req, {'headers': ['sopa-tenant'], 'params': ['type', 'uuid']}).then(function(params) {
    passivate.execute(params).then(function(result) {

      res.statusCode = result.statusCode;
      res.send();

      logger.success(log, req, result);
    }, function(error) {
      response.sendError(res, error);
      logger.error(log, req, error);
    });
  }, function(error) {
    response.sendError(res, error);
    logger.error(log, req, error);
  });
});

/**
 * @api {patch} /:type/remove/:uuid Remove
 * @apiName remove
 * @apiGroup business
 * @apiVersion 1.0.0
 * @apiDescription
 * Removes the object, but marking its lifecycle as removed. The resource can still be retrived using the trace operation. To erase the resource so that
 * it no longer can be retrived, use delete.
 *
 * @apiExample {curl} Example usage:
 *     curl -i http://localhost/person/create 
 *
 * @apiSampleRequest  http://localhost:8081/:type/create
 *
 * @apiHeader         {String}  sopa-version        Version of schema/rules to use for validation
 * @apiHeader         {String}  sopa-namespace      Namespace of schema/rules to use. This overrules lookup path for object type and tenant
 * @apiHeaderExample  {String}  sopa-version
 *                              1.0.2
 * @apiHeaderExample  {String}  sopa-namespace
 *                              kl
 *
 * @apiParam          {String}  type                Type of object eg. Person, Unit, Function, Class ...
 * @apiParam          {json}    object              Create JSON data for Person, Unit, Function, Class ...
 *
 * @apiSuccess (201)  {header}  uuid                Uuid of new object in response location header.
 * @apiSuccess (201)  {json}    paths               List of services that can be called on the created object.
 * @apiSuccessExample {header}  location:
 *                              /person/trace/
 * @apiSuccessExample {json}    Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *       "firstname": "John",
 *       "lastname": "Doe"
 *     } 
 */
app.patch('/:type/remove/:uuid', function (req, res) {
  logger.start(log, req);

  request.required(req, {'headers': ['sopa-tenant'], 'params': ['type', 'uuid']}).then(function(params) {
    remove.execute(params, req.body).then(function(result) {

      res.statusCode = result.statusCode;
      res.send();

      logger.success(log, req, result);
    }, function(error) {
      response.sendError(res, error);
      logger.error(log, req, error);
    });
  }, function(error) {
    response.sendError(res, error);
    logger.error(log, req, error);
  });
});

/**
 * @api {delete} /:type/delete/:uuid Delete
 * @apiName delete
 * @apiGroup business
 * @apiVersion 1.0.0
 * @apiDescription
 * Removes the object, but marking its lifecycle as removed. The resource can still be retrived using the trace operation. To erase the resource so that
 * it no longer can be retrived, use delete.
 *
 * @apiExample {curl} Example usage:
 *     curl -i http://localhost/person/create 
 *
 * @apiSampleRequest  http://localhost:8081/:type/create
 *
 * @apiHeader         {String}  sopa-version        Version of schema/rules to use for validation
 * @apiHeader         {String}  sopa-namespace      Namespace of schema/rules to use. This overrules lookup path for object type and tenant
 * @apiHeaderExample  {String}  sopa-version
 *                              1.0.2
 * @apiHeaderExample  {String}  sopa-namespace
 *                              kl
 *
 * @apiParam          {String}  type                Type of object eg. Person, Unit, Function, Class ...
 * @apiParam          {json}    object              Create JSON data for Person, Unit, Function, Class ...
 *
 * @apiSuccess (201)  {header}  uuid                Uuid of new object in response location header.
 * @apiSuccess (201)  {json}    paths               List of services that can be called on the created object.
 * @apiSuccessExample {header}  location:
 *                              /person/trace/
 * @apiSuccessExample {json}    Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *       "firstname": "John",
 *       "lastname": "Doe"
 *     } 
 */
app.delete('/:type/delete/:uuid', function (req, res) {
  logger.start(log, req);

  request.required(req, {'headers': ['sopa-tenant'], 'params': ['type', 'uuid']}).then(function(params) {
    _delete.execute(params).then(function(result) {

      res.statusCode = result.statusCode;
      res.send();

      logger.success(log, req, result);
    }, function(error) {
      response.sendError(res, error);
      logger.error(log, req, error);
    });
  }, function(error) {
    response.sendError(res, error);
    logger.error(log, req, error);
  });
});

/**
 * @api {get} /:type/trace/:uuid Trace
 * @apiName trace
 * @apiGroup business
 * @apiVersion 1.0.0
 * @apiDescription
 * bla bla
 *
 * @apiExample {curl} Example usage:
 *     curl -i http://localhost/person/create 
 *
 * @apiSampleRequest  http://localhost:8081/:type/create
 *
 * @apiHeader         {String}  sopa-version        Version of schema/rules to use for validation
 * @apiHeader         {String}  sopa-namespace      Namespace of schema/rules to use. This overrules lookup path for object type and tenant
 * @apiHeaderExample  {String}  sopa-version
 *                              1.0.2
 * @apiHeaderExample  {String}  sopa-namespace
 *                              kl
 *
 * @apiParam          {String}  type                Type of object eg. Person, Unit, Function, Class ...
 * @apiParam          {json}    object              Create JSON data for Person, Unit, Function, Class ...
 *
 * @apiSuccess (201)  {header}  uuid                Uuid of new object in response location header.
 * @apiSuccess (201)  {json}    paths               List of services that can be called on the created object.
 * @apiSuccessExample {header}  location:
 *                              /person/trace/
 * @apiSuccessExample {json}    Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *       "firstname": "John",
 *       "lastname": "Doe"
 *     } 
 */
app.get('/:type/trace/:uuid', function (req, res) {
  logger.start(log, req);

  request.required(req, {'headers': ['sopa-tenant'], 'params': ['type', 'uuid']}).then(function(params) {
    trace.execute(params).then(function(result) {

      res.statusCode = result.statusCode;
      res.send(JSON.stringify(result.entity, null, 2));

      logger.success(log, req, result);
    }, function(error) {
      response.sendError(res, error);
      logger.error(log, req, error);
    });
  }, function(error) {
    response.sendError(res, error);
    logger.error(log, req, error);
  });
});

/**
 * @api {get} /:type/history/:uuid/:registrationTime History
 * @apiName history
 * @apiGroup business
 * @apiVersion 1.0.0
 * @apiDescription
 * bla bla
 *
 * @apiExample {curl} Example usage:
 *     curl -i http://localhost/person/create 
 *
 * @apiSampleRequest  http://localhost:8081/:type/create
 *
 * @apiHeader         {String}  sopa-version        Version of schema/rules to use for validation
 * @apiHeader         {String}  sopa-namespace      Namespace of schema/rules to use. This overrules lookup path for object type and tenant
 * @apiHeaderExample  {String}  sopa-version
 *                              1.0
 * @apiHeaderExample  {String}  sopa-namespace
 *                              kl
 *
 * @apiParam          {String}  type                Type of object eg. Person, Unit, Function, Class ...
 * @apiParam          {json}    object              Create JSON data for Person, Unit, Function, Class ...
 *
 * @apiSuccess (201)  {header}  uuid                Uuid of new object in response location header.
 * @apiSuccess (201)  {json}    paths               List of services that can be called on the created object.
 * @apiSuccessExample {header}  location:
 *                              /person/trace/
 * @apiSuccessExample {json}    Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *       "firstname": "John",
 *       "lastname": "Doe"
 *     } 
 */
app.get('/:type/history/:uuid/:registrationTime', function (req, res) {
  logger.start(log, req);

  request.required(req, {'headers': ['sopa-tenant'], 'params': ['type', 'uuid', 'registrationTime']}).then(function(params) {
    trace.execute(params).then(function(result) {

      res.statusCode = result.statusCode;
      res.send();

      logger.success(log, req, result);
    }, function(error) {
      response.sendError(res, error);
      logger.error(log, req, error);
    });
  }, function(error) {
    response.sendError(res, error);
    logger.error(log, req, error);
  });
});

/**
 * @api {get} /:type/evolution/:uuid/:validityTime Evolution
 * @apiName evolution
 * @apiGroup business
 * @apiVersion 1.0.0
 * @apiDescription
 * bla bla
 *
 * @apiExample {curl} Example usage:
 *     curl -i http://localhost/person/create 
 *
 * @apiSampleRequest  http://localhost:8081/:type/create
 *
 * @apiHeader         {String}  sopa-version        Version of schema/rules to use for validation
 * @apiHeader         {String}  sopa-namespace      Namespace of schema/rules to use. This overrules lookup path for object type and tenant
 * @apiHeaderExample  {String}  sopa-version
 *                              1.0.2
 * @apiHeaderExample  {String}  sopa-namespace
 *                              kl
 *
 * @apiParam          {String}  type                Type of object eg. Person, Unit, Function, Class ...
 * @apiParam          {json}    object              Create JSON data for Person, Unit, Function, Class ...
 *
 * @apiSuccess (201)  {header}  uuid                Uuid of new object in response location header.
 * @apiSuccess (201)  {json}    paths               List of services that can be called on the created object.
 * @apiSuccessExample {header}  location:
 *                              /person/trace/
 * @apiSuccessExample {json}    Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *       "firstname": "John",
 *       "lastname": "Doe"
 *     } 
 */
app.get('/:type/evolution/:uuid/:validityTime', function (req, res) {
  logger.start(log, req);

  request.required(req, {'headers': ['sopa-tenant'], 'params': ['type', 'uuid', 'validityTime']}).then(function(params) {
    evolution.execute(params).then(function(result) {

      res.statusCode = result.statusCode;
      res.send();

      logger.success(log, req, result);
    }, function(error) {
      response.sendError(res, error);
      logger.error(log, req, error);
    });
  }, function(error) {
    response.sendError(res, error);
    logger.error(log, req, error);
  });
});

/**
 * @api {get} /:type/get/:uuid Get
 * @apiName get
 * @apiGroup business
 * @apiVersion 1.0.0
 * @apiDescription
 * bla bla
 *
 * @apiExample {curl} Example usage:
 *     curl -i http://localhost/person/create 
 *
 * @apiSampleRequest  http://localhost:8081/:type/create
 *
 * @apiHeader         {String}  sopa-version        Version of schema/rules to use for validation
 * @apiHeader         {String}  sopa-namespace      Namespace of schema/rules to use. This overrules lookup path for object type and tenant
 * @apiHeaderExample  {String}  sopa-version
 *                              1.0.2
 * @apiHeaderExample  {String}  sopa-namespace
 *                              kl
 *
 * @apiParam          {String}  type                Type of object eg. Person, Unit, Function, Class ...
 * @apiParam          {json}    object              Create JSON data for Person, Unit, Function, Class ...
 *
 * @apiSuccess (201)  {header}  uuid                Uuid of new object in response location header.
 * @apiSuccess (201)  {json}    paths               List of services that can be called on the created object.
 * @apiSuccessExample {header}  location:
 *                              /person/trace/
 * @apiSuccessExample {json}    Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *       "firstname": "John",
 *       "lastname": "Doe"
 *     } 
 */
app.get('/:type/get/:uuid', function (req, res) {
  logger.start(log, req);

  request.required(req, {'headers': ['sopa-tenant'], 'params': ['type', 'uuid']}).then(function(params) {
    get.execute(params).then(function(result) {

      res.statusCode = result.statusCode;
      res.send(JSON.stringify(result.entity, null, 2));

      logger.success(log, req, result);
    }, function(error) {
      response.sendError(res, error);
      logger.error(log, req, error);
    });
  }, function(error) {
    response.sendError(res, error);
    logger.error(log, req, error);
  });
});

app.get('/:type/get/:uuid/:validityTime', function (req, res) {
  logger.start(log, req);

  request.required(req, {'headers': ['sopa-tenant'], 'params': ['type', 'uuid', 'validityTime']}).then(function(params) {
    get.execute(params).then(function(result) {

      res.statusCode = result.statusCode;
      res.send(JSON.stringify(result.entity, null, 2));

      logger.success(log, req, result);
    }, function(error) {
      response.sendError(res, error);
      logger.error(log, req, error);
    });
  }, function(error) {
    response.sendError(res, error);
    logger.error(log, req, error);
  });
});

app.get('/:type/get/:uuid/:validityTime/:registrationTime', function (req, res) {
  logger.start(log, req);

  request.required(req, {'headers': ['sopa-tenant'], 'params': ['type', 'uuid', 'validityTime', 'registrationTime']}).then(function(params) {
    get.execute(params).then(function(result) {

      res.statusCode = result.statusCode;
      res.send(JSON.stringify(result.entity, null, 2));

      logger.success(log, req, result);
    }, function(error) {
      response.sendError(res, error);
      logger.error(log, req, error);
    });
  }, function(error) {
    response.sendError(res, error);
    logger.error(log, req, error);
  });
});

/**
 * @api {get} /:type/search/:query Search (get)
 * @apiName search-get
 * @apiGroup business
 * @apiVersion 1.0.0
 * @apiDescription
 * bla bla
 *
 * @apiExample {curl} Example usage:
 *     curl -i http://localhost/person/create 
 *
 * @apiSampleRequest  http://localhost:8081/:type/create
 *
 * @apiHeader         {String}  sopa-version        Version of schema/rules to use for validation
 * @apiHeader         {String}  sopa-namespace      Namespace of schema/rules to use. This overrules lookup path for object type and tenant
 * @apiHeaderExample  {String}  sopa-version
 *                              1.0.2
 * @apiHeaderExample  {String}  sopa-namespace
 *                              kl
 *
 * @apiParam          {String}  type                Type of object eg. Person, Unit, Function, Class ...
 * @apiParam          {json}    object              Create JSON data for Person, Unit, Function, Class ...
 *
 * @apiSuccess (201)  {header}  uuid                Uuid of new object in response location header.
 * @apiSuccess (201)  {json}    paths               List of services that can be called on the created object.
 * @apiSuccessExample {header}  location:
 *                              /person/trace/
 * @apiSuccessExample {json}    Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *       "firstname": "John",
 *       "lastname": "Doe"
 *     } 
 */
app.get('/:type/search/:query', function (req, res) {
  logger.start(log, req);

  request.required(req, {'headers': ['sopa-tenant'], 'params': ['type', 'query']}).then(function(params) {
    search.get(params).then(function(result) {

      res.statusCode = result.statusCode;
      res.send(JSON.stringify(result.entity, null, 2));

      logger.success(log, req, result);
    }, function(error) {
      response.sendError(res, error);
      logger.error(log, req, error);
    });
  }, function(error) {
    response.sendError(res, error);
    logger.error(log, req, error);
  });
});

app.get('/:type/search/:query/:digest', function (req, res) {
  logger.start(log, req);

  request.required(req, {'headers': ['sopa-tenant'], 'params': ['type', 'query', 'digest']}).then(function(params) {
    search.get(params).then(function(result) {

      res.statusCode = result.statusCode;
      res.send(JSON.stringify(result.entity, null, 2));

      logger.success(log, req, result);
    }, function(error) {
      response.sendError(res, error);
      logger.error(log, req, error);
    });
  }, function(error) {
    response.sendError(res, error);
    logger.error(log, req, error);
  });
});

/**
 * @api {post} /:type/search Search (post)
 * @apiName search-post
 * @apiGroup business
 * @apiVersion 1.0.0
 * @apiDescription
 * bla bla
 *
 * @apiExample {curl} Example usage:
 *     curl -i http://localhost/person/create 
 *
 * @apiSampleRequest  http://localhost:8081/:type/create
 *
 * @apiHeader         {String}  sopa-version        Version of schema/rules to use for validation
 * @apiHeader         {String}  sopa-namespace      Namespace of schema/rules to use. This overrules lookup path for object type and tenant
 * @apiHeaderExample  {String}  sopa-version
 *                              1.0.2
 * @apiHeaderExample  {String}  sopa-namespace
 *                              kl
 *
 * @apiParam          {String}  type                Type of object eg. Person, Unit, Function, Class ...
 * @apiParam          {json}    object              Create JSON data for Person, Unit, Function, Class ...
 *
 * @apiSuccess (201)  {header}  uuid                Uuid of new object in response location header.
 * @apiSuccess (201)  {json}    paths               List of services that can be called on the created object.
 * @apiSuccessExample {header}  location:
 *                              /person/trace/
 * @apiSuccessExample {json}    Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *       "firstname": "John",
 *       "lastname": "Doe"
 *     } 
 */
app.post('/:type/search', function (req, res) {
  logger.start(log, req);

  request.required(req, {'headers': ['sopa-tenant']}).then(function(params) {
    search.post(params, req.body).then(function(result) {

      res.statusCode = result.statusCode;
      res.send(JSON.stringify(result.entity, null, 2));

      logger.success(log, req, result);
    }, function(error) {
      response.sendError(res, error);
      logger.error(log, req, error);
    });
  }, function(error) {
    response.sendError(res, error);
    logger.error(log, req, error);
  });
});