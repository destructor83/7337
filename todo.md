
- Rules validation
- Search
- full functional test

- api doc
- integrate docson in documentation
- Create schemas for Classification, Organisation, Case, Document, Address

- Authentication & Authorization
- Masking of get/trace/search results based on authorization scope (mongo query and JSON-SELECT) eg. remove CPR or blank out XXXXXX-XXXX, leave out results, scope objects

- Object Browser

- Performance test
- Load test
- Break test

- Schema editor
- Rules editor
- BPM Hire/move/fire
- STS integration / OIO XML

- log analyser / browser
- load analyser / browser

- setup nginx with lua to get request UUID added

- System specifikke udvidelser / patch af fx. org. hieraki (løn system, vagtplan, etc)
