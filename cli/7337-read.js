#!/usr/bin/env node
var program = require('commander');
var chalk = require('chalk');
var fs = require('fs');

program
    .usage('[options] [file]')
    .description('Parses 7337 objects and outputs temporal traces. Default is history.')
    .option('-t, --trace', 'bitemporal trace of object')
    .option('-e, --evolution', 'evolution of object')
    .parse(process.argv);
    
            console.log(program.args.length)
            if (program.args.length > 0) {
                var file = program.args[0];
                console.log('%s %s', program.trace, file);
                console.log(chalk.bold.cyan("Trace: " + file));
                
                var fileSize = fs.statSync(file).size;
                console.log(fileSize);
                var fileStream = fs.createReadStream(file);
        
                fileStream.on('readable', () => {
                  var chunk;
                  while (null !== (chunk = fileStream.read())) {
                    console.log('got %d bytes of data', chunk.length);
                  }
                });
        } else {
            process.stdin.resume();
            process.stdin.setEncoding('utf8');
            process.stdin.on('data', function(data) {
                process.stdout.write(data);
            });
            //make sure ending line
            process.stdin.on('end', function(data) {
                console.log('\n');
            });
        }