var NodeGit = require("nodegit");
var assert = require('chai').assert;
var path = require("path");
var _ = require('underscore');
var fse = require("fs-extra");
var fs = require("fs");

var repoPath = "../../../../newRepo";
var pathToRepo = path.resolve(__dirname, repoPath);

describe('Test of basic git stuff', function() {
  before(function () {
    fse.remove(pathToRepo);
  });
  
  describe('init', function () {
    it('should create a bare git repo', function () {
        fse.ensureDir(pathToRepo, function(err) {
            NodeGit.Repository.init(pathToRepo, 0).then(function (repo) {
              // In this function we have a repo object that we can perform git operations
              // on.
              fs.readdir(pathToRepo, function (err, files){
                console.log("assertion");
                assert.ok(false);
                assert.equal(files.length, 1, 'should have one dir');
                assert.equal(files[0], '.git', 'should be .git');
              });
                console.log(repo);
              // Note that with a new repository many functions will fail until there is
              // an initial commit.
            }).catch(function (reasonForFailure) {
              // If the repo cannot be created for any reason we can handle that case here.
              // NodeGit won't init a repo over a pre-existing repo.
            
                console.log(reasonForFailure);
            });
            console.log(err);
        });
    });
  });
});
