var assert = require('chai').assert;
var expect = require('chai').expect;
var should = require('chai').should();
var path = require("path");
var _ = require('underscore');
var fse = require("fs-extra");
var fs = require("fs");
var repoPath = "../../../../test_space/data/";
var uuid = require("../../../helpers/uuid");
var abspath = path.resolve(__dirname, repoPath);

console.log(abspath);

var objectuuid = "ae210d18-2e14-4406-b5bb-61221c4cae32"; //uuid.generate();
var objecttype = "unit";
var objectpath = abspath + "/" + objecttype + "/" + objectuuid + "/";
var reguuid = "661a347d-df66-426b-a93d-2b906be61802";
var regpath = objectpath + "/" + reguuid;

describe('Test of basic git stuff', function() {
  var dirExists = false;
  before(function () {
    fse.remove(abspath);
  });
  
  after(function () {
    fse.remove(abspath);
  });

  describe('path and file creation', function () {
    it('should create a new dir for the object', function (done) {
      fse.ensureDir(objectpath, function(err) {
          if (err) {
            assert.ok(false, "error thrown");
          } else {
            fs.readdir(objectpath + "../", function (err, files) {
              if (err) {
                assert.ok(false, "error thrown");
              } else {
                assert.equal(files.length, 1, 'should have one dir');
                assert.equal(files[0], objectuuid, 'should be object uuid');
              }
              done();
            });
          }
        });
      });
      
    it('should create a file for the registration', function (done) {
      var rs = fs.createReadStream('cli/git/tests/registration');
      var ws = fs.createWriteStream(regpath);
      rs.on('data', function(chunk) {
        ws.write(chunk);
      });
      rs.on('end', function () {
        ws.end();
        done();
      });
    });
  });
});
