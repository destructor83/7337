#!/usr/bin/env node
var program = require('commander');
var fs = require("fs");

var _get = require('../models/get');

program
  .usage('[options] <uuid|id>|<stdin>')
  .description('Get data about 7337 object at a specific time.')
  .option('-v, --valid-time [date-time]', 'at this valid time, else NOW')
  .option('-r, --registration-time [date-time]', 'at this registration time, else NOW')
  .option('-s, --select [JSON selectors]', 'select only part of the document XXX: na')
  .option('-x, --expand [JSON selectors]', 'expand the relations that are selected XXX: na')
//  .option('-m, --meta', 'include meta data in result: relation target types, validity and registration')
  .parse(process.argv);

if (program.args.length > 1) {
  program.help();
  process.exit(-1);
}

if (program.args.length == 0) {
  var uuid = '';
  
  process.stdin.resume();
  process.stdin.setEncoding('utf8');

  process.stdin.on('data', function (chunk) {
    uuid += chunk;
  });

  process.stdin.on('end', function () {
    doGet(uuid, program);
  });

} else if (program.args.length == 1) {

  doGet(program.args[0], program);
}

function doGet(uuid, program) {
  
  // Strip uuid for possible junk at the end
  uuid = uuid.substr(0,36);
  
  fs.readFile("/home/ubuntu/data/" + uuid, function (err, entity) {
    if (err) {
      console.log("7337 get: Error reading object with uuid: " + uuid);
      process.exit(-1);
    }

    _get.it({entity: entity, validTime: program.validTime, registrationTime: program.registrationTime}).then(
      function(success) {
        
        // XXX: Expand document references to enbedded object data
        
        // XXX: Filter document with selectors
      
        process.stdout.write(JSON.stringify(success.data, null, 2) + '\n');
        process.exit(0);
      }, function(fail) {
        console.log("7337 get: " + fail.statusMessage);
        if (fail.error) console.log(fail.error);
        process.exit(-1);
      }
    );
  });
}