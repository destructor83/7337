#!/usr/bin/env node
var program = require('commander');
var fs = require("fs");
var jsonfile = require('jsonfile');

var create = require('../models/create');

program
  .usage('[options] <file>|<stdin>')
  .description('Create 7337 object.')
  .option('-c, --comment [comment]', 'comment for registration')
  .option('-i, --id [id]', 'id used when generating object uuid')
  .option('-f, --valid-from [date-time]', 'when data is valid from')
  .option('-t, --valid-to [date-time]', 'when data is valid to')
  .option('-a, --actor [uuid|urn]', 'refernce to actor as: uuid:<uuid> or urn:<path>')
  .option('-n, --note [note]', 'note for validity record')
  .option('-s, --schema [type]', 'name of the object type, eg: 7337.dk:organisation:unit')
//  .option('-d, --dryrun ', 'no data is created. It returns a fake UUID')
//  .option('-v, --verbose ', 'be chatty - list rules that will be applied')
  .parse(process.argv);

if (program.args.length > 1) {
  program.help();
  process.exit(-1);
}

if (program.args.length == 0) {
  var input = '';
  
  process.stdin.resume();
  process.stdin.setEncoding('utf8');

  process.stdin.on('data', function (chunk) {
    input += chunk;
  });

  process.stdin.on('end', function () {
    doCreate(input, program);
  });

} else if (program.args.length == 1) {
  fs.readFile(program.args[0], function (err, data) {
    if (err) {
      console.log("7337 create: Error reading file: " + program.args[0] + "\n" + err);
      return;
    }

    doCreate(data, program);
  });
}

var datadir = "/home/ubuntu/data/";
function doCreate(input, program) {
  create.it({input: input, validFrom: program.validFrom, validTo: program.validTo, schema: program.schema, comment: program.comment, actor: program.actor, note: program.note, id: program.id, verbose: program.verbose}).then(

  function(success) {

    // Store data - XXX: use output stream / push to git and send AMQP
    
    jsonfile.spaces = 2;
    jsonfile.writeFile(datadir + success.data.uuid, success.data, function (err) {
      if (err) {
        console.log("7337 create: Cannot write to file: " + datadir + success.data.uuid);
        console.log(err.stack);
        process.exit(-1);
      } else {
        process.stdout.write(success.data.uuid + '\n');
        process.exit(0);
      }
    });
  }, function(fail) {
    console.log("7337 create: " + fail.statusMessage);
    if (fail.error) console.log(fail.error);
    process.exit(-1);
  });
}