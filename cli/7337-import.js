#!/usr/bin/env node
var _ = require('underscore');
var program = require('commander');
var fs = require("fs");
var jsonfile = require('jsonfile');

var _import = require('../models/import');

program
  .usage('[options] <file>|<stdin>')
  .description('Import 7337 object. This will override any changes to the same object if it has been imported before.')
  .option('-c, --comment [comment]', 'comment for registration')
//  .option('-d, --dryrun ', 'no data is created. It returns a fake UUID')
//  .option('-v, --verbose ', 'be chatty - list rules that will be applied')
  .parse(process.argv);

if (program.args.length > 1) {
  program.help();
  process.exit(-1);
}

if (program.args.length == 0) {
  var input = '';
  
  process.stdin.resume();
  process.stdin.setEncoding('utf8');

  process.stdin.on('data', function (chunk) {
    input += chunk;
  });

  process.stdin.on('end', function () {
    doImport(input, program);
  });

} else if (program.args.length == 1) {
  fs.readFile(program.args[0], function (err, entity) {
    if (err) {
      console.log("7337 import: Error reading file: " + program.args[0] + "\n" + err);
      return;
    }

    doImport(entity, program);

  });
}

function doImport(input, program) {
  _import.it({input: input, comment: program.comment}).then(
    function(success) {
    
      // Store data - XXX: use output stream / push to git and send AMQP
      jsonfile.spaces = 2;
      jsonfile.writeFile("/home/ubuntu/data/" + success.data.uuid, success.data, function (err) {
        if (err) {
          console.log("7337 import: Cannot write til file: /home/ubuntu/data/" + success.data.uuid);
          process.exit(-1);
        } else {
          process.stdout.write(success.data.uuid + '\n');
          process.exit(0);
        }
      });
    }, function(fail) {
      console.log("7337 import: " + fail.statusMessage);
      if (fail.error) console.log(fail.error);
      process.exit(-1);
    }
  );
}