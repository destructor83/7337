#!/usr/bin/env node
var _ = require('underscore');
var program = require('commander');
var fs = require("fs");
var jsonfile = require('jsonfile');

var passivate = require('../models/passivate');

program
  .usage('[options] <uuid>|<stdin>')
  .description('Passivate 7337 object. Passivate is a dynamic marking of that the object is not to be shown. The objects lifecycle is set to: Passivated.')
  .option('-c, --comment [comment]', 'comment for registration')
//  .option('-d, --dryrun ', 'no data is created. It returns a fake UUID')
//  .option('-v, --verbose ', 'be chatty - list rules that will be applied')
  .parse(process.argv);

if (program.args.length > 1) {
  program.help();
  process.exit(-1);
}
if (program.args.length == 0) {
  var input = '';
  
  process.stdin.resume();
  process.stdin.setEncoding('utf8');

  process.stdin.on('data', function (chunk) {
    input += chunk;
  });

  process.stdin.on('end', function () {
    readObject(input);
  });

} else if (program.args.length == 1) {

  readObject(program.args[0]);
}

function readObject(uuid) {
  uuid = uuid.substr(0,36);
  fs.readFile("/home/ubuntu/data/" + uuid, function (err, entity) {
    if (err) {
      console.log("7337 passivate: Error object with uuid: " + uuid);
      process.exit(-1);
    }
    
    doPassivate(JSON.parse(entity), program);
  });
}

function doPassivate(entity, program) {
  passivate.it({entity: entity, comment: program.comment}).then(
    function(success) {
    
      // Store data - XXX: use output stream / push to git and send AMQP
      jsonfile.spaces = 2;
      jsonfile.writeFile("/home/ubuntu/data/" + success.data.uuid, success.data, function (err) {
        if (err) {
          console.log("7337 passivate: Cannot write til file: /home/ubuntu/data/" + success.data.uuid);
          process.exit(-1);
        } else {
          process.stdout.write(success.data.uuid + '\n');
          process.exit(0);
        }
      });
    }, function(fail) {
      console.log("7337 passivate: " + fail.statusMessage);
      if (fail.error) console.log(fail.error);
      process.exit(-1);
    }
  );
}