#!/usr/bin/env node
var _ = require('underscore');
var program = require('commander');
var fs = require("fs");
var jsonfile = require('jsonfile');

var discard = require('../models/discard');

program
  .usage('[options] <uuid> <file>|<stdin>')
  .description('Discard 7337 object data.')
  .option('-c, --comment [comment]', 'comment for registration')
//  .option('-d, --dryrun ', 'no data is created. It returns a fake UUID')
//  .option('-v, --verbose ', 'be chatty - list rules that will be applied')
  .parse(process.argv);

if (program.args.length > 2 || program.args.length === 0) {
  program.help();
  process.exit(-1);
}

if (program.args.length == 1) {
  var uuid = '';
  
  process.stdin.resume();
  process.stdin.setEncoding('utf8');

  process.stdin.on('data', function (chunk) {
    uuid += chunk;
  });

  process.stdin.on('end', function () {
    
    fs.readFile(program.args[0], function (err, input) {
      if (err) {
        console.log("7337 discard: Error reading file: " + program.args[0] + "\n" + err);
        process.exit(-1);
      }
      
      fs.readFile("/home/ubuntu/data/" + uuid.substring(0,36), function (err, entity) {
        if (err) {
          console.log("7337 discard: Error reading object with uuid: " + uuid);
          process.exit(-1);
        }
        
        doDiscard(JSON.parse(entity), input, program);
      });
    });
  });

} else if (program.args.length == 2) {
  fs.readFile(program.args[1], function (err, input) {
    if (err) {
      console.log("7337 discard: Error reading file: " + program.args[1] + "\n" + err);
      process.exit(-1);
    }

    fs.readFile("/home/ubuntu/data/" + program.args[0], function (err, entity) {
      if (err) {
        console.log("7337 discard: Error object with uuid: " + program.args[0] + '\n' + err);
        process.exit(-1);
      }
      
      doDiscard(JSON.parse(entity), input, program);
    });
  });
}

function doDiscard(entity, input, program) {
  discard.it({entity: entity, input: input, comment: program.comment}).then(
    function(success) {
    
      // Store data - XXX: use output stream / push to git and send AMQP
      jsonfile.spaces = 2;
      jsonfile.writeFile("/home/ubuntu/data/" + success.data.uuid, success.data, function (err) {
        if (err) {
          console.log("7337 discard: Cannot write til file: /home/ubuntu/data/" + success.data.uuid);
          process.exit(-1);
        } else {
          process.stdout.write(success.data.uuid + '\n');
          process.exit(0);
        }
      });
    }, function(fail) {
      console.log(fail.statusMessage);
      if (fail.error) console.log(fail.error);
      process.exit(-1);
    }
  );
}