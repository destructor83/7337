var _ = require('underscore');
var bunyan = require('bunyan');

var logger = require('../middlewares/logger');
var uuid = require('../helpers/uuid');

// init
var log = bunyan.createLogger({
  name: 'create',
  level: 'trace'
});
