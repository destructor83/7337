#!/usr/bin/env node
var program = require('commander');
var fs = require("fs");

program
  .usage('<uuid>|<stdin>')
  .description('Trace of 7337 object.')
  .parse(process.argv);

if (program.args.length > 1) {
  program.help();
  process.exit(-1);
}

if (program.args.length == 0) {
  var input = '';
  
  process.stdin.resume();
  process.stdin.setEncoding('utf8');

  process.stdin.on('data', function (chunk) {
    input += chunk;
  });

  process.stdin.on('end', function () {
    doTrace(input);
  });

} else if (program.args.length == 1) {

  doTrace(program.args[0]);
}

function doTrace(uuid) {
  uuid = uuid.substr(0,36);
  fs.readFile("/home/ubuntu/data/" + uuid, "utf-8", function(err, data) {
    if (err) {
      console.log("7337 trace: Cannot read object with UUID: " + uuid);
      process.exit(-1);
    }
    process.stdout.write(data + '\n', 'utf-8');
    process.exit(0);
  });
}