#!/usr/bin/env node
var program = require('commander');

program
 .version('0.0.1')
 .description('Utility for interacting with 7337 objects')
 .usage('<command>')
 .command('create',     'create object with data from JSON file with lifecycle set as local')
 .command('delete',     'delete object permanently')
 .command('discard',    'discard part of the objects data permanently')
// .command('evolution',  'returns object evolution at a specific valid time')
// .command('find',     'find objects from query, returns list of uuids')
 .command('get',        'get data at a specific time')
// .command('history',    'returns object history at a specific registration time')
 .command('import',     'load object and set lifecycle to imported')
 .command('passivate',  'change objects lifecycle to passivated')
 .command('remove',     'change objects lifecycle to removed')
 .command('trace',      'get objects bitemporal trace')
 .command('update',     'update object with data from JSON file')
// .command('watch',     'watch changes on objects')
 .parse(process.argv);