const fs = require("fs");
const bunyan = require('bunyan');
const _ = require('lodash');

const uuid = require('7337-object/src/uuid');

var titles = ["A/n plejehjemsass.", "Afd.leder døgninst.", "Afd.leder m souschef funktion", "Afdelingsbibliotekar", "Afdelingsleder", "Afspændingspædagog", "Agronom", "Akademiingeniør", "Andre chefer", "Arkitekt", "Ass. halinspektør", "Ass. leder", "Assistent", "Afdelingsleder", "Afdelingsleder og udvalgssekretær", "Afsnitsleder", "Afsnitsleder for integration", "Analysekonsulent", "Arbejdsmarkeds- og voksendirektør", "Ass. centerleder", "Bac.scient.cons", "Bachelor", "Beredskabsassistent", "Beredskabschef", "Beredskabsinspektør", "Beredskabsmester", "Beskæftigelsesvejl.", "Bibliot./mellemlede.", "Bibliot./specialist", "Bibliotekar", "Biblioteksleder", "Bogopsætter", "Borgmester", "Brandmand", "Brandpersonale", "Byggetekniker", "Bygningskonstruktør", "Beskæftigelsesrådgiver", "Bolig - repatriering", "Bygherrerådgiver", "Børne-og kulturchef", "Børnebibliotekar", "Børnehavekl.leder", "Cand scient", "Cand. it", "Cand. oecon.", "Cand. scient", "Cand. scient.pol", "Cand. scient.soc", "Cand. techn. soc.", "Cand.ling.merc.", "Cand.mag", "Cand.merc.", "Cand.phil", "Cand.phil.", "Cand.pæd", "Cand.scient.adm.", "Cand.scient.bibl.", "Centerleder", "Chef", "Chefkonsulent", "Civilingeniør", "Civiløkonom", "Callcenter", "Centerleder", "Centerleder rosengården", "Chef for affald og genbrug", "Chef for seniorservice", "Controller", "Dagcenterl/daghj.led", "Dagplejeassistent", "Dagplejepædagog", "Dagplejer", "Datamatiker/datanom", "Deltidsbrandmand", "Diplomingeniør", "Distriktsleder", "Demenskonsulent", "Diplomingeniør", "Distriksleder", "Distriktsleder", "Distriktstandlæge", "Driftsassistent", "Driftsleder", "Døgninst.medhjælper", "Edb-assistent", "Edb-personale", "Egu-elev", "Ekspeditionssekretær", "Elev", "Elev over 21 år", "Ergoterapeut", "Ern-og hush.øko.m.fl", "Ernæringsass.elev", "Ernæringsassistent", "Ergoterapeut", "Erhvervsvejl.", "Ernæringsassistent", "Faglært", "Faglært holdleder", "Familiepleje", "Familieplejekonsul.", "Filialklinikleder", "Foresp.nye lønninger", "Formand", "Forstander", "Forstkandidat", "Forvaltningschef", "Frivillig", "Fuldmægtig", "Fysioterapeut", "Fagl. koord.", "Faglig konsulent", "Faglig koordinator", "Faglig områdeleder", "Fam.plej.-0103015030", "Fam.plej.-1008970390", "Familiekonsulent", "Fleks og ledighedsydelse", "Forebyggende hjemmebesøger", "Forhandlingskonsulent", "Formand", "Formand for folkeoplysningsudvalget", "Fuldmægtig", "Førtidspension", "Gartner m. v.", "Gis-medarbejder", "Grafisk designer", "Grafisk formidler", "Gæstedagplejer", "Hal/stadioninspektør", "Handicapledsager", "Havneassistent", "Havnechef", "Havnefoged/mester", "Heltidsbesk. leder", "Hjemmehjælper", "Hjemmevejleder", "Holdleder", "Hortonom", "Hr-konsulent", "Husassistent", "Håndværker", "Ikke udd. personale", "Individuel aflønning", "Informatikass.", "Instr.fysioterapeut", "Instruktør/holdleder", "It-konsulent", "It-medarbejder", "Idrætspilot", "Indkøbskonsulent", "Informatikassistent", "Instruktør", "Jobkons./vejleder", "Jobkons/vejleder", "Jordbrugsteknolog", "Journalist", "Jurist", "Jobcenterchef", "Kantineleder", "Klinisk underviser", "Klubass. u/udd.", "Klubassistent", "Klubmedarb. ej udd.", "Kok", "Komm.best.medl.", "Kommunaldirektør", "Kommunallæge", "Konsulent", "Kontaktperson", "Kontorassistent", "Kontorelev", "Kontorelev o/25 år", "Kostfaglig eneansvar", "Kostfaglig leder", "Kulturtekniker", "Kommunikationschef", "Kommunikationskonsulent", "Konsulent", "Kostkonsulent", "Kultur- og udviklingschef", "Landinspektør", "Landskabsark", "Led børnebibliotekar", "Led. adm. stilling", "Led. skolepsykolog", "Led. sundh.plejerske", "Led. sygeplejerske", "Led.adm.stilling", "Led.hal/stadioninsp.", "Led.sygeplejerske", "Ledende beskæft.vejl", "Ledende ergoterapeut", "Ledende fys.terapeut", "Ledende pædagog", "Ledende sundhedspl.", "Ledende økonoma", "Leder", "Leder/mellemled/spec", "Ledende ssp-konsulent", "Leder af drift- og supportafdelingen", "Leder af rådhusservice", "Leder af uu", "Lonkl_tekst", "Lystekniker", "Lærer", "Lærer m/særlige kval", "Lærer/overlærer", "Lægekonsulent", "Lønkonsulent", "Magister", "Maskinmester", "Mellemleder", "Miljø-/lab.tekniker", "Miljøingeniør", "Musikskoleleder", "Musikskolelærer", "Mediegrafiker", "Miljømedarbejder", "Musikterapeut", "Naturformidler", "Nævnsmedlem", "Områdeleder", "Omsorgs-/pæd.medhj.", "Omsorgs-/pædagog mhj", "Omsorgsmedhjælper", "Overassistent", "Overassistent (f)", "Overlærer", "Overtandlæge", "Overassistent", "Parkeringsav./kontr.", "Piccoline", "Planlægger", "Plejehjemsassistent", "Plejer", "Plejer /bofællesskab", "Produktionsteknolog", "Professionsbachelor", "Projektkoordinator", "Projektleder", "Psykolog", "Pers.hj.", "Pers.jur konsulent", "Personalekonsulent", "Produktionsleder", "Projektleder", "Projektmedarbejder", "Pæd. medhj.", "Pæd. medhj./ass.", "Pæd.ass-/pguelev o18", "Pæd.medhj.", "Pæd.medhj./ass.", "Pæd.mehj.", "Pædagog", "Pædagogisk assistent", "Pædagogisk konsulent", "Pædagogisk medhjælp", "Pædagogmedhjælper", "Pædagogstuderende", "Pædagogisk vejleder", "Rengøringsassistent", "Regnskabsspecialist", "Råd-nævn-kommiss.", "Sagsbehandler", "Serviceassistent", "Skibsfører", "Skolekonsulent", "Skolepsykolog", "Skolerengøringsass.", "Skov-/landskabsing.", "Skovbruger", "Soc.- og sundh.ass.", "Soc.- og sundh.hjælp", "Soc.-og sundhedsass.", "Soc.-sundh.ass.elev", "Soc./sundh.hj.elev", "Socialformidler", "Socialpsyk. medarb.", "Socialpæd.konsulent", "Socialpædagog", "Socialrådgiver", "Souschef", "Souschef/afd.leder", "Specialarbejder", "Specialist", "Specialist it", "Specialkonsulent", "Speciallægekonsulent", "Specialtandlæge", "Stedfort. for forst.", "Stedfortr.inst.leder", "Stedfortræder", "Studentermedhjælp", "Støtte i familie", "Støttepædagog", "Sundhedsmedhjælper", "Sundhedsplej.stud", "Sundhedsplejerske", "Sygehjælper", "Sygeplejerske", "Sagsbehandlende visitator", "Sagsbehandler", "Sekretariatsleder", "Skovarbejder", "Social pædagog", "Socialfaglig medarbejder", "Souschef", "Specialkonsulent", "Stabschef", "Sundhedschef", "Sundhedskonsulent", "Tandklinikassistent", "Tandlæge", "Tandlægekonsulent", "Tandplejer", "Tandtekniker", "Tekn. servicechef", "Tekn. serviceleder", "Tekn. servicemedarb.", "Teknikumingeniør", "Teknisk chef", "Teknisk designer", "Teknisk serviceleder", "Tilsynsassistent", "Timelønnet leder", "Timelønnet lærer", "Teamkoordinator", "Teamleder", "Tolk + ad hoc", "Trafikplanlægger", "Trivlseskonsulent", "Turisme- og marketingkoordinator", "Turismechef", "Udd.ejendomsserv.tek", "Udvalgsformand", "Ufuldst. ansættelse", "Ungarbejder", "Ungdomsskoleinsp.", "Uu-vejleder - koordinator", "Uud.hjemmevejleder", "Uuddannet lærer", "Uddannelseskonsulent", "Udviklingskonsulent", "Udviklingsmedarbejder", "Ungekonsulent", "Vejleder", "Viceberedskabschef", "Viceberedskabsmester", "Viceborgmester", "Viceskoleinspektør", "Visitator", "Voksenelev", "Voksenelev o/25 år", "Virksomhedskonsulent", "Visitator", "Voksne", "Værkstedsassistent", "Værkstedsleder", "Webredaktør", "Assisterende centerleder", "Centerleder", "Distriktsleder", "Ergoterapeut", "Foreb. sygeplejerske", "Hjemmevejleder", "Kloakchef", "Primærsygepl sundhedsklinikken", "Reng.leder", "Sundhedsvejleder", "Uu-vejleder", "Økonom", "Økonoma", "Øvelsesskolelærer", "Økonomi og personalemedarbejder", "Økonomi- & administrationsdirektør", "Økonomichef", "Økonomikonsulent", "Økonomimedarbejder", "Økonomisk kordinator", "Økonomisk/adm. koordinato"];


// Create classification objects and type of titles

// Type
const TITLE_SCHEMA = {
  id: uuid.v5("application/schema+json" + "http://reflective.dk/title"),
  type: 'application/schema+json',
  registrations: [
    {
      validity: [
        {
          input: {
            "$schema": "http://json-schema.org/draft-06/schema",
            "$id": "http://reflective.dk/title",
            "type": "object",
            "allOf": [
              { "$ref": "http://reflective.dk/classification/class" }
            ]
          }
        }
      ]
    }
  ]
};

// Local classification
const LOCAL_CLASSIFICATION = {
  id: uuid.v5('local_classification'),
  type: 'application/json',
  registrations: [
    {
      schema: 'http://reflective.dk/classification/classification',
      validity: [
        {
          input: {
            key: 'local_classification',
            name: 'Vores klassifikationer',
            state: 'published'
          }
        }
      ]
    }
  ]
};

// Titles facet
const TITLE_FACET = {
  id: uuid.v5('title_facet'),
  type: 'application/json',
  registrations: [
    {
      schema: 'http://reflective.dk/classification/facet',
      validity: [
        {
          input: {
            key: 'local_classification',
            name: 'Vores klassifikationer',
            state: 'published',
            classification: {id: LOCAL_CLASSIFICATION.id}
          }
        }
      ]
    }
  ]
};

/*
module.exports = (opts) => {
  var log_level = _.get(opts, 'log_level', 'error');
  this.logger = bunyan.createLogger({name: 'R-boot', level: log_level });

  this.localClassification = new stream.Transform( { objectMode: true } );
  this.localClassification._transform = (chunk, encoding, done) => {
    if (_.isUndefined(this.localClassification.sent)) {
      this.localClassification.push(JSON.stringify(LOCAL_CLASSIFICATION,null,2)+',');
      this.localClassification.sent = true;
    }
    this.localClassification.push(chunk);
    done();
  };

  this.titleFacet = new stream.Transform( { objectMode: true } );
  this.titleFacet._transform = (chunk, encoding, done) => {
    if (_.isUndefined(this.titleFacet.sent)) {
      this.titleFacet.push(JSON.stringify(TITLE_FACET,null,2)+',');
      this.titleFacet.sent = true;
    }
    this.titleFacet.push(chunk);
    done();
  };

  this.titleSchema = new stream.Transform( { objectMode: true } );
  this.titleSchema._transform = (chunk, encoding, done) => {
    if (_.isUndefined(this.titleSchema.sent)) {
      this.titleSchema.push(JSON.stringify(TITLE_SCHEMA,null,2)+',');
      this.titleSchema.sent = true;
    }
    this.titleSchema.push(chunk);
    done();
  };

  this.titleData = new stream.Transform( { objectMode: true } );
  this.titleData._transform = (chunk, encoding, done) => {
    if (_.isUndefined(this.createOrganisation.sent)) {
      this.titleFacet.push(JSON.stringify(TITLE_FACET,null,2)+',');
      this.titleFacet.sent = true;
    }
    this.titleFacet.push(chunk);
    done();
  };
*/
  function createObject(title) {
    var payload = {
      name: title,
      state: 'published',

      facet: {id: TITLE_FACET.id},
    };

    return {
      type: 'application/json',
      registrations: [
        {
          schema: 'http://reflective.dk/title',
          validity: [
            {
              input: payload
            }
          ]
        }
      ]
    };
  }
//};

var data = {objects:[]};
titles.forEach((title) => {
  data.objects.push(createObject(title));
});

fs.writeFileSync("./data/title_type.json", '{"objects":['+JSON.stringify(TITLE_SCHEMA, null, 2)+']}');
fs.writeFileSync("./data/title_facet.json", '{"objects":['+JSON.stringify(TITLE_FACET, null, 2)+']}');
fs.writeFileSync("./data/local_classification.json", '{"objects":['+JSON.stringify(LOCAL_CLASSIFICATION, null, 2)+']}');
fs.writeFileSync("./data/title_data.json", JSON.stringify(data, null, 2));




