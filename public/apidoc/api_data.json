[
  {
    "type": "post",
    "url": "/:type/create",
    "title": "Create",
    "name": "create",
    "group": "business",
    "version": "1.0.0",
    "description": "<p>Creates a new object of the given type. For successful creation, the posted data has to validate against the schema and rules for the type.</p>",
    "examples": [
      {
        "title": "curl",
        "content": "curl -i http://localhost/person/create",
        "type": "curl"
      },
      {
        "title": "wget",
        "content": "curl -i http://localhost/person/create",
        "type": "wget"
      },
      {
        "title": "javascript",
        "content": "curl -i http://localhost/person/create",
        "type": "javascript"
      },
      {
        "title": "java",
        "content": "curl -i http://localhost/person/create",
        "type": "java"
      },
      {
        "title": ".net",
        "content": "curl -i http://localhost/person/create",
        "type": ".net"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8081/:type/create"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "sopa-version",
            "description": "<p>Version of schema/rules to use for validation</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "sopa-namespace",
            "description": "<p>Namespace of schema/rules to use. This overrules lookup path for object type and tenant</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "sopa-version",
          "content": "1.2",
          "type": "String"
        },
        {
          "title": "sopa-namespace",
          "content": "kl.dk",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type of object eg. Person, Unit, Function, Class ...</p>"
          },
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "object",
            "description": "<p>Create JSON data for Person, Unit, Function, Class ...</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "header",
            "optional": false,
            "field": "uuid",
            "description": "<p>Uuid of new object in response location header.</p>"
          },
          {
            "group": "201",
            "type": "json",
            "optional": false,
            "field": "paths",
            "description": "<p>List of services that can be called on the created object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "location:",
          "content": "<uuid>",
          "type": "header"
        },
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 OK\n{\n  \"firstname\": \"John\",\n  \"lastname\": \"Doe\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/business.js",
    "groupTitle": "business"
  },
  {
    "type": "delete",
    "url": "/:type/delete/:uuid",
    "title": "Delete",
    "name": "delete",
    "group": "business",
    "version": "1.0.0",
    "description": "<p>Removes the object, but marking its lifecycle as removed. The resource can still be retrived using the trace operation. To erase the resource so that it no longer can be retrived, use delete.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost/person/create",
        "type": "curl"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8081/:type/create"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "sopa-version",
            "description": "<p>Version of schema/rules to use for validation</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "sopa-namespace",
            "description": "<p>Namespace of schema/rules to use. This overrules lookup path for object type and tenant</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "sopa-version",
          "content": "1.0.2",
          "type": "String"
        },
        {
          "title": "sopa-namespace",
          "content": "kl",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type of object eg. Person, Unit, Function, Class ...</p>"
          },
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "object",
            "description": "<p>Create JSON data for Person, Unit, Function, Class ...</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "header",
            "optional": false,
            "field": "uuid",
            "description": "<p>Uuid of new object in response location header.</p>"
          },
          {
            "group": "201",
            "type": "json",
            "optional": false,
            "field": "paths",
            "description": "<p>List of services that can be called on the created object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "location:",
          "content": "/person/trace/",
          "type": "header"
        },
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 OK\n{\n  \"firstname\": \"John\",\n  \"lastname\": \"Doe\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/business.js",
    "groupTitle": "business"
  },
  {
    "type": "patch",
    "url": "/:type/discard/:uuid",
    "title": "Discard",
    "name": "discard",
    "group": "business",
    "version": "1.0.0",
    "description": "<p>bla bla</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost/person/create",
        "type": "curl"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8081/:type/create"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "sopa-version",
            "description": "<p>Version of schema/rules to use for validation</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "sopa-namespace",
            "description": "<p>Namespace of schema/rules to use. This overrules lookup path for object type and tenant</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "sopa-version",
          "content": "1.0.2",
          "type": "String"
        },
        {
          "title": "sopa-namespace",
          "content": "kl",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type of object eg. Person, Unit, Function, Class ...</p>"
          },
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "object",
            "description": "<p>Create JSON data for Person, Unit, Function, Class ...</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "header",
            "optional": false,
            "field": "uuid",
            "description": "<p>Uuid of new object in response location header.</p>"
          },
          {
            "group": "201",
            "type": "json",
            "optional": false,
            "field": "paths",
            "description": "<p>List of services that can be called on the created object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "location:",
          "content": "/person/trace/",
          "type": "header"
        },
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 OK\n{\n  \"firstname\": \"John\",\n  \"lastname\": \"Doe\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/business.js",
    "groupTitle": "business"
  },
  {
    "type": "get",
    "url": "/:type/evolution/:uuid/:validityTime",
    "title": "Evolution",
    "name": "evolution",
    "group": "business",
    "version": "1.0.0",
    "description": "<p>bla bla</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost/person/create",
        "type": "curl"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8081/:type/create"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "sopa-version",
            "description": "<p>Version of schema/rules to use for validation</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "sopa-namespace",
            "description": "<p>Namespace of schema/rules to use. This overrules lookup path for object type and tenant</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "sopa-version",
          "content": "1.0.2",
          "type": "String"
        },
        {
          "title": "sopa-namespace",
          "content": "kl",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type of object eg. Person, Unit, Function, Class ...</p>"
          },
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "object",
            "description": "<p>Create JSON data for Person, Unit, Function, Class ...</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "header",
            "optional": false,
            "field": "uuid",
            "description": "<p>Uuid of new object in response location header.</p>"
          },
          {
            "group": "201",
            "type": "json",
            "optional": false,
            "field": "paths",
            "description": "<p>List of services that can be called on the created object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "location:",
          "content": "/person/trace/",
          "type": "header"
        },
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 OK\n{\n  \"firstname\": \"John\",\n  \"lastname\": \"Doe\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/business.js",
    "groupTitle": "business"
  },
  {
    "type": "get",
    "url": "/:type/get/:uuid",
    "title": "Get",
    "name": "get",
    "group": "business",
    "version": "1.0.0",
    "description": "<p>bla bla</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost/person/create",
        "type": "curl"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8081/:type/create"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "sopa-version",
            "description": "<p>Version of schema/rules to use for validation</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "sopa-namespace",
            "description": "<p>Namespace of schema/rules to use. This overrules lookup path for object type and tenant</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "sopa-version",
          "content": "1.0.2",
          "type": "String"
        },
        {
          "title": "sopa-namespace",
          "content": "kl",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type of object eg. Person, Unit, Function, Class ...</p>"
          },
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "object",
            "description": "<p>Create JSON data for Person, Unit, Function, Class ...</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "header",
            "optional": false,
            "field": "uuid",
            "description": "<p>Uuid of new object in response location header.</p>"
          },
          {
            "group": "201",
            "type": "json",
            "optional": false,
            "field": "paths",
            "description": "<p>List of services that can be called on the created object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "location:",
          "content": "/person/trace/",
          "type": "header"
        },
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 OK\n{\n  \"firstname\": \"John\",\n  \"lastname\": \"Doe\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/business.js",
    "groupTitle": "business"
  },
  {
    "type": "get",
    "url": "/:type/history/:uuid/:registrationTime",
    "title": "History",
    "name": "history",
    "group": "business",
    "version": "1.0.0",
    "description": "<p>bla bla</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost/person/create",
        "type": "curl"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8081/:type/create"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "sopa-version",
            "description": "<p>Version of schema/rules to use for validation</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "sopa-namespace",
            "description": "<p>Namespace of schema/rules to use. This overrules lookup path for object type and tenant</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "sopa-version",
          "content": "1.0",
          "type": "String"
        },
        {
          "title": "sopa-namespace",
          "content": "kl",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type of object eg. Person, Unit, Function, Class ...</p>"
          },
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "object",
            "description": "<p>Create JSON data for Person, Unit, Function, Class ...</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "header",
            "optional": false,
            "field": "uuid",
            "description": "<p>Uuid of new object in response location header.</p>"
          },
          {
            "group": "201",
            "type": "json",
            "optional": false,
            "field": "paths",
            "description": "<p>List of services that can be called on the created object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "location:",
          "content": "/person/trace/",
          "type": "header"
        },
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 OK\n{\n  \"firstname\": \"John\",\n  \"lastname\": \"Doe\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/business.js",
    "groupTitle": "business"
  },
  {
    "type": "put",
    "url": "/:type/import",
    "title": "Import",
    "name": "import",
    "group": "business",
    "version": "1.0.0",
    "description": "<p>bla bla</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost/person/create",
        "type": "curl"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8081/:type/create"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "sopa-version",
            "description": "<p>Version of schema/rules to use for validation</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "sopa-namespace",
            "description": "<p>Namespace of schema/rules to use. This overrules lookup path for object type and tenant</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "sopa-version",
          "content": "1.0.2",
          "type": "String"
        },
        {
          "title": "sopa-namespace",
          "content": "kl",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type of object eg. Person, Unit, Function, Class ...</p>"
          },
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "object",
            "description": "<p>Create JSON data for Person, Unit, Function, Class ...</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "header",
            "optional": false,
            "field": "uuid",
            "description": "<p>Uuid of new object in response location header.</p>"
          },
          {
            "group": "201",
            "type": "json",
            "optional": false,
            "field": "paths",
            "description": "<p>List of services that can be called on the created object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "location:",
          "content": "/person/trace/",
          "type": "header"
        },
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 OK\n{\n  \"firstname\": \"John\",\n  \"lastname\": \"Doe\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/business.js",
    "groupTitle": "business"
  },
  {
    "type": "patch",
    "url": "/:type/passivate/:uuid",
    "title": "Passivate",
    "name": "passivate",
    "group": "business",
    "version": "1.0.0",
    "description": "<p>bla bla</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost/person/create",
        "type": "curl"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8081/:type/create"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "sopa-version",
            "description": "<p>Version of schema/rules to use for validation</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "sopa-namespace",
            "description": "<p>Namespace of schema/rules to use. This overrules lookup path for object type and tenant</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "sopa-version",
          "content": "1.0.2",
          "type": "String"
        },
        {
          "title": "sopa-namespace",
          "content": "kl",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type of object eg. Person, Unit, Function, Class ...</p>"
          },
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "object",
            "description": "<p>Create JSON data for Person, Unit, Function, Class ...</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "header",
            "optional": false,
            "field": "uuid",
            "description": "<p>Uuid of new object in response location header.</p>"
          },
          {
            "group": "201",
            "type": "json",
            "optional": false,
            "field": "paths",
            "description": "<p>List of services that can be called on the created object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "location:",
          "content": "/person/trace/",
          "type": "header"
        },
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 OK\n{\n  \"firstname\": \"John\",\n  \"lastname\": \"Doe\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/business.js",
    "groupTitle": "business"
  },
  {
    "type": "patch",
    "url": "/:type/remove/:uuid",
    "title": "Remove",
    "name": "remove",
    "group": "business",
    "version": "1.0.0",
    "description": "<p>Removes the object, but marking its lifecycle as removed. The resource can still be retrived using the trace operation. To erase the resource so that it no longer can be retrived, use delete.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost/person/create",
        "type": "curl"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8081/:type/create"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "sopa-version",
            "description": "<p>Version of schema/rules to use for validation</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "sopa-namespace",
            "description": "<p>Namespace of schema/rules to use. This overrules lookup path for object type and tenant</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "sopa-version",
          "content": "1.0.2",
          "type": "String"
        },
        {
          "title": "sopa-namespace",
          "content": "kl",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type of object eg. Person, Unit, Function, Class ...</p>"
          },
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "object",
            "description": "<p>Create JSON data for Person, Unit, Function, Class ...</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "header",
            "optional": false,
            "field": "uuid",
            "description": "<p>Uuid of new object in response location header.</p>"
          },
          {
            "group": "201",
            "type": "json",
            "optional": false,
            "field": "paths",
            "description": "<p>List of services that can be called on the created object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "location:",
          "content": "/person/trace/",
          "type": "header"
        },
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 OK\n{\n  \"firstname\": \"John\",\n  \"lastname\": \"Doe\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/business.js",
    "groupTitle": "business"
  },
  {
    "type": "get",
    "url": "/:type/search/:query",
    "title": "Search (get)",
    "name": "search_get",
    "group": "business",
    "version": "1.0.0",
    "description": "<p>bla bla</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost/person/create",
        "type": "curl"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8081/:type/create"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "sopa-version",
            "description": "<p>Version of schema/rules to use for validation</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "sopa-namespace",
            "description": "<p>Namespace of schema/rules to use. This overrules lookup path for object type and tenant</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "sopa-version",
          "content": "1.0.2",
          "type": "String"
        },
        {
          "title": "sopa-namespace",
          "content": "kl",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type of object eg. Person, Unit, Function, Class ...</p>"
          },
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "object",
            "description": "<p>Create JSON data for Person, Unit, Function, Class ...</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "header",
            "optional": false,
            "field": "uuid",
            "description": "<p>Uuid of new object in response location header.</p>"
          },
          {
            "group": "201",
            "type": "json",
            "optional": false,
            "field": "paths",
            "description": "<p>List of services that can be called on the created object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "location:",
          "content": "/person/trace/",
          "type": "header"
        },
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 OK\n{\n  \"firstname\": \"John\",\n  \"lastname\": \"Doe\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/business.js",
    "groupTitle": "business"
  },
  {
    "type": "post",
    "url": "/:type/search",
    "title": "Search (post)",
    "name": "search_post",
    "group": "business",
    "version": "1.0.0",
    "description": "<p>bla bla</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost/person/create",
        "type": "curl"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8081/:type/create"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "sopa-version",
            "description": "<p>Version of schema/rules to use for validation</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "sopa-namespace",
            "description": "<p>Namespace of schema/rules to use. This overrules lookup path for object type and tenant</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "sopa-version",
          "content": "1.0.2",
          "type": "String"
        },
        {
          "title": "sopa-namespace",
          "content": "kl",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type of object eg. Person, Unit, Function, Class ...</p>"
          },
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "object",
            "description": "<p>Create JSON data for Person, Unit, Function, Class ...</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "header",
            "optional": false,
            "field": "uuid",
            "description": "<p>Uuid of new object in response location header.</p>"
          },
          {
            "group": "201",
            "type": "json",
            "optional": false,
            "field": "paths",
            "description": "<p>List of services that can be called on the created object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "location:",
          "content": "/person/trace/",
          "type": "header"
        },
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 OK\n{\n  \"firstname\": \"John\",\n  \"lastname\": \"Doe\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/business.js",
    "groupTitle": "business"
  },
  {
    "type": "get",
    "url": "/:type/trace/:uuid",
    "title": "Trace",
    "name": "trace",
    "group": "business",
    "version": "1.0.0",
    "description": "<p>bla bla</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost/person/create",
        "type": "curl"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8081/:type/create"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "sopa-version",
            "description": "<p>Version of schema/rules to use for validation</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "sopa-namespace",
            "description": "<p>Namespace of schema/rules to use. This overrules lookup path for object type and tenant</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "sopa-version",
          "content": "1.0.2",
          "type": "String"
        },
        {
          "title": "sopa-namespace",
          "content": "kl",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type of object eg. Person, Unit, Function, Class ...</p>"
          },
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "object",
            "description": "<p>Create JSON data for Person, Unit, Function, Class ...</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "header",
            "optional": false,
            "field": "uuid",
            "description": "<p>Uuid of new object in response location header.</p>"
          },
          {
            "group": "201",
            "type": "json",
            "optional": false,
            "field": "paths",
            "description": "<p>List of services that can be called on the created object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "location:",
          "content": "/person/trace/",
          "type": "header"
        },
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 OK\n{\n  \"firstname\": \"John\",\n  \"lastname\": \"Doe\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/business.js",
    "groupTitle": "business"
  },
  {
    "type": "patch",
    "url": "/:type/update/:uuid",
    "title": "Update",
    "name": "update",
    "group": "business",
    "version": "1.0.0",
    "description": "<p>bla bla</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost/person/create",
        "type": "curl"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8081/:type/create"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "sopa-version",
            "description": "<p>Version of schema/rules to use for validation</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "sopa-namespace",
            "description": "<p>Namespace of schema/rules to use. This overrules lookup path for object type and tenant</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "sopa-version",
          "content": "1.0.2",
          "type": "String"
        },
        {
          "title": "sopa-namespace",
          "content": "kl",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type of object eg. Person, Unit, Function, Class ...</p>"
          },
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "object",
            "description": "<p>Create JSON data for Person, Unit, Function, Class ...</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "header",
            "optional": false,
            "field": "uuid",
            "description": "<p>Uuid of new object in response location header.</p>"
          },
          {
            "group": "201",
            "type": "json",
            "optional": false,
            "field": "paths",
            "description": "<p>List of services that can be called on the created object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "location:",
          "content": "/person/trace/",
          "type": "header"
        },
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 OK\n{\n  \"firstname\": \"John\",\n  \"lastname\": \"Doe\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/business.js",
    "groupTitle": "business"
  }
]
