Example of a unit that is created and then updated. Get unit-get.json is the output from GET.

- unit-create-input.json    Input to create
- unit-update-input.json    Input to update
- unit-get-output.json      Output from get

$ sopa create unit unit-create-input.json -vf 2016-07-04 --comment="Example"

$ sopa update unit unit-update-input.json --valid-from=2016-08-01 --comment="Now also with tasks" --actor-type="/businessobject/unit" --actor-id="urn:key:1" --note="From KLE"

$ sopa get unit c5a7f3e0-5696-409b-abc7-9b79ca9f33ff -vt 2016-08-01 -rt 2016-08-01
$ sopa get unit c5a7f3e0-5696-409b-abc7-9b79ca9f33ff # now/now

- unit-create-data.json    The data file after create 
- unit-update-data.json    The data file after update

The *-data.js file === to registration message

Limitation: only one validity for each create / update, so if eg. a unit is to be created and later moved to a different parent then two ops. needed: create then update


Core commands:

$ sopa create
$ sopa update
$ sopa import       # input is output from trace or cat of all the changes
$ sopa passivate    # logic: lifecycle=passivated
$ sopa remove       # logic: lifecycle=removed
$ sopa delete       # physical delete object
$ sopa discard      # physical delete object data given in input
$ sopa trace        # Bitemporal trace document, embed reg/validity into document, perhaps add support for vt/rt filtering
$ sopa evolution    # Document with calculated registrations, from-to
$ sopa history      # Document with calculated validity from-to
$ sopa get          # get document at vt/rt
$ sopa search       # query object type, and expand output with data from relations

Domain commands - Support for uploading new domain commands ?

Other commands:

$ sopa show types  - Show known types with skema name and version for objects that make use of skemaes

