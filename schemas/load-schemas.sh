_7337=`pwd`/../cli/7337.js


# definitions
$_7337 create -s "http://json-schema.org/draft-04/schema#" -i "7337.dk:definitions:uuid" definitions/uuid.schema.json
$_7337 create -s "http://json-schema.org/draft-04/schema#" -i "7337.dk:definitions:urn" definitions/urn.schema.json
$_7337 create -s "http://json-schema.org/draft-04/schema#" -i "7337.dk:definitions:reference" definitions/reference.schema.json
$_7337 create -s "http://json-schema.org/draft-04/schema#" -i "7337.dk:definitions:reference_indexed" definitions/reference_indexed.schema.json
$_7337 create -s "http://json-schema.org/draft-04/schema#" -i "7337.dk:definitions:businessobject" definitions/businessobject.schema.json
$_7337 create -s "application/javascript" -i "7337.dk:definitions:businessobject" definitions/businessobject.rule.js


# Party objects
$_7337 create -s "http://json-schema.org/draft-04/schema#" -i "7337.dk:party:authority" businessobjects/party/authority.schema.json
$_7337 create -s "application/javascript" -i "7337.dk:party:authority" businessobjects/party/authority.rule.js
$_7337 create -s "http://json-schema.org/draft-04/schema#" -i "7337.dk:party:company" businessobjects/party/company.schema.json
$_7337 create -s "application/javascript" -i "7337.dk:party:company" businessobjects/party/company.rule.js
$_7337 create -s "http://json-schema.org/draft-04/schema#" -i "7337.dk:party:person" businessobjects/party/person.schema.json
$_7337 create -s "application/javascript" -i "7337.dk:party:person" businessobjects/party/person.rule.js


# Classification objects
$_7337 create -s "http://json-schema.org/draft-04/schema#" -i "7337.dk:classification:classification" businessobjects/classification/classification.schema.json
$_7337 create -s "application/javascript" -i "7337.dk:classification:classification" businessobjects/classification/classification.rule.js
$_7337 create -s "http://json-schema.org/draft-04/schema#" -i "7337.dk:classification:facet" businessobjects/classification/facet.schema.json
$_7337 create -s "application/javascript" -i "7337.dk:classification:facet" businessobjects/classification/facet.rule.js
$_7337 create -s "http://json-schema.org/draft-04/schema#" -i "7337.dk:classification:class" businessobjects/classification/class.schema.json
$_7337 create -s "application/javascript" -i "7337.dk:classification:class" businessobjects/classification/class.rule.js


# Organisation objects
$_7337 create -s "http://json-schema.org/draft-04/schema#" -i "7337.dk:organisation:unit" businessobjects/organisation/unit.schema.json
$_7337 create -s "application/javascript" -i "7337.dk:organisation:unit" businessobjects/organisation/unit.rule.js
$_7337 create -s "http://json-schema.org/draft-04/schema#" -i "7337.dk:organisation:organisation" businessobjects/organisation/organisation.schema.json
$_7337 create -s "application/javascript" -i "7337.dk:organisation:organisation" businessobjects/organisation/organisation.rule.js
$_7337 create -s "http://json-schema.org/draft-04/schema#" -i "7337.dk:organisation:community" businessobjects/organisation/community.schema.json
$_7337 create -s "application/javascript" -i "7337.dk:organisation:community" businessobjects/organisation/community.rule.js
$_7337 create -s "http://json-schema.org/draft-04/schema#" -i "7337.dk:organisation:function" businessobjects/organisation/function.schema.json
$_7337 create -s "application/javascript" -i "7337.dk:organisation:function" businessobjects/organisation/function.rule.js
$_7337 create -s "http://json-schema.org/draft-04/schema#" -i "7337.dk:organisation:user" businessobjects/organisation/user.schema.json
$_7337 create -s "application/javascript" -i "7337.dk:organisation:user" businessobjects/organisation/user.rule.js
$_7337 create -s "http://json-schema.org/draft-04/schema#" -i "7337.dk:organisation:itsystem" businessobjects/organisation/itsystem.schema.json
$_7337 create -s "application/javascript" -i "7337.dk:organisation:itsystem" businessobjects/organisation/itsystem.rule.js


# domain objects
$_7337 create -s "http://json-schema.org/draft-04/schema#" -i "7337.dk:composite:engagement" businessobjects/domain/engagement.schema.json
$_7337 create -s "application/javascript" -i "7337.dk:composite:engagement" businessobjects/domain/engagement.rule.js
