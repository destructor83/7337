# Load example data for all datatypes
#
#
_7337=`pwd`/../cli/7337.js

# Party objects
$_7337 create -s "7337.dk:party:person" -i "Kaj" examples/person-01-create.json
$_7337 create -s "7337.dk:party:person" -i "Kurt" examples/person-02-create.json
$_7337 create -s "7337.dk:party:person" -i "Kirsten" examples/person-03-create.json
$_7337 create -s "7337.dk:party:person" -i "Karina" examples/person-04-create.json

$_7337 create -s "7337.dk:party:authority" -i "KL" examples/authority-01-create.json
$_7337 create -s "7337.dk:party:authority" -i "KOMBIT" examples/authority-02-create.json
$_7337 create -s "7337.dk:party:authority" -i "Københavns kommune" examples/authority-03-create.json
$_7337 create -s "7337.dk:party:authority" -i "Kolding kommune" examples/authority-04-create.json

$_7337 create -s "7337.dk:party:company" -i "KMD" examples/company-01-create.json
$_7337 create -s "7337.dk:party:company" -i "Kajs auto" examples/company-02-create.json
$_7337 create -s "7337.dk:party:company" -i "Kirstens klinik" examples/company-03-create.json
$_7337 create -s "7337.dk:party:company" -i "7337" examples/company-04-create.json


# Classifikation objects
$_7337 create -s "7337.dk:classification:classification" -i "KLE" examples/classification-01-create.json

$_7337 create -s "7337.dk:classification:facet" -i "Emne" examples/facet-01-create.json
$_7337 create -s "7337.dk:classification:facet" -i "Handling" examples/facet-02-create.json

$_7337 create -s "7337.dk:classification:class" -i "25" examples/class-25-create.json
$_7337 create -s "7337.dk:classification:class" -i "25.00" examples/class-25.00-create.json
$_7337 create -s "7337.dk:classification:class" -i "27" examples/class-27-create.json
$_7337 create -s "7337.dk:classification:class" -i "27.00" examples/class-27.00-create.json


# Organisation objects
$_7337 create -s "7337.dk:organisation:organisation" -i "kommune" examples/organisation-01-create.json

$_7337 create -s "7337.dk:organisation:unit" -i "00" examples/unit-00-create.json
$_7337 create -s "7337.dk:organisation:unit" -i "01" examples/unit-01-create.json
$_7337 create -s "7337.dk:organisation:unit" -i "02" examples/unit-02-create.json
$_7337 create -s "7337.dk:organisation:unit" -i "01.01" examples/unit-01.01-create.json
$_7337 create -s "7337.dk:organisation:unit" -i "01.02" examples/unit-01.02-create.json
$_7337 create -s "7337.dk:organisation:unit" -i "02.01" examples/unit-02.01-create.json

# Has a unit as parent
$_7337 create -s "7337.dk:organisation:organisation" -i "havn" examples/organisation-02-create.json

$_7337 create -s "7337.dk:organisation:unit" -i "03.01" examples/unit-03.01-create.json
$_7337 create -s "7337.dk:organisation:unit" -i "03.02" examples/unit-03.02-create.json
$_7337 create -s "7337.dk:organisation:unit" -i "03.03" examples/unit-03.03-create.json
$_7337 create -s "7337.dk:organisation:unit" -i "03.03.01" examples/unit-03.03.01-create.json
$_7337 create -s "7337.dk:organisation:unit" -i "03.03.02" examples/unit-03.03.02-create.json

$_7337 create -s "7337.dk:organisation:function" -i "01" examples/function-01-create.json
$_7337 create -s "7337.dk:organisation:function" -i "02" examples/function-02-create.json
$_7337 create -s "7337.dk:organisation:function" -i "03" examples/function-03-create.json

$_7337 create -s "7337.dk:organisation:itsystem" -i "01" examples/itsystem-01-create.json
$_7337 create -s "7337.dk:organisation:itsystem" -i "02" examples/itsystem-02-create.json

$_7337 create -s "7337.dk:organisation:user" -i "01" examples/user-01-create.json
$_7337 create -s "7337.dk:organisation:user" -i "02" examples/user-02-create.json
$_7337 create -s "7337.dk:organisation:user" -i "03" examples/user-03-create.json

$_7337 create -s "7337.dk:organisation:community" -i "01" examples/community-01-create.json
