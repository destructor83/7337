var merge = require('merge');

// List of relations and the legal object types they can reference with UUID
// Alter and return
exports.relation_types = function(types) {
  return merge(types, {
    "parent.uuid":          ["7337.dk:classification:class"],
    "facet.uuid":           ["7337.dk:classification:facet"],
    "owner.uuid":           ["7337.dk:organisation:organisation", "7337.dk:organisation:unit", "7337.dk:organisation:function", "7337.dk:organisation:itsystem", 
                             "7337.dk:organisation:user", "7337.dk:organisation:community", "7337.dk:party:person"],
    "responsible.uuid":     ["7337.dk:organisation:organisation", "7337.dk:organisation:unit", "7337.dk:organisation:function", "7337.dk:organisation:itsystem", 
                             "7337.dk:organisation:user", "7337.dk:organisation:community", "7337.dk:party:person"],
    "editor.(.*).uuid":     ["7337.dk:organisation:organisation", "7337.dk:organisation:unit", "7337.dk:organisation:function", "7337.dk:organisation:itsystem", 
                             "7337.dk:organisation:user", "7337.dk:organisation:community", "7337.dk:party:person"],
    "law.(.*).uuid":                ["7337.dk:classification:class"],
    "mapping.(.*).uuid":            ["7337.dk:classification:class"],
    "addition.(.*).uuid":           ["7337.dk:classification:class"],
    "replace.(.*).uuid":            ["7337.dk:classification:class"],
    "valid_combination.(.*).uuid":  ["7337.dk:classification:class"]
  });
};

// Alter others rules and/or setup new ones
// Array of rule functions
exports.rules = function(rules) {
  return merge(rules, {});
};