var _ = require('underscore');
var merge = require('merge');

// List of relations and the legal object types they can reference with UUID
// Alter and return
exports.relation_types = function(types) {
  return merge(types, {
    "parent.uuid":          ["7337.dk:organisation:unit"],
    "associated.(.*).uuid": ["7337.dk:organisation:unit", "7337.dk:organisation:function", "7337.dk:organisation:itsystem", 
                             "7337.dk:organisation:user", "7337.dk:organisation:community", "7337.dk:party:person"],
    "owner.uuid":           ["7337.dk:organisation:organisation", "7337.dk:organisation:unit", "7337.dk:organisation:function", "7337.dk:organisation:itsystem", 
                             "7337.dk:organisation:user", "7337.dk:organisation:community", "7337.dk:party:person"],
    "responsible.uuid":     ["7337.dk:organisation:organisation", "7337.dk:organisation:unit", "7337.dk:organisation:function", "7337.dk:organisation:itsystem", 
                             "7337.dk:organisation:user", "7337.dk:organisation:community", "7337.dk:party:person"]
  });
};

// Alter others rules and/or setup new ones
// Array of rule functions
exports.rules = function(rules) {
  return merge(rules, {
    mandatory_owner: rule1
  });
};

// All objects has to have a key and it is mandatory
function rule1(doc, verbose) {
  return new Promise(function(resolve, reject) {
    if (_.isUndefined(doc.owner)) {
      return reject({statusCode: 404, statusMessage: "Classification rule: The mandatory relation: owner is undefined"});
    }
    if (_.isNull(doc.owner)) {
      return reject({statusCode: 404, statusMessage: "Classification rule: The mandatory relation: owner is null"});
    }
    return resolve(doc);
  });
}