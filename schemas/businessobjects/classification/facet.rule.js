var merge = require('merge');

// List of relations and the legal object types they can reference with UUID
// Alter and return
exports.relation_types = function(types) {
  return merge(types, {
    "owner.uuid":           ["7337.dk:organisation:organisation", "7337.dk:organisation:unit", "7337.dk:organisation:function", "7337.dk:organisation:itsystem", 
                             "7337.dk:organisation:user", "7337.dk:organisation:community", "7337.dk:party:person"],
    "responsible.uuid":     ["7337.dk:organisation:organisation", "7337.dk:organisation:unit", "7337.dk:organisation:function", "7337.dk:organisation:itsystem", 
                             "7337.dk:organisation:user", "7337.dk:organisation:community", "7337.dk:party:person"],
    "editor.(.*).uuid":     ["7337.dk:organisation:organisation", "7337.dk:organisation:unit", "7337.dk:organisation:function", "7337.dk:organisation:itsystem", 
                             "7337.dk:organisation:user", "7337.dk:organisation:community", "7337.dk:party:person"],
    "classification.uuid":  ["7337.dk:classification:classification"]
  });
};

// Alter others rules and/or setup new ones
// Array of rule functions
exports.rules = function(rules) {
  return merge(rules, {});
};