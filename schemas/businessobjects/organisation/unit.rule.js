var merge = require('merge');

// List of relations and the legal object types they can reference with UUID
// Alter and return
exports.relation_types = function(types) {
  return merge(types, {
    "parent.uuid":          ["7337.dk:organisation:unit"],
    "associated.(.*).uuid": ["7337.dk:organisation:unit", "7337.dk:organisation:function", "7337.dk:organisation:itsystem", 
                             "7337.dk:organisation:user", "7337.dk:organisation:community", "7337.dk:party:person"],
    "owner.uuid":           ["7337.dk:organisation:organisation"],
    "residence.(.*).uuid":  ["7337.dk:base:address"],
    "contact.(.*).uuid":    [],
    "industry.uuid":        ["7337.dk:classification:class"],
    "task.(.*).uuid":       ["7337.dk:classification:class"],
    "type.uuid":            ["7337.dk:classification:class"],
    "production.uuid":      ["7337.dk:party:company"],
    "tax.uuid":             ["7337.dk:party:company"],
    "employee.(.*).uuid":   ["7337.dk:party:person"]
  });
};

// Alter others rules and/or setup new ones
// Array of rule functions
exports.rules = function(rules) {
  return merge(rules, {});
};