var merge = require('merge');

// List of relations and the legal object types they can reference with UUID
// Alter and return
exports.relation_types = function(types) {
  return merge(types, {
    "associated.(.*).uuid": ["7337.dk:organisation:unit", "7337.dk:organisation:function", "7337.dk:organisation:itsystem", 
                             "7337.dk:organisation:organisation", "7337.dk:organisation:community", "7337.dk:party:person"],
    "owner.uuid":           ["7337.dk:organisation:organisation"],
    "residence.(.*).uuid":  ["7337.dk:base:address"],
    "contact.(.*).uuid":    [],
    "task.(.*).uuid":       ["7337.dk:classification:class"],
    "usertype.(.*).uuid":   ["7337.dk:classification:class"],
    "type.uuid":            ["7337.dk:classification:class"]
  });
};

// Alter others rules and/or setup new ones
// Array of rule functions
exports.rules = function(rules) {
  return merge(rules, {});
};