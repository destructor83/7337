var _ = require("underscore");
var merge = require("merge");

// List of relations and the legal object types they can reference with UUID
exports.relation_types = function(types) {
  return merge(types, {});
};

// Alter others rules and/or setup new ones
exports.rules = function(rules) {
  return merge(rules, {
    mandatory_key: rule1
  });
};

// All objects has to have a key and it is mandatory
function rule1(doc, verbose) {
  return new Promise(function(resolve, reject) {
    if (_.isUndefined(doc.key)) {
      return reject({statusCode: 404, statusMessage: "Businessobject rule: The mandatory field: key is undefined"});
    }
    if (_.isNull(doc.key)) {
      return reject({statusCode: 404, statusMessage: "Businessobject rule: The mandatory field: key is null"});
    }
    return resolve(doc);
  });
}