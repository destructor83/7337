if [ -e /tmp/login.json ]
then
  export access_token=`cat /tmp/login.json | jq -r '.access_token'`
  curl -H "Authorization: Bearer ${access_token}" -H "Content-Type: application/json" -H 'context: {"domain": "base", "extension": "los"}' -X POST -d @$1 https://test.reflective.dk/api/object/add
else
  curl -H "Content-Type: application/json" -H 'context: {"domain": "base", "chain": "los", "extension": "los"}' -X POST -d @$1 http://object:8080/add
fi
