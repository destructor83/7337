if [ -e /tmp/login.json ]
then
  export access_token=`cat /tmp/login.json | jq -r '.access_token'`
  curl -H "Authorization: Bearer ${access_token}" -H "Content-Type: application/json" -H 'context: {"domain": "base"}' -X POST -d '{"matches": {"schema": "http://reflective.dk/kmd/los/unit"}}' https://test.reflective.dk/api/index/query
else
  curl -H "Content-Type: application/json" -H 'context: {"domain": "base", "chain": "los", "extension": "los"}' -X POST -d '{"matches": {"schema": "http://reflective.dk/kmd/los/unit"}}' http://index:8080/query
fi