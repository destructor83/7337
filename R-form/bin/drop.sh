if [ -e /tmp/login.json ]
then
  export access_token=`cat /tmp/login.json | jq -r '.access_token'`
  curl -H "Authorization: Bearer ${access_token}" -H "Content-Type: application/json" -H 'context: {"domain": "base"}' -X POST -d "{\"domain\": \"base\", \"extension\": \"$1\"}"  https://test.reflective.dk/api/winch/drop
else
  curl -H "Content-Type: application/json" -H 'context: {"domain": "base"}' -X POST -d "{\"domain\": \"base\", \"extension\": \"$1\"}"  http://winch:8080/drop
fi