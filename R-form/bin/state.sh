if [ -e /tmp/login.json ]
then
  export access_token=`cat /tmp/login.json | jq -r '.access_token'`
  curl -H "Authorization: Bearer ${access_token}" -H "Content-Type: application/json" -H 'context: {"domain": "base", "extension": "los"}' -X GET  https://test.reflective.dk/api/winch/state
else
  curl -H "Content-Type: application/json" -X GET  http://winch:8080/state
fi

