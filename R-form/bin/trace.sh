if [ -e /tmp/login.json ]
then
  export access_token=`cat /tmp/login.json | jq -r '.access_token'`
  curl -H "Authorization: Bearer ${access_token}" -H "Content-Type: application/json" -H 'context: {"domain": "base", "extension": "los"}' -X POST -d "{\"objects\": [{\"id\": \"$1\"}]}" https://test.reflective.dk/api/index/trace
else
  curl -H "Content-Type: application/json" -H 'context: {"domain": "base"}' -X POST -d "{\"objects\": [{\"id\": \"$1\"}]}" http://index:8080/trace
fi