var smalltreedata = [{
  id: "root",
  value: "Films data",
  open: true,
  data: [{
      id: "1",
      open: true,
      value: "The Shawshank Redemption",
      data: [
        { id: "1.1", value: "Part 1", lat: 56.9527847, lng: 8.6912995 },
        {
          id: "1.2",
          value: "Part 2",
          data: [
            { id: "1.2.1", value: "Page 1" },
            { id: "1.2.2", value: "Page 2" },
            { id: "1.2.3", value: "Page 3" },
            { id: "1.2.4", value: "Page 4" },
            { id: "1.2.5", value: "Page 5" }
          ]
        },
        { id: "1.3", value: "Part 3", lat: 57.9537847, lng: 8.6913995 }
      ]
    },
    {
      id: "2",
      open: true,
      value: "The Godfather",
      data: [
        { id: "2.1", value: "Part 1" },
        { id: "2.2", value: "Part 2" }
      ]
    }
  ]
}];

webix.ui({
  type: "clean",
  rows: [{
    id: "changeOrganisation",
    view: "form",
    type: "clean",
    elements: [
      {
        view: "combo",
        required: true,
        labelAlign: "right",
        label: "Vælg",
        labelWidth: "100",
        placeholder: "en organisation",
        options: [
          { id: 1, value: "Den Administrative Organisation" },
          { id: 2, value: "Den Politisk Organisation" },
          { id: 3, value: "AM Organisationen" },
          { id: 4, value: "Projekt ABC" }
        ]
      },
      {
        type: "clean",
        rows: [{
          view: "tabview",
          cells: [{
              header: "Klade",
              body: {
                type: "clean",
                rows: [{
                    view: "tree",
                    select: 'multiselect',
                    drag: true,
                    data: smalltreedata
                  },
                  {
                    type: "clean",
                    rows: [{
                      id: "http://reflective.dk/person",
                      view: "form",
                      elements: [
                        { template: "Afdeling", type: "section" },
                        {
                          view: "text",
                          id: "name",
                          label: "Navn",
                          labelWidth: "110",
                          labelAlign: "right",
                          disabled: false
                        },
                        {
                          cols: [{
                              view: "text",
                              id: "key",
                              label: "Afd. id",
                              labelWidth: "110",
                              labelAlign: "right",
                              disabled: false
                            },
                            {
                              view: "checkbox",
                              id: "state",
                              label: "Aktiv",
                              labelWidth: "110",
                              labelAlign: "right",
                              value: "aktiv",
                              uncheckValue: "inaktiv",
                              checkValue: "aktiv",
                              disabled: false
                            },
                          ]
                        },
                        {
                          view: "combo",
                          required: true,
                          id: "enhed",
                          icon: "sitemap",
                          label: "Overordnet",
                          labelWidth: "110",
                          labelAlign: "right",
                          popup: "tree_pop",
                        },
                        {
                          view: "fieldset",
                          label: "KLE",
                          body: {
                            rows: [{
                                view: "multicombo",
                                label: "Opgaver",
                                labelAlign: "right",
                                labelWidth: 100,
                                options: [
                                  { "id": 1, "value": "Personale" },
                                  { "id": 2, "value": "Økonomi" },
                                  { "id": 3, "value": "Fagligt" }
                                ]
                              },
                              {
                                view: "multicombo",
                                label: "Afgrænsninger",
                                labelAlign: "right",
                                labelWidth: 100,
                                options: [
                                  { "id": 1, "value": "Personale" },
                                  { "id": 2, "value": "Økonomi" },
                                  { "id": 3, "value": "Fagligt" }
                                ]
                              },
                              {
                                view: "multicombo",
                                label: "Information",
                                labelAlign: "right",
                                labelWidth: 100,
                                options: [
                                  { "id": 1, "value": "Personale" },
                                  { "id": 2, "value": "Økonomi" },
                                  { "id": 3, "value": "Fagligt" }
                                ]
                              },
                            ]
                          }
                        }
                      ]
                    }]
                  }
                ]
              }

            },
            {
              header: "Ændringer",
              body: {
                view: "tree",
                select: 'multiselect',
                drag: true,
                data: smalltreedata
              }
            },
          ]
        }]
      }
    ]
  }]
}).show({ id: 1, value: "template" }, { x: 0, y: 0 });
