webix.ui({
  "view": "form",
  "elements": [{
      view: "fieldset",
      label: "Stedfortræder for",
      body: {
        rows: [{
            view: "text",
            required: true,
            label: "CPR på leder",
            labelAlign: "right",
            labelWidth: "100"
          },
          {
            view: "text",
            label: "Navn",
            readonly: true,
            labelAlign: "right",
            labelWidth: "100"
          }
        ]
      },
    },
    {
      view: "fieldset",
      label: "Stedfortræder",
      body: {
        rows: [{
            view: "text",
            required: true,
            label: "CPR",
            labelAlign: "right",
            labelWidth: "100"
          },
          {
            view: "text",
            label: "Navn",
            readonly: true,
            labelAlign: "right",
            labelWidth: "100"
          },
          {
            view: "combo",
            label: "Ansat i",
            labelAlign: "right",
            labelWidth: "100",
            value: "B&U Staben",
            options: ["B&U Staben", "Børnehaven Kolbøtten"]
          },
          {
            view: "multicombo",
            label: "Stedfortræder i",
            labelAlign: "right",
            labelWidth: "100",
            value: "1",
            options: [
              { "id": 1, "value": "B&U Staben" },
              { "id": 2, "value": "Børnehaven Kolbøtten" }
            ]
          },
          {
            view: "combo",
            label: "Type",
            labelAlign: "right",
            labelWidth: "100",
            value: "Beslutning",
            options: ["Beslutning", "Administrativ"]
          },
          { view: "datepicker", required: true, label: "Gældende fra", labelAlign: "right", labelWidth: "100", name: "end", stringResult: true, format: "%d.%m.%y" },
          {
            cols: [{
                view: "label",
                label: "Ansvar",
                width: 100,
                labelAlign: "right",
                labelWidth: "100",
              },
              //{ template:"Ansvar",width:100,height:200,borderless:true }, // here you place any component you like
              {
                view: "tree",
                borderless: true,
                height: "200",
                label: "Leder for",
                labelAlign: "right",
                labelWidth: "100",
                select: true,
                multiselect: true,
                data: [{
                  id: "root",
                  value: "Ansvar",
                  open: true,
                  data: [{
                      id: "1",
                      open: true,
                      value: "Personale",
                      data: [
                        { id: "1.1", value: "MUS" },
                        { id: "1.2", value: "IT-tildeler" },
                        { id: "1.3", value: "Ny medarbejder" }
                      ]
                    },
                    {
                      id: "2",
                      open: true,
                      value: "Økonomi",
                      data: []
                    },
                    {
                      id: "3",
                      open: true,
                      value: "Fagligt",
                      data: []
                    }
                  ]
                }]
              }
            ]
          }
        ]
      },
    },
    {
      view: "button",
      label: "Ok"
    },
  ]
});
