webix.ui({
  type: "clean",
  rows: [{
    id: "http://reflective.dk/person",
    view: "form",
    elements: [
      { template: "Opret organisation", type: "section" },
      {
        cols: [{
            view: "combo",


            required: true,
            labelAlign: "right",
            label: "Type",
            labelWidth: "100",
            options: [
              { id: 1, value: "Administrative Organisation" },
              { id: 2, value: "Projekt Organisation" },
              { id: 3, value: "AM Organisation" },
              { id: 4, value: "Politisk Organisation" }
            ]
          },
          {
            id: "name",
            view: "text",
            name: "name",
            required: true,
            label: "Navn",
            labelAlign: "right",
            labelWidth: "100"
          }
        ]
      },
      {
        view: "checkbox",
        id: "state",
        label: "Aktiv",
        labelWidth: "110",
        labelAlign: "right",
        value: "aktiv",
        uncheckValue: "inaktiv",
        checkValue: "aktiv",
        disabled: false
      }
    ]
  }]
}).show({ id: 1, value: "template" }, { x: 0, y: 0 });
