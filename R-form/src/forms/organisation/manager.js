webix.ui({
    "view": "form",
    "elements": [
      {
        view: "fieldset",
        label: "Person",
        body: {
          rows: [{
              view: "text",
              required: true,
              label: "CPR",
              labelAlign: "right",
              labelWidth: "100"
            },
            {
              view: "text",
              label: "Navn",
              readonly: true,
              labelAlign: "right",
              labelWidth: "100"
            },
            {
              view: "combo",
              label: "Ansat i",
              labelAlign: "right",
              labelWidth: "100",
              value: "B&U Staben",
              options: ["B&U Staben", "Børnehaven Kolbøtten"]
            },
          ]
        },
      },
      {
        view: "fieldset",
        label: "Ledelse",
        body: {
          rows: [{
              cols: [
                { template: "Leder for", width: 100, height: 400, borderless: true, align: "right" }, // here you place any component you like
                {
                  view: "tree",
                  borderless: true,

                  label: "Leder for",
                  labelWidth: 100,
                  select: true,
                  multiselect: true,
                  data: [{
                    id: "root",
                    value: "Xkøbing kommune",
                    open: true,
                    data: [{
                        id: "1",
                        open: true,
                        value: "Børn og Unge forvaltningen",
                        data: [
                          { id: "1.1", value: "Staben" },
                          { id: "1.2", value: "Dagtilbud" },
                          { id: "1.3", value: "Skoler" }
                        ]
                      },
                      {
                        id: "2",
                        open: true,
                        value: "Økonomi og Arbejdsmarkedsforvaltningen",
                        data: [
                          { id: "2.1", value: "HR" },
                          { id: "2.2", value: "Økonomi" }
                        ]
                      }
                    ]
                  }]
                }

              ]
            },
            {
              view: "combo",
              label: "Type",
              labelAlign: "right",
              labelWidth: "100",
              value: "Leder",
              options: ["Direktør", "Chef", "Leder"]
            },
            {
              view: "multicombo",
              label: "Ansvar",
              labelAlign: "right",
              labelWidth: "100",
              value: "1,2,3",
              options: [
                { "id": 1, "value": "Personale" },
                { "id": 2, "value": "Økonomi" },
                { "id": 3, "value": "Fagligt" }

              ]
            },
            { view: "datepicker", required: true, label: "Gældende fra", labelAlign: "right", labelWidth: "100", name: "end", stringResult: true, format: "%d.%m.%y" },
          ]
        },
      },
      {
        view: "button",
        label: "Ok"
      },
    ]
  }


);
