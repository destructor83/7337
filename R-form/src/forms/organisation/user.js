var stillinger = ["A/n plejehjemsass.", "Afd.leder døgninst.", "Afd.leder m souschef funktion", "Afdelingsbibliotekar", "Afdelingsleder", "Afspændingspædagog", "Agronom", "Akademiingeniør", "Andre chefer", "Arkitekt", "Ass. halinspektør", "Ass. leder", "Assistent", "Afdelingsleder", "Afdelingsleder og udvalgssekretær", "Afsnitsleder", "Afsnitsleder for integration", "Analysekonsulent", "Arbejdsmarkeds- og voksendirektør", "Ass. centerleder", "Bac.scient.cons", "Bachelor", "Beredskabsassistent", "Beredskabschef", "Beredskabsinspektør", "Beredskabsmester", "Beskæftigelsesvejl.", "Bibliot./mellemlede.", "Bibliot./specialist", "Bibliotekar", "Biblioteksleder", "Bogopsætter", "Borgmester", "Brandmand", "Brandpersonale", "Byggetekniker", "Bygningskonstruktør", "Beskæftigelsesrådgiver", "Bolig - repatriering", "Bygherrerådgiver", "Børne-og kulturchef", "Børnebibliotekar", "Børnehavekl.leder", "Cand scient", "Cand. it", "Cand. oecon.", "Cand. scient", "Cand. scient.pol", "Cand. scient.soc", "Cand. techn. soc.", "Cand.ling.merc.", "Cand.mag", "Cand.merc.", "Cand.phil", "Cand.phil.", "Cand.pæd", "Cand.scient.adm.", "Cand.scient.bibl.", "Centerleder", "Chef", "Chefkonsulent", "Civilingeniør", "Civiløkonom", "Callcenter", "Centerleder", "Centerleder rosengården", "Chef for affald og genbrug", "Chef for seniorservice", "Controller", "Dagcenterl/daghj.led", "Dagplejeassistent", "Dagplejepædagog", "Dagplejer", "Datamatiker/datanom", "Deltidsbrandmand", "Diplomingeniør", "Distriktsleder", "Demenskonsulent", "Diplomingeniør", "Distriksleder", "Distriktsleder", "Distriktstandlæge", "Driftsassistent", "Driftsleder", "Døgninst.medhjælper", "Edb-assistent", "Edb-personale", "Egu-elev", "Ekspeditionssekretær", "Elev", "Elev over 21 år", "Ergoterapeut", "Ern-og hush.øko.m.fl", "Ernæringsass.elev", "Ernæringsassistent", "Ergoterapeut", "Erhvervsvejl.", "Ernæringsassistent", "Faglært", "Faglært holdleder", "Familiepleje", "Familieplejekonsul.", "Filialklinikleder", "Foresp.nye lønninger", "Formand", "Forstander", "Forstkandidat", "Forvaltningschef", "Frivillig", "Fuldmægtig", "Fysioterapeut", "Fagl. koord.", "Faglig konsulent", "Faglig koordinator", "Faglig områdeleder", "Fam.plej.-0103015030", "Fam.plej.-1008970390", "Familiekonsulent", "Fleks og ledighedsydelse", "Forebyggende hjemmebesøger", "Forhandlingskonsulent", "Formand", "Formand for folkeoplysningsudvalget", "Fuldmægtig", "Førtidspension", "Gartner m. v.", "Gis-medarbejder", "Grafisk designer", "Grafisk formidler", "Gæstedagplejer", "Hal/stadioninspektør", "Handicapledsager", "Havneassistent", "Havnechef", "Havnefoged/mester", "Heltidsbesk. leder", "Hjemmehjælper", "Hjemmevejleder", "Holdleder", "Hortonom", "Hr-konsulent", "Husassistent", "Håndværker", "Ikke udd. personale", "Individuel aflønning", "Informatikass.", "Instr.fysioterapeut", "Instruktør/holdleder", "It-konsulent", "It-medarbejder", "Idrætspilot", "Indkøbskonsulent", "Informatikassistent", "Instruktør", "Jobkons./vejleder", "Jobkons/vejleder", "Jordbrugsteknolog", "Journalist", "Jurist", "Jobcenterchef", "Kantineleder", "Klinisk underviser", "Klubass. u/udd.", "Klubassistent", "Klubmedarb. ej udd.", "Kok", "Komm.best.medl.", "Kommunaldirektør", "Kommunallæge", "Konsulent", "Kontaktperson", "Kontorassistent", "Kontorelev", "Kontorelev o/25 år", "Kostfaglig eneansvar", "Kostfaglig leder", "Kulturtekniker", "Kommunikationschef", "Kommunikationskonsulent", "Konsulent", "Kostkonsulent", "Kultur- og udviklingschef", "Landinspektør", "Landskabsark", "Led børnebibliotekar", "Led. adm. stilling", "Led. skolepsykolog", "Led. sundh.plejerske", "Led. sygeplejerske", "Led.adm.stilling", "Led.hal/stadioninsp.", "Led.sygeplejerske", "Ledende beskæft.vejl", "Ledende ergoterapeut", "Ledende fys.terapeut", "Ledende pædagog", "Ledende sundhedspl.", "Ledende økonoma", "Leder", "Leder/mellemled/spec", "Ledende ssp-konsulent", "Leder af drift- og supportafdelingen", "Leder af rådhusservice", "Leder af uu", "Lonkl_tekst", "Lystekniker", "Lærer", "Lærer m/særlige kval", "Lærer/overlærer", "Lægekonsulent", "Lønkonsulent", "Magister", "Maskinmester", "Mellemleder", "Miljø-/lab.tekniker", "Miljøingeniør", "Musikskoleleder", "Musikskolelærer", "Mediegrafiker", "Miljømedarbejder", "Musikterapeut", "Naturformidler", "Nævnsmedlem", "Områdeleder", "Omsorgs-/pæd.medhj.", "Omsorgs-/pædagog mhj", "Omsorgsmedhjælper", "Overassistent", "Overassistent (f)", "Overlærer", "Overtandlæge", "Overassistent", "Parkeringsav./kontr.", "Piccoline", "Planlægger", "Plejehjemsassistent", "Plejer", "Plejer /bofællesskab", "Produktionsteknolog", "Professionsbachelor", "Projektkoordinator", "Projektleder", "Psykolog", "Pers.hj.", "Pers.jur konsulent", "Personalekonsulent", "Produktionsleder", "Projektleder", "Projektmedarbejder", "Pæd. medhj.", "Pæd. medhj./ass.", "Pæd.ass-/pguelev o18", "Pæd.medhj.", "Pæd.medhj./ass.", "Pæd.mehj.", "Pædagog", "Pædagogisk assistent", "Pædagogisk konsulent", "Pædagogisk medhjælp", "Pædagogmedhjælper", "Pædagogstuderende", "Pædagogisk vejleder", "Rengøringsassistent", "Regnskabsspecialist", "Råd-nævn-kommiss.", "Sagsbehandler", "Serviceassistent", "Skibsfører", "Skolekonsulent", "Skolepsykolog", "Skolerengøringsass.", "Skov-/landskabsing.", "Skovbruger", "Soc.- og sundh.ass.", "Soc.- og sundh.hjælp", "Soc.-og sundhedsass.", "Soc.-sundh.ass.elev", "Soc./sundh.hj.elev", "Socialformidler", "Socialpsyk. medarb.", "Socialpæd.konsulent", "Socialpædagog", "Socialrådgiver", "Souschef", "Souschef/afd.leder", "Specialarbejder", "Specialist", "Specialist it", "Specialkonsulent", "Speciallægekonsulent", "Specialtandlæge", "Stedfort. for forst.", "Stedfortr.inst.leder", "Stedfortræder", "Studentermedhjælp", "Støtte i familie", "Støttepædagog", "Sundhedsmedhjælper", "Sundhedsplej.stud", "Sundhedsplejerske", "Sygehjælper", "Sygeplejerske", "Sagsbehandlende visitator", "Sagsbehandler", "Sekretariatsleder", "Skovarbejder", "Social pædagog", "Socialfaglig medarbejder", "Souschef", "Specialkonsulent", "Stabschef", "Sundhedschef", "Sundhedskonsulent", "Tandklinikassistent", "Tandlæge", "Tandlægekonsulent", "Tandplejer", "Tandtekniker", "Tekn. servicechef", "Tekn. serviceleder", "Tekn. servicemedarb.", "Teknikumingeniør", "Teknisk chef", "Teknisk designer", "Teknisk serviceleder", "Tilsynsassistent", "Timelønnet leder", "Timelønnet lærer", "Teamkoordinator", "Teamleder", "Tolk + ad hoc", "Trafikplanlægger", "Trivlseskonsulent", "Turisme- og marketingkoordinator", "Turismechef", "Udd.ejendomsserv.tek", "Udvalgsformand", "Ufuldst. ansættelse", "Ungarbejder", "Ungdomsskoleinsp.", "Uu-vejleder - koordinator", "Uud.hjemmevejleder", "Uuddannet lærer", "Uddannelseskonsulent", "Udviklingskonsulent", "Udviklingsmedarbejder", "Ungekonsulent", "Vejleder", "Viceberedskabschef", "Viceberedskabsmester", "Viceborgmester", "Viceskoleinspektør", "Visitator", "Voksenelev", "Voksenelev o/25 år", "Virksomhedskonsulent", "Visitator", "Voksne", "Værkstedsassistent", "Værkstedsleder", "Webredaktør", "Assisterende centerleder", "Centerleder", "Distriktsleder", "Ergoterapeut", "Foreb. sygeplejerske", "Hjemmevejleder", "Kloakchef", "Primærsygepl sundhedsklinikken", "Reng.leder", "Sundhedsvejleder", "Uu-vejleder", "Økonom", "Økonoma", "Øvelsesskolelærer", "Økonomi og personalemedarbejder", "Økonomi- & administrationsdirektør", "Økonomichef", "Økonomikonsulent", "Økonomimedarbejder", "Økonomisk kordinator", "Økonomisk/adm. koordinato"];
var cpr = {
  "040766": { firstname: "Michael", lastname: "Nielsen" },
  "280378": { firstname: "Jes", middlename: "Rønnow", lastname: 'Lungskov' }
};

var smalltreedata = [{
  id: "root",
  value: "Films data",
  open: true,
  data: [{
      id: "1",
      open: true,
      value: "The Shawshank Redemption",
      data: [
        { id: "1.1", value: "Part 1", lat: 56.9527847, lng: 8.6912995 },
        {
          id: "1.2",
          value: "Part 2",
          data: [
            { id: "1.2.1", value: "Page 1" },
            { id: "1.2.2", value: "Page 2" },
            { id: "1.2.3", value: "Page 3" },
            { id: "1.2.4", value: "Page 4" },
            { id: "1.2.5", value: "Page 5" }
          ]
        },
        { id: "1.3", value: "Part 3", lat: 57.9537847, lng: 8.6913995 }
      ]
    },
    {
      id: "2",
      open: true,
      value: "The Godfather",
      data: [
        { id: "2.1", value: "Part 1" },
        { id: "2.2", value: "Part 2" }
      ]
    }
  ]
}];





webix.ui({
  type: "clean",
  rows: [{
    id: "http://reflective.dk/itsystem",
    view: "form",
    elements: [
      { template: "Bruger", type: "section" },
      {
        view: "combo",
        required: true,
        label: "IT System",
        labelAlign: "right",
        labelWidth: 100,
        options: [
          { "id": 1, "value": "Organisation" },
          { "id": 2, "value": "SBSYS" },
          { "id": 3, "value": "OPUS" }
        ]
      },
      {
        view: "text",
        id: "name",
        required: true,
        label: "Navn",
        labelWidth: "100",
        labelAlign: "right",
        disabled: false
      },
      {
        cols: [{
            view: "text",
            id: "key",
            label: "Bruger id",
            labelWidth: "100",
            labelAlign: "right",
            disabled: false
          },
          {
            view: "checkbox",
            id: "state",
            label: "Aktiv",
            labelWidth: "100",
            labelAlign: "right",
            value: "aktiv",
            uncheckValue: "inaktiv",
            checkValue: "aktiv",
            disabled: false
          },
        ]
      },
      {
        view: "fieldset",
        label: "Bruger i afdeling",
        body: {
          rows: [{
            view: "multitext",
            required: true,
            label: " ",
            labelWidth: "90",
            labelAlign: "right",
            popup: "tree_pop",
            value: ''
          }]
        }
      },
      {
        view: "multicombo",
        label: "Forretningsroller",
        labelAlign: "right",
        labelWidth: 100,
        options: [
          { "id": 1, "value": "Personale" },
          { "id": 2, "value": "Økonomi" },
          { "id": 3, "value": "Fagligt" }
        ]
      },
      {
        view: "multicombo",
        label: "Systemroller",
        labelAlign: "right",
        labelWidth: 100,
        options: [
          { "id": 1, "value": "Personale" },
          { "id": 2, "value": "Økonomi" },
          { "id": 3, "value": "Fagligt" }
        ]
      },
    ]
  }]
}).show({ id: 1, value: "template" }, { x: 0, y: 0 });

webix.ui({
  view: "popup",
  id: "tree_pop",
  body: {
    id: "tree",
    view: "tree",
    select: true,
    data: webix.copy(smalltreedata)
  }
}).hide();
