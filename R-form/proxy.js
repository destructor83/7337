var proxy = require('express-http-proxy');
var express = require('express');
var app = express();

app.use('/api/*', proxy('http://process:8080/', {
  proxyReqPathResolver: function(request) {
    var operation = request.params['0'];

    return new Promise(function (resolve, reject) {
      var resolvedPathValue = "http://process:8080/" + operation;
      resolve(resolvedPathValue);
    });
  }
}));
app.use('/', express.static('deploy'));
app.listen(8080);