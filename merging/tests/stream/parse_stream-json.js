var moment = require("moment");
var makeSource = require("stream-json");
var async = require("async");
var _ = require("lodash");

var fs = require("fs");
var path = 'merging/tests/examples/';



var start = ((new Date()).getTime());
function readIt(callback) {
  var keyValues = 0;
  var objectCounter = 0;
  var registrationsActive = false;
  var getFromActive = false;
  var validReg = false;
  var keyToFind = "parent";
  var validityStarted = false;
  var keyToFindActive = false;
  var theValue = undefined;
  var parseEnded = false;
  var source = makeSource();
  source.on("keyValue", function(value){
    if (parseEnded === false) {
      if (registrationsActive) {
        if (value === "from") {
          console.log("from found");
          getFromActive = true;
        }
        if (validityStarted && value === keyToFind) {
          console.log("found key:", keyToFind);
          keyToFindActive = true;
        }
        if (value === "validity") {
          validityStarted = true;
        }
      } else {
        if (value === "registrations") {
          console.log("registrations started"); 
          registrationsActive = true;
        }
      }
      if (keyToFindActive === true) {
        console.log("kayValue inside value:", value);
      }
    }
  });
  source.on("stringValue", function(value){
    if (parseEnded === false) {
      if (getFromActive === true) {
        if (moment(value).isSameOrBefore(date)) {
          console.log("registration valid, continuing");
          getFromActive = false;
          validReg = true;
        } else {
          console.log("registration not valid, stop parsing it");
        }
      }
      if (keyToFindActive === true) {
        console.log("stringValue inside value:", value);
      }
    }
  });
  var endOnObject = false;
  source.on("startObject", function(value){
    if (parseEnded === false) {
      if (keyToFindActive === true) {
        console.log("started object for value");
        endOnObject = true;
      }
    }
  });
  source.on("endObject", function(value){
    if (parseEnded === false) {
      if (keyToFindActive === true && endOnObject) {
        console.log("ended object for value, thereby ending parsing");
        callback();
        readstream.close();
        readstream.destroy();
        parseEnded = true;
      }
    }
  });
  
  var readstream = fs.createReadStream(path + 'unit-updated-data.json');
  readstream.pipe(source.input);
};
//TODO skip to next registration if current is not valid
var date = moment();


async.each(_.range(100), function(result, callback) {
  var json = require('../examples/unit-updated-data.json');
console.dir(json);callback();
  //readIt(callback);
  
}, function(error) {
    if(error) {
      console.error(error);
    } else {
      var end = ((new Date()).getTime());
      console.log('It took (ms).: ' + (end - start));
    }
}, function (error) {
  if (error) {
    console.error(error);
  }
});

