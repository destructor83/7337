var moment = require("moment");
var oboe = require("oboe");
var async = require("async");
var _ = require("lodash");

var fs = require("fs");
var path = 'merging/tests/examples/';


var date = moment('2016-08-10');
var currentFrom = false;

var start = ((new Date()).getTime());
function readIt(callback) {
  var readstream = fs.createReadStream(path + 'unit-updated-data.json');
  oboe(readstream)
  .on('node', {
    'registrations.*.from': function(from){
      currentFrom = from;
      console.log('currentFrom', from);
    },
    'registrations.*.validity.input.parent': function(parent){
      if (moment(currentFrom).isSameOrBefore(date)) {
        console.log("'the parent:");
        console.dir(parent);
        this.abort();
        readstream.destroy();
        callback();
      }
    }   
  })
  .on('done', function(){
    console.log("done");
  })
  .on('fail', function(){
    console.log("failed");   
  });
}
async.each(_.range(1), function(result, callback) {
  readIt(callback);
}, function(error) {
    if(error) {
      console.error(error);
    } else {
      var end = ((new Date()).getTime());
      console.log('It took (ms).: ' + (end - start));
    }
}, function (error) {
  if (error) {
    console.error(error);
  }
});
