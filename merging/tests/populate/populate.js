var path = require("path");
var jsonfile = require('jsonfile');
var _ = require('underscore');
var fse = require("fs-extra");
var create = require('../../../models/create.js');

var width = 2;
var depth = 3;

var repoPath = "../../../../test_space/populate/";
var abspath = path.resolve(__dirname, repoPath) + "/";

jsonfile.spaces = 2;
var writefile = function (result, callback) {
  var filename = abspath + result.data.uuid + ".json";
  jsonfile.writeFileSync(filename, result.data);
};

fse.remove(abspath, function (err) {
  fse.ensureDir(abspath, function (err) {
    generate();
  });
});

var organization_uuid = "59bad0e1-6604-5abc-b1a9-5b70cc9cba8b";
var orgdata = {
  "key" : "organization-1",
  "name" : "TheOrg",
  "state" : "active"
};



var generate = function () {
  var orgargs = { 
    input: orgdata,
    validFrom: '2016-07-10T00:00:00.000Z',
    type: 'organization',
    comment: "Opret tingen",
    actor: undefined, //"urn:pladderfisk",
    note: "note OG kommentar?",
    id: "main_organization"
  };
  create.it(orgargs).then(function(result) {
    writefile(result);
  }, function (error) {
    console.log(error);
  });
  
  createunit(undefined, undefined, width, depth);
};
var createunit = function (prefix, parent, width, depth) {
  if (depth > 0) {
    for (var i = 0; i < width;i++) {
      var name;
      if (_.isUndefined(prefix)) {
        name = "unit-" + i;
      } else {
        name = "unit-" + prefix + "-" + i;
      }
      var unitdata = {
        "name" : "unit",
        "state" : "active",
        "owner": {
          "uuid": organization_uuid
        }
      };
      unitdata.key = unitdata.name = name;
      //console.log(prefix + " " + parent);
      if (_.isUndefined(parent) === false) {
        unitdata.parent = {"uuid": parent};
      }
      var unitargs = { 
        input: unitdata,
        vf: '2016-07-10T00:00:00.000Z',
        //vt: '2016-07-10T00:00:00.000Z',
        type: 'unit',
        comment: "Opret tingen",
        actor: undefined, //"urn:pladderfisk",
        note: "note OG kommentar?",
        id: unitdata.key
      };

      callcreate(unitargs, prefix, i, depth);
    }
  }
};

var callcreate = function(unitargs, prefix, count, depth) {
    create.it(unitargs).then(function(result) {
      var new_prefix = _.isUndefined(prefix) ? count : prefix + "-" + count;
      createunit(new_prefix, result.data.uuid, width, depth - 1);

      writefile(result, function (error) {
        if (error) {
         console.error(error.stack);
        }
      });
  }, function (error) {
    if (error) {
      console.error(error.stack);
    }
  });
}