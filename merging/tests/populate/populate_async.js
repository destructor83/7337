var path = require("path");
var async = require("async");
var jsonfile = require('jsonfile');
jsonfile.spaces = 2;
var math = require("math");
var merge = require("merge");
var _ = require('underscore');
var fse = require("fs-extra");
var create = require('../../../models/create.js');
var start = ((new Date()).getTime());

var width = 3;
var depth = 3;
var maxFiles = 8100;
var repoPath = "../../../../test_space/populate/";
var abspath = path.resolve(__dirname, repoPath) + "/";

var results = [];
var addFile = function (result) {
  var filename = abspath + result.data.uuid + ".json";
  results.push({filename: filename, data: result.data});
};

fse.remove(abspath, function (error) {
  if (error) {
    console.error(error);
  } else {
    fse.ensureDir(abspath, function (error) {
      if (error) {
        console.error(error);
      } else {
        generate();
      }
    });
  }
});

var organization_uuid = "59bad0e1-6604-5abc-b1a9-5b70cc9cba8b";
var unitdata = {
  "name" : "unit",
  "state" : "active",
  "owner": {
    "uuid": organization_uuid
  }
};
var unitargs = { 
  input: unitdata,
  vf: '2016-07-10T00:00:00.000Z',
  schema: 'unit',
  comment: "Opret tingen",
  actor: undefined, //"urn:pladderfisk",
  note: "note OG kommentar?"
};
var writeFiles = function () {
  console.log(results.length);
  async.eachLimit(results, maxFiles, function(result, callback) {
    jsonfile.writeFile(result.filename, result.data, function (error) {
      if (error) {
        callback(error);
      } else {
        callback();
      }
    });
  }, function(error) {
      if(error) {
        console.error(error);
      } else {
        var end = ((new Date()).getTime());
        console.log('All files have been processed successfully, it took (ms).: ' + (end - start));
      }
  }, function (error) {
    if (error) {
      console.error(error);
    }
  });
};
var generate = function () {
  var expectedLeaves = math.pow(width, depth);
  var callbackCount = 0;
  createunit(undefined, undefined, width, depth, function () {
    callbackCount++;
    if (callbackCount === expectedLeaves) {
      writeFiles();
    }
  });
};

var createunit = function (prefix, parent, width, depth, callback) {
  if (depth > 0) {
    for (var i = 0; i < width;i++) {
      var name = _.isUndefined(prefix) ? "unit-" + i : "unit-" + prefix + "-" + i;
      var args = merge.recursive({}, unitargs);
      args.id = args.input.key = args.input.name = name;
      if (_.isUndefined(parent) === false) {
        args.input.parent = {"uuid": parent};
      }
      args.input = JSON.stringify(args.input);
      callcreate(args, prefix, i, depth, callback);
    }
  } else {
    callback();
  }
};

var callcreate = function(unitargs, prefix, count, depth, callback) {
  create.it(unitargs).then(function(result) {
    var new_prefix = _.isUndefined(prefix) ? count : prefix + "-" + count;
    createunit(new_prefix, result.data.uuid, width, depth - 1, callback);
    addFile(result);
  }, function (error) {
    if (error) {
      console.error(error);
    }
  });
};