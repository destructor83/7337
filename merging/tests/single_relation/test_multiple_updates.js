var assert = require('chai').assert;
var path = require("path");
var jq = require("json-query");
var bitemporal = require("../../../helpers/bitemporal");
var _ = require('underscore');
var fs = require("fs");
var query = require("../../query");

var registrations;
var path = '';
path = 'merging/tests/single_relation/';

describe('Test of merging logic', () => {

  before(() => {
    registrations = []
    
    registrations.push(JSON.parse(fs.readFileSync(path + 'create.json', 'utf8')));
    registrations.push(JSON.parse(fs.readFileSync(path + 'update.1.json', 'utf8')));
    registrations.push(JSON.parse(fs.readFileSync(path + 'update.2.json', 'utf8')));
  });

  describe('merging actual', () => {
    it('should have the latest registered values', () => {
      registrations.sort(bitemporal.compareRegistrations);
      
      var date = new Date('2016-07-10T00:00:00.000Z');
      var result = query.get(registrations, "*", date);
      
      assert.equal(result.class.opgave["0"].reference.uuid, 'a5700d6c-7aaf-477d-aca8-28ae5b6023cd');
      assert.equal(result.class.opgave["1"].reference.uuid, '0cc01158-40f3-44cb-9537-15f7fe07d6e0');
    });
  });
});