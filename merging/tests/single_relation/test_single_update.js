var assert = require('chai').assert;
var path = require("path");
var jq = require("json-query");
var bitemporal = require("../../../helpers/bitemporal");
var _ = require('underscore');
var fs = require("fs");
var query = require("../../query");
var moment = require("moment");

var registrations;
var path = '';
path = 'merging/tests/single_relation/';

describe('Test of merging logic', () => {

  before(() => {
    registrations = []
    var create = JSON.parse(fs.readFileSync(path + 'create.json', 'utf8'));
    var update = JSON.parse(fs.readFileSync(path + 'update.1.json', 'utf8'));
    registrations.push(update);
    registrations.push(create);
    assert.equal(registrations[0].registration.rid, "73d5379e-052a-4aa6-885c-5e608865cb91", "latest registration first");
    assert.equal(registrations[1].registration.rid, "717f09ec-ebf4-4131-8871-75be9c18f4d4", "earliest registration last");
  });

  describe('parsing registrations', () => {
    it('sort should put registrations in correct order', (done) => {
      registrations.sort(bitemporal.compareRegistrations);
      assert.equal(registrations[0].registration.rid, "717f09ec-ebf4-4131-8871-75be9c18f4d4", "earliest registration first");
      assert.equal(registrations[1].registration.rid, "73d5379e-052a-4aa6-885c-5e608865cb91", "latest registration last");
      done();
    });
      
    it('should trace a change in /businessobjects/organization/<uuid>#class.opgave.0', () => {
      registrations.sort(bitemporal.compareRegistrations);
      
      var result = query.trace(registrations, "class.opgave.0");

      assert.equal(result.length, 2);
      
      assert.equal(result[0].references[0].reference.uuid, 'c5a7f3e0-5696-409b-abc7-9b79ca9f33f8');
      assert.equal(result[0].validity.from, '2016-07-04T00:00:00.000Z');

      assert.equal(result[1].references[0].reference.uuid, 'a5700d6c-7aaf-477d-aca8-28ae5b6023cd');
      assert.equal(result[1].validity.from, '2016-07-06T00:00:00.000Z');
    });
  });
    
  describe('get actual values', () => {
    it('should get different values for different dates', () => {
      registrations.sort(bitemporal.compareRegistrations);
      
      var date = new Date('2016-07-04T00:00:00.000Z');
      var result = query.get(registrations, "*", date);

      assert.equal(result.class.opgave["0"].reference.uuid, 'c5a7f3e0-5696-409b-abc7-9b79ca9f33f8');

      date = new Date('2016-07-06T00:00:00.000Z');
      result = query.get(registrations, "*", date);
      
      assert.equal(result.class.opgave["0"].reference.uuid, 'a5700d6c-7aaf-477d-aca8-28ae5b6023cd');
      
      date = new Date('2016-07-01T00:00:00.000Z');
      result = query.get(registrations, "*", date);
      
      assert.ok(_.isUndefined(result));
    });
  });
  
  describe('get evolution', () => {
    it('should have registered one value change /businessobjects/organization/<uuid>#class.opgave.0', () => {
      registrations.sort(bitemporal.compareRegistrations);
      
      var date = new Date('2016-07-04T00:00:00.000Z');
     
      var result = query.evolution(registrations, "class.opgave.0", date);

      assert.equal(result.length, 2, "should have evolved once");
      
      assert.ok(moment(result[0].validity.from).isSame("2016-07-04T00:00:00.000Z"));
      assert.ok(moment(result[0].validity.to).isSame("2016-07-06T00:00:00.000Z"));
      assert.ok(moment(result[1].validity.from).isSame("2016-07-06T00:00:00.000Z"));
      assert.ok(_.isUndefined(result[1].validity.to));
      
      console.log(result);
      assert.equal(result[0].value.class.opgave[0].reference.uuid, "c5a7f3e0-5696-409b-abc7-9b79ca9f33f8");
      assert.equal(result[1].value.class.opgave[0].reference.uuid, "a5700d6c-7aaf-477d-aca8-28ae5b6023cd");

    });
  });
});