var path = require("path");
var jq = require("json-query");
var jsonfile = require('jsonfile');
var _ = require('underscore');
var moment = require("moment");
var async = require("async");
var fs = require("fs");
var fse = require("fs-extra");
var query = require("../../query");
var uuid = require("../../../helpers/uuid");

var repoPath = "../../../../test_space/populate/";
var abspath = path.resolve(__dirname, repoPath) + "/";

var before_read = (new Date()).getTime();

var entities = {};
var dataer = [];
var files = fs.readdirSync(abspath);
var maxFiles = 8100;
var callbackCount = 0;
var renderYet = function (error) {
  callbackCount++;
  if (callbackCount === files.length) {
    var end = ((new Date()).getTime());
    console.log("Number of objects.: " + files.length);
    console.log("Total took (secs): " + (end - before_read) / 1000);
  }
  if (error) {
    console.error(error);
  }
  return;
};
async.eachLimit(files, maxFiles, function(file, callback) {
  fs.readFile(abspath + file, function (error, data) {
    if (error) {
      renderYet(error);
    } else {
      dataer.push(data);
      renderYet();
    }
    return callback();
  });
});
