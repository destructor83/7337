var path = require("path");
var jq = require("json-query");
var jsonfile = require('jsonfile');
var _ = require('underscore');
var moment = require("moment");
var async = require("async");
var fs = require("fs");
var fse = require("fs-extra");
var query = require("../../query");
var uuid = require("../../../helpers/uuid");

var repoPath = "../../../../test_space/populate/";
var abspath = path.resolve(__dirname, repoPath) + "/";

var before_read = (new Date()).getTime();

var entities = {};
var outgoing_relations = {};
var files = fs.readdirSync(abspath);
var maxFiles = 8100;

async.eachLimit(files, maxFiles, function(file, callback) {
  jsonfile.readFile(abspath + file, function (error, object) {
    if (error) {
      return callback(error);
    } else {
      if (_.isUndefined(entities[object.type]) === true) {
        entities[object.type] = {};
      }
      entities[object.type][object.uuid] = object;
      //add outgoing relation
      if (_.isUndefined(object.registrations[0].validity.input.parent) === false) {
        var parent_uuid = object.registrations[0].validity.input.parent.uuid;
        if (_.isUndefined(outgoing_relations[parent_uuid])) {
          outgoing_relations[parent_uuid] = [];
        }
        outgoing_relations[parent_uuid].push(object.uuid);
      }
      return callback();
    }
  });
}, function afterLoad() {
  render();
});

var output = {};
function render() {
  var after_read = (new Date()).getTime();
  
  var render_children = function (parent_uuid, children) {
    for (var unit_key in outgoing_relations[parent_uuid]) {
      var unit = entities.unit[outgoing_relations[parent_uuid][unit_key]];
      var parent = unit.registrations[0].validity.input.parent;
  
      if (_.isUndefined(parent) === false && parent.uuid === parent_uuid) {
        children[unit.uuid] = {name:unit.registrations[0].validity.input.name, children: {}};
        render_children(unit.uuid, children[unit.uuid].children);
      }
    }
    return;
  };
  
  var before_roots = (new Date()).getTime();

  for (var unit_key in entities.unit) {
    var unit = entities.unit[unit_key];
    if (_.isUndefined(unit.registrations[0].validity.input.parent)) {
      output[unit.uuid] = {name:unit.registrations[0].validity.input.name, children: {}};
      render_children(unit_key, output[unit.uuid].children);
    }
  }
  //console.dir(output);
  console.log("Number of objects.: " + files.length);
  var end = ((new Date()).getTime());
  console.log("Total took (secs): " + (end - before_read) / 1000);
  console.log("Read took (secs): " + (after_read - before_read) / 1000);
  console.log("Root discover took (secs): " + (end - before_roots) / 1000);
  return;
};