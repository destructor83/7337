var JSONStream = require('JSONStream');
var _ = require('underscore');
var fse = require("fs-extra");
var es = require('event-stream')

var filePathStr = "merging/tests/hierarchy/7b5993fb-fe9a-4e28-95d0-e8721567d6db.json";
var readStream = fse.createReadStream(filePathStr);

// This will wait until we know the readable stream is actually valid before piping
readStream.on('open', function () {
  // This just pipes the read stream to the response object (which goes to the client)
  readStream.pipe(JSONStream.parse('registrations.*')).pipe(es.mapSync(function (data) {
      console.dir(data)
      return data
  }));
});
