var path = require("path");
var jq = require("json-query");
var jsonfile = require('jsonfile');
var _ = require('underscore');
var moment = require("moment");
var fs = require("fs");
var fse = require("fs-extra");
var query = require("../../query");
var uuid = require("../../../helpers/uuid");

var repoPath = "../../../../test_space/populate/";
var abspath = path.resolve(__dirname, repoPath) + "/";

var before_read = (new Date()).getTime();

var entities = {};
var files = fs.readdirSync(abspath);
for (var file in files) {
  //console.log(abspath + "/" + files[file]);
  var obj = jsonfile.readFileSync(abspath + "/" + files[file]);

  if (_.isUndefined(entities[obj.type]) === true) {
    entities[obj.type] = {};
  }
  entities[obj.type][obj.uuid] = obj;
}

var render_children = function (parent_uuid, indent) {
  var output = "";
  for (var unit_key in entities.unit) {
    var unit = entities.unit[unit_key];
    var parent = unit.registrations[0].validity.input.parent;

    if (_.isUndefined(parent) === false && parent.uuid === parent_uuid) {
      var tabs = "";
      for (var i = 0; i < indent; i++) {
        tabs = tabs + "\t";
      }
      output = output + tabs + unit.registrations[0].validity.input.name + "\n";
      output = output + render_children(unit_key, indent + 1);
    }
  }
  return output;
};

var after_read = (new Date()).getTime();
var root_units = [];
var output = "";
for (var unit_key in entities.unit) {
  var unit = entities.unit[unit_key];
  if (_.isUndefined(unit.registrations[0].validity.input.parent)) {
    root_units.push(unit);
    output = output + unit.registrations[0].validity.input.name + "\n"
    output = output + render_children(unit_key, 1);
  }
}
console.log(output);
var end = ((new Date()).getTime());
console.log("Total took (secs): " + (end - before_read) / 1000)
console.log("Read took (secs): " + (after_read - before_read) / 1000) 