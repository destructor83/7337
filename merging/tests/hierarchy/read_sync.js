var path = require("path");
var jsonfile = require('jsonfile');
var _ = require('underscore');
var fs = require("fs");

var repoPath = "../../../../test_space/populate/";
var abspath = path.resolve(__dirname, repoPath) + "/";

var before_read = (new Date()).getTime();

var entities = {};
var files = fs.readdirSync(abspath);
for (var file in files) {
  var obj = jsonfile.readFileSync(abspath + files[file]);
  entities[obj.uuid] = obj;
}
console.log("Number of objects.: " + files.length);
var end = ((new Date()).getTime());
console.log("Total took (secs): " + (end - before_read) / 1000);
