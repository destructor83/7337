var path = require("path");
var jsonfile = require('jsonfile');
var _ = require('underscore');
var fs = require("fs");

var repoPath = "../../../../test_space/populate/";
var abspath = path.resolve(__dirname, repoPath) + "/";

var before_read = (new Date()).getTime();

var entities = {};
var outgoing_relations = {};
var files = fs.readdirSync(abspath);
for (var file in files) {
  var obj = jsonfile.readFileSync(abspath + files[file]);

  if (_.isUndefined(entities[obj.type]) === true) {
    entities[obj.type] = {};
  }
  entities[obj.type][obj.uuid] = obj;
  //add outgoing relation
  if (_.isUndefined(obj.registrations[0].validity.input.parent) === false) {
    var parent_uuid = obj.registrations[0].validity.input.parent.uuid;
    if (_.isUndefined(outgoing_relations[parent_uuid])) {
      outgoing_relations[parent_uuid] = [];
    }
    outgoing_relations[parent_uuid].push(obj.uuid);
  }
}
var after_read = (new Date()).getTime();

var render_children = function (parent_uuid, indent) {
  var output = "";
  for (var unit_key in outgoing_relations[parent_uuid]) {
    var unit = entities.unit[outgoing_relations[parent_uuid][unit_key]];
    var parent = unit.registrations[0].validity.input.parent;

    if (_.isUndefined(parent) === false && parent.uuid === parent_uuid) {
      var tabs = "";
      for (var i = 0; i < indent; i++) {
        tabs = tabs + "\t";
      }
      output = output + tabs + unit.registrations[0].validity.input.name + "\n";
      output = output + render_children(unit.uuid, indent + 1);
    }
  }
  return output;
};

var before_roots = (new Date()).getTime();
var root_units = [];
var output = "";
for (var unit_key in entities.unit) {
  var unit = entities.unit[unit_key];
  if (_.isUndefined(unit.registrations[0].validity.input.parent)) {
    root_units.push(unit);
    output = output + unit.registrations[0].validity.input.name + "\n"
    output = output + render_children(unit_key, 1);
  }
}
console.log(output);
console.log("Number of objects.: " + files.length);
var end = ((new Date()).getTime());
console.log("Total took (secs): " + (end - before_read) / 1000);
console.log("Read took (secs): " + (after_read - before_read) / 1000);
console.log("Root discover took (secs): " + (end - before_roots) / 1000);