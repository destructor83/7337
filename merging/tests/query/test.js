var assert = require('chai').assert;
var path = require("path");
var jq = require("json-query");
var _ = require('underscore');
var fs = require("fs");
var query = require("../../query");
var moment = require("moment");

var path = '';
path = 'merging/tests/query/';

describe('Querying JSON', () => {
  describe('finding properties from selector', () => {
    it('lookup should correctly find property', () => {
      var create = JSON.parse(fs.readFileSync(path + 'create.json', 'utf8'));
      var selector = "class.opgave[0]";
      var res = query.select(create.input, selector);
      assert.equal(res.references[0].reference.uuid, 'c5a7f3e0-5696-409b-abc7-9b79ca9f33f8');
    });
    it('finding properties without index', () => {
      var create = JSON.parse(fs.readFileSync(path + 'create.json', 'utf8'));
      var selector = "name";
      var res = query.select(create.input, selector);
      assert.equal(res.value, 'Axapoint sw');
    });
    it('not finding property', () => {
      var create = JSON.parse(fs.readFileSync(path + 'create.json', 'utf8'));
      var selector = "foobar";
      var res = query.select(create.input, selector);
      assert.ok(_.isUndefined(res.value));
    });
  });
});


og hvis seneste reg lc=removed eller passivated, så ingen get
- Hvis seneste reg er 'removed', så returner ikke objekt
- Hvis seneste reg er 'passivated', så returner ikke objekt