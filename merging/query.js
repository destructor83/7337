var jq = require("json-query");
var _ = require('underscore');
var merge = require('merge');
var moment = require('moment');

var select = (object, selector) => {
  var matches = selector.match(/\[(.*)\]/g);
  if(_.isNull(matches) === false && matches.length > 0) {
    var index = matches[0];
    var propertyName = selector.replace(index, '');
    index = index.substring(1, index.length - 1);
    return jq(propertyName + '[index=' + index + ']', {
      data: object
    });
  } else {
    return jq(selector, {
      data: object
    });
  }
};

var select_registrations = (registrations, selector) => {
  var matches = selector.match(/\[(.*)\]/g);
  if(_.isNull(matches) === false && matches.length > 0) {
    var index = matches[0];
    var propertyName = selector.replace(index, '');
    index = index.substring(1, index.length - 1);
    return jq(propertyName + '[*index=' + index + ']', {
      data: registrations
    });
  } else {
    return jq(selector, {
      data: registrations
    });
  }
};
exports.select = select;
exports.select_registrations = select_registrations;

exports.get = (registrations, selector, validDate) => {
  var result = undefined;
  for (var i = 0;i < registrations.length;i++) {
    var registration = registrations[i]
    var selected = select(registration.input, selector);
    if (_.isUndefined(selected.references) === false) {
      if (moment(registration.validity.from).isSameOrBefore(validDate)) {
        if (_.isUndefined(result)) {
          result = registration.input;
        } else {
          result = merge.recursive(result, registration.input);
        }
      }
    }
  }
  return result;
};

exports.trace = (registrations, selector) => {
  var results = [];
  for (var i = 0;i < registrations.length;i++) {
    var registration = registrations[i]
    var selected = select(registration.input, selector);
    if (_.isUndefined(selected.references) === false) {
      var row = {};
      row.references = selected.references;
      row.validity = registration.validity;
      results.push(row);
    }
  }
  return results;
};
/**
 * Registrations should be ordered.
 **/
exports.evolution = (registrations, selector) => {
  var result = [];
  for (var i = 0;i < registrations.length;i++) {
    var registration = registrations[i];
    var row= {};
    
    var selected = select(registration.input, selector);
    if (_.isUndefined(selected.references) === false) {
      row.value = registration.input;
      console.log(registration.input.class.opgave[0].reference.uuid);
      row.validity = registration.validity;
      //end previous interval, if it was openended.
      if (i > 0 && _.isUndefined(result[i -1].validity.to)) {
        result[i -1].validity.to = registration.validity.from;
      }
      result.push(row);
    }
  }
  return result;
};