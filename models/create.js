var _ = require('underscore');
var moment = require('moment');
var node_uuid = require('node-uuid');

var uuid = require('../helpers/uuid');
var schema = require('../helpers/schema');
var rules = require('../helpers/rules');


exports.it = function(args) {

  return validate_args(args)  // Also JSON parsing
  .then(schema.validate)
  .then(prepareCreate)
  .then(rules.validate)
  .then(completeCreate);
};

function prepareCreate(args) {
  return new Promise(function(resolve, reject) {
    var now = new Date().toISOString();
    var hrTime = process.hrtime();
    var now_nano = hrTime[0] + "" + hrTime[1];
    var entity = {};

    if (_.isUndefined(args.id)) {
      entity.uuid = node_uuid.v4();
    } else {
      entity.uuid = uuid.v5(args.schema+args.id); // id unique per object type
    }
    entity['$schema'] = args.schema;

    var registration = {};
    registration.id = uuid.v5(now_nano);
    registration.user = {};
    registration.user.urn = "XXX:oauth2 cli integration missing";

    if (args.comment) {
      registration.comment = args.comment;
    }

    registration.from = now;
    registration.operation = 'create';
    registration.lifecycle = 'local';

    registration.validity = {};
    if (_.isUndefined(args.validFrom) === false) {
      registration.validity.from = args.validFrom;
    }
    if (_.isUndefined(args.validTo) === false) {
      registration.validity.to = args.validTo;
    }
    if (_.isUndefined(args.note) === false) {
      registration.validity.note = args.note;
    }
    if (_.isUndefined(args.actor) === false) {
      registration.validity.actor = {};
      if (args.actor.startsWith("uuid:")) {
        // XXX: check that object exists and is one of the 6 organisations types
        registration.validity.actor.uuid = args.actor.substring(5);
      } else if (args.actor.startsWith("urn:")) {
        registration.validity.actor.urn = args.actor.substring(4);
      }
    }
    registration.validity.input = args.input;

    entity.registrations = [];
    entity.registrations.push(registration);
    args.entity = entity;

    return resolve(args);
  });
}

function completeCreate(args) {
  return new Promise(function(resolve, reject) {

    return resolve({statusCode: 201, statusMessage: "Object with UUID=" + args.entity.uuid + " created", data: args.entity});
  });
}

function validate_args(args) {
  return new Promise(function(resolve, reject) {

    if (_.isUndefined(args.id)) {
      args.id = undefined;
    } else if (args.schema === 'application/javascript') { 
      args.id = '¶' + args.id;
    }
    if (_.isUndefined(args.comment)) {
      args.comment = undefined;
    }
    if (_.isUndefined(args.note)) {
      args.note = undefined;
    }
    if (_.isUndefined(args.actor)) {
      args.actor = undefined;
    }
  
    // check mandatory input
    if (_.isUndefined(args.input) || _.isNull(args.input)) {
      return reject({statusCode: 500, statusMessage: "No data supplied"});
    } else if (args.schema === 'application/javascript') { 
      args.input = args.input.toString();
    } else try {
      args.input = JSON.parse(args.input);
    } catch (error) {
      return reject({statusCode: 500, statusMessage: "could not parse 'input' as string. Can be JSON stream or serialized JSON."});
    }

    if (_.isUndefined(args.schema) || _.isNull(args.schema)) {
      return reject({statusCode: 500, statusMessage: "No schema supplied"});
    }
  
    if (_.isUndefined(args.validFrom) === false) {
      // validate date/time
      var date = moment(args.validFrom);
      if (date.isValid() === false) {
        return reject({statusCode: 500, statusMessage: "'validFrom' argument should be in the format '" + moment().toISOString() + "'"});
      }
    }
  
    if (_.isUndefined(args.validTo) === false) {
      // validate date/time
      date = moment(args.validTo);
      if (date.isValid() === false) {
        return reject({statusCode: 500, statusMessage: "'validTo' argument should be in the format '" + moment().toISOString() + "'"});
      }
    }
  
    if (args.validFrom && args.validTo && moment(args.validTo).isAfter(args.validFrom)) {
      return reject({statusCode: 500, statusMessage: "Interval error: validTo is before validFrom"});
    }

    resolve(args);
  });
}