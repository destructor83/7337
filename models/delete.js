var _ = require('underscore');

var uuid = require('../helpers/uuid');

exports.it = function(args) {
  return new Promise(function(resolve, reject) {
    var now = new Date().toISOString();

    var registration = {};
    registration.id = uuid.v5(now);
    registration.user = {};
    registration.user.urn = "XXX:oauth2 cli integration missing";

    if (_.isUndefined(args.comment) == false) registration.comment = args.comment;

    registration.from = now;
    registration.operation = 'delete';
    registration.lifecycle = 'deleted';

    args.entity.registrations.push(registration);

    return resolve({statusCode: 202, statusMessage: "Object lifecycle set to deleted", data: args.entity});
  });
};