var _ = require('underscore');
var bunyan = require('bunyan');

var logger = require('../../middlewares/logger');

var mongodb = require('../../helpers/mongodb');

// init
var log = bunyan.createLogger({
  name: 'lookup',
  level: 'trace'
});

exports.update = function(params) {
  return new Promise(function(resolve, reject) {
    var ret = {};
    if (_.isUndefined(params) || _.isUndefined(params.tenant)) {
      ret.statusCode = 500;
      ret.statusMessage = "Invalid input";
      reject(ret);
      return;
    }
    if (_.isEmpty(params.tenant)) {
      ret.statusCode = 500;
      ret.statusMessage = "tenant cannot be empty";
      reject(ret);
      return;
    }
    if (!_.isUndefined(params.type) && _.isEmpty(params.type)) {
      ret.statusCode = 500;
      ret.statusMessage = "type cannot be empty";
      reject(ret);
      return;
    }
    if (!_.isUndefined(params.path) && _.isEmpty(params.path)) {
      ret.statusCode = 500;
      ret.statusMessage = "path cannot be empty";
      reject(ret);
      return;
    }
    if (!_.isUndefined(params.rules) && _.isEmpty(params.rules)) {
      ret.statusCode = 500;
      ret.statusMessage = "rules cannot be empty";
      reject(ret);
      return;
    }
    
    var collection = mongodb.get().collection('_lookup');
    logger.trace(log, params, "got mongo collection");

    // Create/update lookup
    collection.findOne({"tenant" : params.tenant}, function(err, entity) {
      if(!err) {
        var lookup = {};
        if (entity !== null) {
          lookup = entity;
        }
        if (_.isUndefined(params.type)) {
          lookup.defaultPath = params.path.split(',');
          ret.statusMessage = 'Default path set to: ' + params.path;
        } else {
          lookup[params.type] = {};
          
          if (!_.isUndefined(params.path)) {
            lookup[params.type].path = params.path.split(',')
          }
          if (!_.isUndefined(params.rules)) {          
            lookup[params.type].rules = params.rules.split(',')
          }
          if (!_.isUndefined(params.schema)) { 
            lookup[params.type].schema = params.schema;
          }          
          ret.statusMessage = 'Lookup data updated for type: ' + params.type;
        }
        
        lookup.tenant = params.tenant;
        if (entity === null) {
          collection.insertOne(lookup);
        } else {
          collection.replaceOne({ "tenant" : params.tenant}, lookup);
        }
        ret.statusCode = 200;
        resolve(ret);
      } else {
        ret.statusCode = 500;
        ret.errors = err;

        reject(err);
      }
    });
  });
};

exports.get = function(params) {
  return new Promise(function(resolve, reject) {
    var ret = {};
    if (_.isUndefined(params) || _.isUndefined(params.tenant)) {
      ret.statusCode = 500;
      ret.statusMessage = "Invalid input";
      reject(ret);
      return;
    }
    if (_.isEmpty(params.tenant)) {
      ret.statusCode = 500;
      ret.statusMessage = "tenant cannot be empty";
      reject(ret);
      return;
    }
    if (!_.isUndefined(params.type) && _.isEmpty(params.type)) {
      ret.statusCode = 500;
      ret.statusMessage = "type cannot be empty";
      reject(ret);
      return;
    }

    var collection = mongodb.get().collection('_lookup');
    logger.trace(log, params, "got mongo collection");

    // Create/update lookup
    collection.findOne({"tenant" : params.tenant}, function(err, entity) {
      var ret = {};
      if(!err) {
        if (entity === null) {
          ret.statusCode = 404;
          ret.statusMessage = 'No lookup data found';
          ret.errors = err;

          reject(ret);
        } else {
          
          ret.statusCode = 200;
          if (_.isUndefined(params.type)) {
            ret.entity = {path: entity.defaultPath.join()};
          } else {
            ret.entity = entity[params.type];
            if (!_.isUndefined(entity[params.type].path)) {
              ret.entity.path = entity[params.type].path.join();
            } else {
              ret.entity.path = entity.defaultPath.join();
            }
          }
          
          resolve(ret);
        }
      } else {
        ret.statusCode = 500;
        ret.errors = err;

        reject(err);
      }
    });
  });
};