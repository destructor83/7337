var _ = require('underscore');
var bunyan = require('bunyan');

var logger = require('../../middlewares/logger');
var mongodb = require('../../helpers/mongodb');
var utils = require('../../helpers/utils');

// init
var log = bunyan.createLogger({
  name: 'schema',
  level: 'trace'
});

exports.update = function(params, schema) {
  return new Promise(function(resolve, reject) {
    var ret = null;

    if ((ret = utils.isUndefined(params, ['path','tenant','namespace','version'])) != null ||
        (ret = utils.isEmpty(params, ['path','tenant','namespace','version'])) != null) {
      reject(ret);
      return;
    }
    ret = {};
    if (!(/^\d\.\d$/.test(params.version) || /^\d\.\d\.\d$/.test(params.version))) {
      // XXX: version has to be: X.X or X.X.X
      ret.statusCode = 500;
      ret.statusMessage = "version has to have the format: #.# or #.#.#";
      reject(ret);
      return;
    }
    if (_.isEmpty(params.path.replace(/\./g, '')) || !/^[a-zA-Z].*/.test(params.path) || !/.*[a-zA-Z]$/.test(params.path)) {
      ret.statusCode = 500;
      ret.statusMessage = "path has start and end with a letter. Dot . can be used as separator";
      reject(ret);
      return;
    }
  
    var collection = mongodb.get().collection('schema');
    logger.trace(log, params, "got mongo collection");

    // Create/update lookup
    collection.findOne({"namespace" : params.namespace, "path" : params.path}, function(err, entity) {
      if(!err) {
        if (entity === null) {
          entity = {};
        }
        
        // XXX: Security scope validation
        
        // Validate
        if (!exports.validateSchema(schema)) {
          reject({statusCode: 400, statusMessage: 'Schema has invalid format'});
          return;
        }
        
        update_refs(schema).then(function(schema) {
          // Update id
          schema.id = params.namespace + ':' + params.path + ':' + params.version;
  
          // Cannot use . in fields 
          var version = params.version.replace(/\./g, '_');
          
          entity.namespace = params.namespace;
          entity.path = params.path;
          entity.tenant = params.tenant;
          entity['version' + version] = JSON.stringify(schema);
  
          if (!params.dryrun) {
            if (entity === null) {
              ret.statusMessage = 'Schema: ' + params.namespace + ':' + params.path + ':' + params.version + " created by tenant: " + params.tenant;
              collection.insertOne(entity);
            } else {
              ret.statusMessage = 'Schema: ' + params.namespace + ':' + params.path + ':' + params.version + ' updated by tenant: ' + params.tenant;
              collection.replaceOne({"namespace" : params.namespace, "path" : params.path}, entity);
            }
          }
          ret.statusCode = 200;
          resolve(ret);
          console.log("DEBUG: ret=" + ret);
        }, function(error) {
          console.log("DEBUG: error=" + error);
          reject(error);
        });
      } else {
        ret.statusCode = 500;
        ret.statusMessage = "UNKNOWN ERROR?";
        ret.errors = err;

        reject(ret);
      }
    });
  });
};

exports.list = function(params) {
  return new Promise(function(resolve, reject) {
    var ret = utils.isUndefined(params, ['tenant']);
    if (ret != null || (ret = utils.isEmpty(params, ['tenant'])) != null) {
      reject(ret);
      return;
    }

    var collection = mongodb.get().collection('schema');
    logger.trace(log, params, "got mongo collection");
    collection.find({}, {_id: false, tenant: false}).toArray(function(err, entities) {
      if(!err) {
        for (var i = 0; i < entities.length; i++) {
          var entity = entities[i];
          
          // Modify schema so that version only contain refs
          for (var key in entity) {
            if (key.toLowerCase().indexOf('version') === 0) {
              var schema = JSON.parse(entity[key]);
              var refs = getRefs(schema, '');
              entity[key] = [];
              for (var n = 0; n < refs.length; n++) {
                entity[key].push({path: refs[n].path, value: refs[n].obj[refs[n].key]});
              }
            }
          }
        }
        console.log("DEBUG result=" + JSON.stringify(entities, null, 2));

        resolve({statusCode: 200, statusMessage: 'Returned list of ' + schema.length, entity: entities});
        return;
      }
    });
  });
};

exports.get = function(params) {
  return new Promise(function(resolve, reject) {
    var ret = utils.isUndefined(params, ['tenant','namespace','path']);
    if (ret != null || (ret = utils.isEmpty(params, ['tenant','namespace','path'])) != null) {
      reject(ret);
      return;
    }
    if (!_.isUndefined(params.version) && !_.isEmpty(params.version) && (!(/^\d\.\d$/.test(params.version) || /^\d\.\d\.\d$/.test(params.version)))) {
      reject({statusCode: 400, statusMessage: 'version has to have the format: #.# or #.#.#'});
      return;
    }
    if (_.isEmpty(params.path.replace(/\./g, '')) || !/^[a-zA-Z].*/.test(params.path) || !/.*[a-zA-Z]$/.test(params.path)) {
      reject({statusCode: 400, statusMessage: 'path has start and end with a letter. Dot . can be used as separator'});
      return;
    }
    
    var collection = mongodb.get().collection('schema');
    logger.trace(log, params, "got mongo collection");
    collection.findOne({"namespace" : params.namespace, "path" : params.path}, function(err, entity) {
      if(!err) {
        if (entity == null) {
          reject({statusCode: 404, statusMessage: "Schema not found: " + params.namespace + ":" + params.path});
          return;
        }

        // XXX: Security scope validation
        
        // Get correct version:
        // no version given: return newest
        // version given: return nearest
        var version = null;
        if (!_.isUndefined(params.version) && !_.isEmpty(params.version)) {
          version = params.version.replace(/\./g, '_');
        }
        if (version == null) {
          version = getLatestVersion(entity);
        } else {
          version = getNearestVersion(entity, version);
        }

        resolve({statusCode: 200, statusMessage: 'Returned version: ' + version, entity: entity['version'+version]});
        return;
      } else {
        reject({statusCode: 500, statusMessage: 'Unknown error', errors: err});
        return;
      }
    });
  });
};

// Check that the schema is valid and can be used for validation of objects
exports.validateSchema = function(schema) {
  return new Promise(function(resolve, reject) {
  // XXX: validate JSON structure
  // XXX: validate that refs have correct structure
  // XXX: validate that dependant schemas exists
  });
}

// Validate object based on schema version
exports.validateObject = function(object) {
  return new Promise(function(resolve, reject) {
  
    // XXX: get object
    
    // XXX: find schema
  
    // XXX: Load schema
    exports.get(params).then(function(result) {
      var schema = result.entity;
      var refs = getRefs(schema, '');
      
      // XXX: get all refs and embed inside 
      
    }, function(error) {
      reject(error);
      return;
    });
  });
  // XXX: validate JSON structure
  // XXX: Validate object against schema
  
  return true;
}


// ---- ----

// Check that all refs are in the DB and add newest versions where no version is given
function update_refs(schema) {
  return new Promise(function(resolve, reject) {
    // Get all $ref's
    var refs = getRefs(schema, '');

    // split up into: namespace, path and version
    for (var i = 0; i < refs.length; i++) {
      if (refs[i].obj['$ref'].split(':').length < 2) {
        reject({statusCode: 400, statusMessage: 'Invalid format: ' + refs[i].obj['$ref'] + ' for: ' + refs[i].path + '.$ref'});
      }
      var namespace = refs[i].obj['$ref'].split(':')[0];
      var path = refs[i].obj['$ref'].split(':')[1];
      var version = refs[i].obj['$ref'].split(':')[2];
      
      var collection = mongodb.get().collection('schema');
  
      // check/find documents based on namespace and path
      collection.findOne({"namespace" : namespace, "path" : path}).then(function(result) {
        if (result == null) {
          reject({statusCode: 404, statusMessage: 'Schema not found: ' + namespace + ':' + path});
          return;
        }

        if (_.isUndefined(version) || _.isEmpty(version)) {
          // Get latest version and add version number to ref
          refs[i].obj['$ref'].concat(':' + getLatestVersion(result));

          // if versions given then check that it exists
        } else if (!_.has(result, 'version' + version)) {
          reject({statusCode: 404, statusMessage: 'Version not found: ' + refs[i].obj['$ref'] + ' for: ' + refs[i].path + '.$ref'});
        }

        // When all is processed with no errors then resolve.
        if (refs.length === (i+1)) {
          resolve(schema);
        }
      }, function(error) {
        reject({statusCode: 500, statusMessage: 'Unknown error'});
        return;
      });
    }
  });
}

function getRefs(obj, path) {
    var refs = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            refs = refs.concat(getRefs(obj[i], path + '.' + i));
        } else if (i == '$ref') {
            refs.push({path: path, key: i,  obj: obj});
        }
    }
    return refs;
}

function getLatestVersion(entity) {
  var version = '';
  for (var i in entity) {
    if (i.indexOf('version') === 0) {
      if (i.substring(7) > version) {
        version = i.substring(7);
      }
    }
  }
  
  return version;
}

function getNearestVersion(entity, version) {
  var versions = [];
  for (var i in entity) {
    if (i.indexOf('version') === 0) {
      versions.push(i.substring(7));
    }
  }

  versions.sort(function(a, b) {
    return a[0].localeCompare(b[0]);
  });

  var nearestVersion = null;
  var regex = new RegExp('^' + version);
  for (var i = 0; i < versions.length; i++) {
    if (versions[i].match(regex)) {
      nearestVersion = versions[i];
    }
  }

  return nearestVersion;
}
