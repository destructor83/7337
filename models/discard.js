var _ = require('underscore');

var uuid = require('../helpers/uuid');
var schema = require('../helpers/schema');
var rules = require('../helpers/rules');
require('../helpers/flatten');


exports.it = function(args) {
  return args_validate(args)  // Also JSON parsing
  .then(schema.validate)
  .then(prepareDiscard)
  .then(rules.validate)
  .then(completeDiscard);
};

function prepareDiscard(args) {
  return new Promise(function(resolve, reject) {
    var now = new Date().toISOString();

    var entity = args.entity;

    // Check that object is in lifecycle that allows update
    var lc = entity.registrations[entity.registrations.length-1].lifecycle;
    if (_.isUndefined(lc) == false && _.isNull(lc) == false && 
       (lc === 'passivated' || lc === 'removed' || lc === 'deleted')) {
  
      return reject({statusCode: 500, statusMessage: "Object with UUID=" + entity.uuid + " has invalid lifecycle: " + lc.lifecycle + " for operation: update"});
    }

    // Registration
    var registration = {};
    registration.id = uuid.v5(now);
    registration.user = {};
    registration.user.urn = "XXX:oauth2 cli integration missing";

    if (args.comment) registration.comment = args.comment;

    registration.from = now;
    registration.operation = 'discard';
    registration.lifecycle = lc;

    // Validity?
    registration.validity = {};
    registration.validity.input = args.input;

    // Now do the discard of data
    var flattenInput = JSON.flatten(args.input);
    for (var i = 0; i < entity.registrations.length; i++) {
      var input = entity.registrations[i].validity.input;

      if (_.isUndefined(input)) continue;
      var flattenERVI = JSON.flatten(input);
      for (var k in flattenInput) {
        if (_.isUndefined(flattenERVI[k])) continue;
        
        // if value on input !null, then only remove if same value, else just remove property
        if (_.isNull(flattenInput[k]) || flattenInput[k] === flattenERVI[k]) {
          delete flattenERVI[k];
        }
      }
      entity.registrations[i].validity.input = JSON.unflatten(flattenERVI);
    }

    entity.registrations.push(registration);
    args.entity = entity;

    return resolve(args);
  });
}

function completeDiscard(args) {
  return new Promise(function(resolve, reject) {
    return resolve({statusCode: 200, statusMessage: "Object with UUID=" + args.entity.uuid + " has had data discarded", data: args.entity});
  });
}

function args_validate(args) {
  return new Promise(function(resolve, reject) {

    if (_.isUndefined(args.comment)) {
      args.comment = false;
    }
  
    // check mandatory input
    if (_.isUndefined(args.input) || _.isNull(args.input)) {
      return reject({statusCode: 500, statusMessage: "No data supplied"});
    } else try {
      args.input = JSON.parse(args.input);
    } catch (e) {
      return reject({statusCode: 500, statusMessage: "Update date is invalid JSON"});
    }

    return resolve(args);
  });
}