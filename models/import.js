var _ = require('underscore');

var uuid = require('../helpers/uuid');
var schema = require('../helpers/schema');
var rules = require('../helpers/rules');

exports.it = function(args) {
  return validate_args(args)  // Also JSON parsing
  .then(schema_validate)
  .then(prepareImport)
  .then(rules_validate)
  .then(completeImport);
};

function prepareImport(args) {
  return new Promise(function(resolve, reject) {
    var now = new Date().toISOString();

    var entity = args.input;

    // Registration
    var registration = {};
    registration.id = uuid.v5(now);
    registration.user = {};
    registration.user.urn = "XXX:oauth2 cli integration missing";

    if (args.comment) registration.comment = args.comment;

    registration.from = now;
    registration.operation = 'import';
    registration.lifecycle = 'imported';

    entity.registrations.push(registration);
    args.entity = entity;

    return resolve(args);
  });
}

function completeImport(args) {
  return new Promise(function(resolve, reject) {
    return resolve({statusCode: 200, statusMessage: "Object with UUID=" + args.entity.uuid + " imported", data: args.entity});
  });
}

function validate_args(args) {
  return new Promise(function(resolve, reject) {

    if (_.isUndefined(args.comment)) {
      args.comment = false;
    }
  
    // check mandatory input
    if (_.isUndefined(args.input) || _.isNull(args.input)) {
      return reject({statusCode: 500, statusMessage: "No data supplied"});
    } else try {
      args.input = JSON.parse(args.input);
    } catch (e) {
      return reject({statusCode: 500, statusMessage: "Invalid JSON input", error: e});
    }

    return resolve(args);
  });
}

function schema_validate(args) {
  // XXX: Validate each registration.validity.input
  return schema.validate(args);
}

function rules_validate(args) {
  // XXX: Run rules after each change
  return rules.validate(args);
}