var _ = require('underscore');
var bunyan = require('bunyan');

var logger = require('../middlewares/logger');

var mongodb = require('../helpers/mongodb');
var amqp = require('../helpers/amqp');
var mysql = require('../helpers/mysql');
var themis = require('../helpers/themis');
var uuid = require('../helpers/uuid');
var registration = require('../helpers/registration');
var document = require('../helpers/document');

// init
var log = bunyan.createLogger({
  name: 'import',
  level: 'trace'
});

exports.execute = function(params, importEntity) {
  return new Promise(function(resolve, reject) {
    var collection = mongodb.get().collection(params.type);
    logger.trace(log, params, "got mongo collection");
  
    // Schema Validate
    themis.validate('/businessobjects/' + params.type, importEntity, function(result) {
      var ret = {};

      if (result.errors.length === 0) {
        logger.trace(log, params, "schema validated");
        collection.findOne({"uuid" : importEntity.uuid}, function(err, entity) {
          if(!err) {
            if (entity !== null && entity.lifecycle === 'local') {
              ret = {statusCode: 422, statusMessage: '', errors: null, uuid: entity.uuid};

              ret.statusCode = 422;
              ret.statusMessage = (dryrun !== null ? "DRYRUN: FAILED " : "FAILED ") + "Object exists and has invalid lifecycle: " + entity.lifecycle;
              reject(ret);
            }
            logger.trace(log, params, "ready to import");

            importEntity.lifecycle = 'imported';
            if (entity === null) { 
              importEntity.lifecycle_registrations = [];
            } else {
              importEntity.lifecycle_registrations = entity.lifecycle_registrations;
            }
            importEntity.lifecycle_registrations[importEntity.lifecycle_registrations.length] = {};
            importEntity.lifecycle_registrations[importEntity.lifecycle_registrations.length-1].registration = registration.new(uuid.generate(), new Date().toISOString(), 'import', 'urn:user:none');
            importEntity = document.cleanup(importEntity, 'registration');

            // XXX: Rule validate - 422 for rules error

            // Add schema version and tenant
            entity._tenant = params.tenant;
            entity._version = params.version;

            // Store
            if (!params.dryrun) {
              if (entity === null) {
                collection.insertOne(importEntity);
              } else {
                collection.replaceOne({ "uuid" : entity.uuid}, importEntity);
              }
              logger.trace(log, params, "document stored in mongo");

              // Add relations to reference cache DB
              mysql.addReferences(params.type, importEntity.uuid, entity);
              logger.trace(log, params, "references updated");

              // Create AMQP message
              delete entity._id;
              importEntity.type = params.type;
              amqp.publish(params.type, "import", importEntity);
              logger.trace(log, params, "amqp message send");
            }

            ret.statusCode = 201;
            ret.uuid = importEntity.uuid;
            
            resolve(ret);
          } else {
            ret.statusCode = 500;
            ret.errors = err;
            
            reject(ret);
          }
        });
      } else {
        // 400 for schema validation error
        ret.statusCode = 400;
        ret.statusMessage = "JSON schema validation error";
        ret.errors = result.errors;
        
        reject(ret);
      }
    });
  });
};