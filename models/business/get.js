var _ = require('underscore');
var bunyan = require('bunyan');

var logger = require('../middlewares/logger');

var uuid = require('../helpers/uuid');
var registration = require('../helpers/registration');
var document = require('../helpers/document');
var bitemporal = require('../helpers/bitemporal');
var object_reader = require('../helpers/object_reader');

// init
var log = bunyan.createLogger({
  name: 'get',
  level: 'trace'
});

exports.execute = function(params) {
  return new Promise(function(resolve, reject) {
    var ret = {};

    var now = new Date();
    var rt = _.isUndefined(params.registrationTime) ? now : new Date(params.registrationTime);
    var vt = _.isUndefined(params.validityTime) ? now : new Date(params.validityTime);      

    object_reader.read(params.type, params.uuid).then(function(data) {
      
      
      
    }, function(error) {
      reject(error);
      return;
    });  
        
        logger.trace(log, params, "got all " + n + "registration");
      
        // XXX: Filter on vt/rt
  
        logger.trace(log, params, "Filtering @( " + rt + " , " + vt + " )");
      
        if(!err) {
          ret = document.is_404_422(entity, ['passivated', 'deleted']);
          if (!_.isEmpty(ret)) {
            reject(ret);
            return;
          }
          logger.trace(log, params, "document found");
          
          entity = document.addZ_and_null(entity);
          entity = filter(entity, rt, vt);
          logger.trace(log, params, "document filtering done");
  
          // remove the _id property
          delete entity._id;
  
          ret.statusCode = 200;
          ret.entity = entity;
          resolve(ret);
        } else {
          ret.statusCode = 500;
          ret.errors = err;
          reject(err);
        }
      }
    });
    });
  });
};

function overlaps(entity, array) {
  var ret = [];
  
  if (array.length === 0) {
    ret.push(entity);
  } else for (var i = 0; i < array.length; i++) {
    var element = array[i];
    
    if (compareDates(entity.validity.from, element.validity.from) > 0 && 
        compareDates(entity.validity.to, element.validity.to) < 0) {
      // element contained? create 2 new elements and add
      var element2 = {};
      element2 = _.clone(element);
      element2.validity = _.clone(element.validity);
      
      element.validity.to = entity.validity.from;
      element2.validity.from = entity.validity.to;
      
      ret.push(element);
      ret.push(entity);
      ret.push(element2);
      
    } else {
      var added = false;
      if (compareDates(entity.validity.from, element.validity.from) < 0 && 
          compareDates(entity.validity.to, element.validity.from) >= 0) {
        // interval overlaps from left, then adjust elements validity.from and add to ret
        element.validity.from = entity.validity.to;
        if (element.validity.from < element.validity.to) {
          ret.push(element);
          added = true;
        }
        if (!_.contains(ret, entity)) ret.push(entity);
      } 
      if (compareDates(entity.validity.from, element.validity.to) < 0 && 
          compareDates(entity.validity.to, element.validity.to) >= 0) {
        // interval overlaps from right, then adjust elements validity.to and add to ret
        element.validity.to = entity.validity.from;
        if (!added && element.validity.from < element.validity.to) {
          ret.push(element);
          added = true;
        }
        if (!_.contains(ret, entity)) ret.push(entity);
      }
      
      // if no overlap, then keep element
      if (!added && compareDates(element.validity.from, element.validity.to) < 0 && 
          (compareDates(entity.validity.to, element.validity.from) <= 0 || compareDates(entity.validity.from, element.validity.to) >= 0)) {
          
        ret.push(element);
      }
    }
  }
  
  return ret;
}

function compareDates(d1, d2) {
  if (d1 === null || d1 === undefined) d1 = "X";
  if (d2 === null || d2 === undefined) d2 = "X";
  if (d1 === d2) return 0;
  
  if (d1 > d2) return 1;
  
  return -1;
}

function filter(entity, rt, vt) {
  
  // Filter so that only relevant entries are left and insert NULL entries
  for (var key in entity) {
    if( Object.prototype.toString.call( entity[key] ) === '[object Array]' && key !== 'lifecycle_registrations') {
      for (var i = 0; i < entity[key].length; i++) {
        // Remove entry?
        if (!bitemporal.insideInterval(entity[key][i].registration, rt) || !bitemporal.insideInterval(entity[key][i].validity, vt)) {
          entity[key][i] = null;
        }
      }
      entity[key] = _.compact(entity[key]);
    }
  }
  
  // Filter out on z value
  for (var key in entity) {
    if( Object.prototype.toString.call( entity[key] ) === '[object Array]' && key !== 'lifecycle_registrations') {
      for (var i = 1; i < entity[key].length; i++) {
        // Remove
        if (entity[key][i-1]._z < entity[key][i]._z) {
          entity[key][i-1] = null;
        }
      }
      entity[key] = _.compact(entity[key]);
    }
  }
  
  // Filter out isnull elements
  for (var key in entity) {
    if( Object.prototype.toString.call( entity[key] ) === '[object Array]' && key !== 'lifecycle_registrations') {
      for (var i = 0; i < entity[key].length; i++) {
        // Remove
        if (entity[key][i]._isnull) {
          entity[key][i] = null;
        } else {
          // remove _z
          delete entity[key][i]._z;
        }
      }
      entity[key] = _.compact(entity[key]);
    }
    if (entity[key] === null || entity[key].length === 0) {
      delete entity[key];
    }
  }
  
  return entity;
};