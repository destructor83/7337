var _ = require('underscore');
var bunyan = require('bunyan');

var logger = require('../middlewares/logger');

var mongodb = require('../helpers/mongodb');
var document = require('../helpers/document');

// init
var log = bunyan.createLogger({
  name: 'trace',
  level: 'trace'
});

exports.execute = function(params) {
  return new Promise(function(resolve, reject) {
    var collection = mongodb.get().collection(params.type);
    logger.trace(log, params, "got mongo collection");

    collection.findOne({"uuid" : params.uuid}, function(err, entity) {
      var ret = {};
      if(!err) {
        ret = document.is_404_422(entity, ['passivated', 'deleted']);
        if (!_.isEmpty(ret)) {
          reject(ret);
          return;
        }
        logger.trace(log, params, "document found");
        ret = {};

        // remove the _id property
        delete entity._id;

        ret.statusCode = 200;
        ret.entity = entity;
        resolve(ret);
      } else {
        ret.statusCode = 500;
        ret.errors = err;
        reject(ret);
      }
    });
  });
};