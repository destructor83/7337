var _ = require('underscore');
var bunyan = require('bunyan');

var logger = require('../middlewares/logger');

var mongodb = require('../helpers/mongodb');
var amqp = require('../helpers/amqp');
var mysql = require('../helpers/mysql');
var themis = require('../helpers/themis');
var uuid = require('../helpers/uuid');
var registration = require('../helpers/registration');
var document = require('../helpers/document');

// init
var log = bunyan.createLogger({
  name: 'create',
  level: 'trace'
});

exports.execute = function(params, entity) {
  return new Promise(function(resolve, reject) {
    var collection = mongodb.get().collection(params.type);
    logger.trace(log, params, "got mongo collection");

    // Create Entity
    entity.uuid = params.dryrun ? "74686973-6973-796f-7572-64727972756e" : uuid.generate();  // Fake dry run UUID
    entity.lifecycle = 'local';
    entity.lifecycle_registrations = [];

    // Schema Validate
    themis.validate('/businessobjects/' + params.type, entity, function(result) {
      var ret = {};

      if (result.errors.length === 0) {
        logger.trace(log, params, "schema validated");
        
        entity.lifecycle_registrations[0] = {};
        entity.lifecycle_registrations[0].registration = registration.add(entity, 'create', 'urn:user:none');
        entity = document.cleanup(entity, 'registration');

        // XXX: Rule validate - 422 for rules error

        // Add schema version and tenant
        entity._tenant = params.tenant;
        entity._version = params.version;
        
        // Store
        if (!params.dryrun) {
          collection.insertOne(entity);
          logger.trace(log, params, "document inserted in mongo");

          // Add relations to reference cache DB
          mysql.addReferences(params.type, entity.uuid, entity);
          logger.trace(log, params, "references added");

          // create AMQP message
          entity.type = params.type;
          amqp.publish(params.type, "create", entity);
          logger.trace(log, params, "amqp message send");
        }
        
        ret = {statusCode: '201', statusMessage: 'Create ' + params.type + ' uuid=' + entity.uuid, errors: null, uuid: entity.uuid};
        ret.statusCode = '201';
        ret.uuid = entity.uuid;
        ret.rid = entity.lifecycle_registrations[0].registration.rid;

        resolve(ret);
      } else {
        // 400 for schema validation error
        ret.statusCode = 400;
        ret.statusMessage = "JSON schema validation error";
        ret.errors = result.errors;

        reject(ret);
      }
    });
  });
};