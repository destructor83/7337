var _ = require('underscore');
var bunyan = require('bunyan');

var logger = require('../middlewares/logger');

var mongodb = require('../helpers/mongodb');
var mysql = require('../helpers/mysql');
var amqp = require('../helpers/amqp');
var uuid = require('../helpers/uuid');
var registration = require('../helpers/registration');
var document = require('../helpers/document');

// init
var log = bunyan.createLogger({
  name: 'delete',
  level: 'trace'
});

exports.execute = function(params) {
  return new Promise(function(resolve, reject) {
    var collection = mongodb.get().collection(params.type);
    logger.trace(log, params, "got mongo collection");

    collection.findOne({"uuid" : params.uuid}, function(err, entity) {
      logger.trace(log, params, "found document");
      var ret = {};
      if(!err) {
        ret = document.is_404_422(entity, ['passivated', 'deleted']);
        if (!_.isEmpty(ret)) {
          reject(ret);
          return;
        }
        logger.trace(log, params, "document found");

        // Update
        entity.lifecycle = 'deleted';
        entity.lifecycle_registrations[entity.lifecycle_registrations.length] = {};
        entity.lifecycle_registrations[entity.lifecycle_registrations.length-1].registration = registration.new(uuid.generate(), new Date().toISOString(), 'delete', 'urn:user:none');

        // Delete
        if (!params.dryrun) {
          collection.remove({ "uuid" : entity.uuid});
          logger.trace(log, params, "document removed from mongo collection");

          mysql.addReferences(params.type, entity.uuid, {});
          logger.trace(log, params, "references removed");

          // Create AMQP message
          entity.type = params.type;      
          amqp.publish(params.type, "deleted", entity.lifecycle_registrations[entity.lifecycle_registrations.length-1].registration);
          logger.trace(log, params, "amqp message send");
        }
        
        ret.statusCode = 204;
        resolve(ret);
      } else {
        ret.statusCode = 500;
        ret.errors = err;
      }
    });
  });
};

