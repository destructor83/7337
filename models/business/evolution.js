var _ = require('underscore');
var bunyan = require('bunyan');

var logger = require('../middlewares/logger');

var mongodb = require('../helpers/mongodb');
var amqp = require('../helpers/amqp');
var uuid = require('../helpers/uuid');
var registration = require('../helpers/registration');
var document = require('../helpers/document');
var bitemporal = require('../helpers/bitemporal');

// init
var log = bunyan.createLogger({
  name: 'evolution',
  level: 'trace'
});

exports.execute = function(params) {
  return new Promise(function(resolve, reject) {
    var collection = mongodb.get().collection(params.type);
    logger.trace(log, params, "got mongo collection");

    collection.findOne({"uuid" : params.uuid}, function(err, entity) {
      var ret = {};
      if(!err) {
        ret = document.is_404_422(entity, ['passivated', 'deleted']);
        if (!_.isEmpty(ret)) {
          reject(ret);
          return;
        }
        logger.trace(log, params, "document found");

        var vt = new Date(params.validityTimeStr);
        entity = document.addZ_and_null(entity);
        entity = evolution(entity, vt);
        logger.trace(log, params, "evolution processing done");

        // remove the _id property
        delete entity._id;

        ret.statusCode = 200;
        resolve(ret);
      } else {
        ret.statusCode = 500;
        ret.errors = err;
        reject(err);
      }
    });
  });
};

function evolution(entity, vt) {
  
  // Filter so that only relevant entries are left
  for (var key in entity) {
    if( Object.prototype.toString.call( entity[key] ) === '[object Array]' && key !== 'lifecycle_registrations') {
      for (var i = 0; i < entity[key].length; i++) {
        // Remove entry?
        if (!bitemporal.insideInterval(entity[key][i].validity, vt)) {
          entity[key][i] = null;
        }
      }
      entity[key] = _.compact(entity[key]);
    }
  }
  
  // Adjust registration interval based on z value
  for (var key in entity) {
    if( Object.prototype.toString.call( entity[key] ) === '[object Array]' && key !== 'lifecycle_registrations') {
      for (var i = 1; i < entity[key].length; i++) {
        if (!_.has(entity[key][i-1].registration, 'to') || entity[key][i-1].registration.to > entity[key][i].registration.from) {
          entity[key][i-1].registration.to = entity[key][i].registration.from;
        }
      }
    }
  }
  
  // Filter out isnull elements
  for (var key in entity) {
    if( Object.prototype.toString.call( entity[key] ) === '[object Array]' && key !== 'lifecycle_registrations') {
      for (var i = 0; i < entity[key].length; i++) {
        // Remove
        if (entity[key][i]._isnull) {
          entity[key][i] = null;
        } else {
          // remove _z
          delete entity[key][i]._z;
        }
      }
      entity[key] = _.compact(entity[key]);
    }
    if (entity[key].length == 0) {
      delete entity[key];
    }
  }
  
  return entity;
};