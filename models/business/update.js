var _ = require('underscore');
var bunyan = require('bunyan');

var logger = require('../middlewares/logger');

var mongodb = require('../helpers/mongodb');
var amqp = require('../helpers/amqp');
var mysql = require('../helpers/mysql');
var themis = require('../helpers/themis');
var uuid = require('../helpers/uuid');
var registration = require('../helpers/registration');
var document = require('../helpers/document');

// init
var log = bunyan.createLogger({
  name: 'update',
  level: 'trace'
});

exports.execute = function(params, update) {
  return new Promise(function(resolve, reject) {
    var collection = mongodb.get().collection(params.type);
    logger.trace(log, params, "got mongo collection");

    // Schema Validate
    themis.validate('/businessobjects/' + params.type, update, function(result) {
      var ret = {};
      if (result.errors.length === 0) {
        logger.trace(log, params, "schema validated");
        
        // Load
        collection.findOne({"uuid" : params.uuid}, function(err, entity) {
          if(!err) {
            ret = document.is_404_422(entity, ['passivated', 'deleted']);
            if (!_.isEmpty(ret)) {
              reject(ret);
              return;
            }
            logger.trace(log, params, "document found");

            registration.add(update, 'update', 'urn:user:none');

            // add update data to entity
            for (var key in update) {
              entity[key] = entity[key].concat(update[key]);
            }
            entity = document.cleanup(entity, 'registration');

            // XXX: Rule validate - 422 for rules error

            // Add schema version and tenant
            entity._tenant = params.tenant;
            entity._version = params.version;

            // Store
            if (!params.dryrun) {
              collection.replaceOne({ "uuid" : entity.uuid}, entity);
              logger.trace(log, params, "document replaced");

              // Add relations to reference cache DB
              mysql.addReferences(params.type, entity.uuid, entity);
              logger.trace(log, params, "references updated");

              // Create AMQP message JSOX = JSON + MOX
              update.type = params.type;      
              amqp.publish(params.type, "update", update);
              logger.trace(log, params, "amqp message send");
            }
            ret.statusCode = 204;

            resolve(ret);
          } else {
            ret.statusCode = 500;
            ret.errors = err;
            
            reject(err);
          }
        });
      } else {
        // 400 for schema validation error
        ret.statusCode = 400;
        ret.statusMessage = "JSON schema validation error";
        ret.errors = result.errors;
        
        reject(ret);
      }
    });
  });
};