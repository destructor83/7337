var mongodb = require('../helpers/mongodb');
var select = require('jsonselect');
var bunyan = require('bunyan');

var logger = require('../middlewares/logger');

// init
var log = bunyan.createLogger({
  name: 'search',
  level: 'trace'
});

// Format search :type/search/:query/:digest
// Format digest 
exports.get = function(params) {
  
  var queryStr = replaceall('(', '{', params.query);
  queryStr = replaceall(')', '}', queryStr);
  queryStr = replaceall("'", '"', queryStr);
  var query = JSON.parse(queryStr);

  // XXX: Build search JSON
  var imput = {
    query: {
      // XXX: Mongodb query format
    },
    digest: {
      select: '', // XXX: JSONSELECT format
      outgoing: {
        relation: {
          type: '',   // Target object type
          name: '',   // Relation name
          role: ''    // Relation role
        },
        digest: {
          // ...
        }
      },
      incomming: {
        relation: {
          type: '',   // Origin object type
          name: '',   // Origin object relation name
          role: ''    // Origin object relation role
        },
        digest: {
          // ...
        }
      }
    }
  }
  
  return execute(input);
};

exports.post = function(body) {
  
  // Validate against search schema
  themis.validate('/definitions/search', body, function(result) {
    var ret = {};
    if (result.errors.length === 0) {
      return execute(body);
    } else {
      
    }
  });
};

function execute(search) {
  return new Promise(function(resolve, reject) {
    var collection = db.get().collection(type);

    var result = [];
    var cursor = collection.find(query).skip(params.first).limit(params.size);
    cursor.each(function(err, entity) {
      var ret = {};
      if(!err) {

        if (entity != null) {
          delete entity._id;

          try {
            if (selectStr != null) entity = select.match(params.select, entity);
            result.push(entity);
          } catch (e) {
            ret.statusCode = 400;
            ret.statusMessage = e;
            resolve(ret);
          }
        } else {
          ret.statusCode = 200;
          ret.entity = result;
          reject(ret);
        }
      } else {
        ret.statusCode = 500;
        ret.statusMessage = err;
        reject(ret);
      }
    });
  });
};

