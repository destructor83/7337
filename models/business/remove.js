var _ = require('underscore');
var bunyan = require('bunyan');

var logger = require('../middlewares/logger');

var mongodb = require('../helpers/mongodb');
var mysql = require('../helpers/mysql');
var amqp = require('../helpers/amqp');
var uuid = require('../helpers/uuid');
var registration = require('../helpers/registration');
var document = require('../helpers/document');

// init
var log = bunyan.createLogger({
  name: 'remove',
  level: 'trace'
});

exports.execute = function(params) {
  return new Promise(function(resolve, reject) {
    var collection = mongodb.get().collection(params.type);
    logger.trace(log, params, "got mongo collection");

    collection.findOne({"uuid" : params.uuid}, function(err, entity) {
      var ret = {};
      if(!err) {
        ret = document.is_404_422(entity, ['passivated', 'deleted']);
        if (!_.isEmpty(ret)) {
          reject(ret);
          return;
        }
        logger.trace(log, params, "document found");

        // Update
        entity.lifecycle = 'removed';
        entity.lifecycle_registrations[entity.lifecycle_registrations.length] = {};
        entity.lifecycle_registrations[entity.lifecycle_registrations.length-1].registration = registration.new(uuid.generate(), new Date().toISOString(), 'remove', 'urn:user:none');

        // Store
        if (!params.dryrun) {
          collection.replaceOne({ "uuid" : entity.uuid}, document.cleanup(entity, 'registration'));
          logger.trace(log, params, "document replaced");

          mysql.addReferences(params.type, entity.uuid, {});
          logger.trace(log, params, "references removed");
          
          // Create AMQP message
          entity.type = params.type;      
          amqp.publish(params.type, "removed", entity.lifecycle_registrations[entity.lifecycle_registrations.length-1].registration);
          logger.trace(log, params, "amqp message send");
        }
        
        
        ret.statusCode = 204;
        resolve(ret);
      } else {
        ret.statusCode = 500;
        ret.errors = err;
        reject(ret);
      }
    });
  });
};
