var _ = require('underscore');

var uuid = require('../helpers/uuid');
var schema = require('../helpers/schema');
var rules = require('../helpers/rules');

exports.it = function(args) {
  
  return args_validate(args)  // Also JSON parsing
  .then(schema.validate)
  .then(prepareUpdate)
  .then(rules.validate)
  .then(completeUpdate);
};

function prepareUpdate(args) {
  return new Promise(function(resolve, reject) {
    var now = new Date().toISOString();

    var entity = args.entity;
    // Check that object is in lifecycle that allows update
    var lc = entity.registrations[entity.registrations.length-1].lifecycle;
    if (_.isUndefined(lc) == false && _.isNull(lc) == false && 
       (lc === 'passivated' || lc === 'removed' || lc === 'deleted')) {
  
      return reject({statusCode: 500, statusMessage: "Object with UUID=" + entity.uuid + " has invalid lifecycle: " + lc.lifecycle + " for operation: update"});
    }

    // Registration
    var registration = {};
    registration.id = uuid.v5(now);
    registration.user = {};
    registration.user.urn = "XXX:oauth2 cli integration missing";

    if (args.comment) registration.comment = args.comment;

    registration.from = now;
    registration.operation = 'update';
    registration.lifecycle = lc;

    // Validity?
    registration.validity = {};
    registration.validity.input = args.input;

    if (args.vf) registration.validity.from = args.vf;
    if (args.vt) registration.validity.to = args.vt;
    if (args.note) registration.validity.note = args.note;
    if (args.actor) {
      registration.validity.actor = {};
      if (args.actor.startsWith("uuid:")) {
        registration.validity.actor.uuid = args.actor.substring(5);
      } else if (args.actor.startsWith("urn:")) {
        registration.validity.actor.urn = args.actor.substring(4);
      }
    }
    entity.registrations.push(registration);

    args.entity = entity;
    return resolve(args);
  });
}

function completeUpdate(args) {
  return new Promise(function(resolve, reject) {
    return resolve({statusCode: 200, statusMessage: "Object with UUID=" + args.entity.uuid + " updated", data: args.entity});
  });
}

function args_validate(args) {
  return new Promise(function(resolve, reject) {
  
    if (_.isUndefined(args.comment)) {
      args.comment = false;
    }
    if (_.isUndefined(args.note)) {
      args.note = false;
    }
    if (_.isUndefined(args.actor)) {
      args.actor = false;
    }
  
    // check mandatory input
    if (_.isUndefined(args.entity) || _.isNull(args.entity)) {
      return reject({statusCode: 500, statusMessage: "No object data supplied"});
    }
  
    // check mandatory input
    if (_.isUndefined(args.input) || _.isNull(args.input)) {
      return reject({statusCode: 500, statusMessage: "No update data supplied"});
    } else try {
      args.input = JSON.parse(args.input);
    } catch (e) {
      return reject({statusCode: 500, statusMessage: "Update date is invalid JSON"});
    }

    if (_.isUndefined(args.vf)) {
      args.vf = false;
    } else {
      // validate date/time
      var date = Date.parse(args.vf);
      if (_.isNaN(date)) {
        return reject({statusCode: 500, statusMessage: "Invalid valid-from format"});
      }
    }
  
    if (_.isUndefined(args.vt)) {
      args.vt = false;
    } else {
      // validate date/time
      date = Date.parse(args.vt);
      if (_.isNaN(date)) {
        return reject({statusCode: 500, statusMessage: "Invalid valid-to format"});
      }
    }
  
    if (args.vf && args.tv && args.vt.localeCompare(args.vf) <= 0) {
      return reject({statusCode: 500, statusMessage: "Interval error: valid-to < valid-from"});
    }
  
    return resolve(args);
  });
}