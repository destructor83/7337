var assert = require('chai').assert;
var _ = require('underscore');
var merge = require('merge');
var create = require('../../models/create.js');

var input = { 
  validFrom: '2016-07-10T00:00:00.000Z',
  validTo: '2016-07-10T00:00:00.000Z',
  input: '{"name": "name"}',
  type: 'unit',
  id: "SHARK",
  comment: "Opret tingen",
  actor: undefined,
  note: "note OG kommentar?"
};

describe('Test of update', function() {
  describe('should od simple update', function () {
    it('should create a thing', function () {
      var args = merge.recursive({}, input);
      return create.it(args).then(function(result) {
        assert.equal(result.statusCode, '201');
        assert.equal(result.data.uuid, 'b97a56e3-6ae8-5ef8-a343-5bb3959edf2a');
      });
    });
  });
  describe('validate create', function () {
    it('should set validTo if not given', function () {
      var args = merge.recursive({}, input);
      args.validTo = args.validFrom = undefined;
      return create.it(args).then(function(result) {
        assert.ok(_.isString(result.data.registrations[0].from), "when validFrom not given, it is set.");
      }, function (fail) {
        console.log(fail);
        assert.ok(false, "should be successful.");
      });
    });
    it('should set id if not given', function () {
      var args = merge.recursive({}, input);
      args.id = undefined;
      return create.it(args).then(function(success) {
        assert.ok(true, "should be successful.");
      }, function (fail) {
        console.log(fail);
        console.log("failing");
        assert.ok(false, "should be successful.");
      });
    });
    it('should require input object', function () {
      var args = merge.recursive({}, input);
      args.input = undefined;
      return create.it(args).then(function(success) {
        assert.ok(false, "should not be successful.");
      }, function (fail) {
        assert.equal(fail.statusCode, 500);
      });
    });
    it('should accept empty object as input', function () {
      var args = merge.recursive({}, input);
      args.input = {};
      return create.it(args).then(function(success) {
      }, function (fail) {
        assert.equal(fail.statusCode, 500);
        assert.ok(false, "should be successful.");
      });
    });
    it('should require type object', function () {
      var args = merge.recursive({}, input);
      args.type = undefined;
      return create.it(args).then(function(success) {
        assert.ok(false, "should not be successful.");
      }, function (fail) {
        assert.equal(fail.statusCode, 500);
      });
    });
  });
  describe('JSON parsing', function () {
    it('should reject empty input', function () {
      var args = merge.recursive({}, input);
      args.input = undefined;
      return create.it(args).then(function(result) {
        assert.ok(false, "should fail.");
      }, function (fail) {
        assert.equal(fail.statusCode, 500);
      });
    });
    it('should reject blank input', function () {
      var args = merge.recursive({}, input);
      args.input = "";
      return create.it(args).then(function(result) {
        assert.ok(false, "should fail.");
      }, function (fail) {
        assert.equal(fail.statusCode, 500);
      });
    });
    it('should accept empty object serialized', function () {
      var args = merge.recursive({}, input);
      args.input = "{}";
      return create.it(args).then(function(result) {
        assert.ok(true);
      }, function (fail) {
        assert.ok(false, "should be succesful.");
      });
    });
  });
});