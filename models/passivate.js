var _ = require('underscore');

var uuid = require('../helpers/uuid');

exports.it = function(args) {
  return new Promise(function(resolve, reject) {
    var now = new Date().toISOString();

    // Check that object is in lifecycle that allows passivate
    var lc = args.entity.registrations[args.entity.registrations.length-1].lifecycle;
    if (_.isUndefined(lc) == false && _.isNull(lc) == false && 
        (lc === 'passivated' || lc === 'removed' || lc === 'deleted')) {
  
      return reject({statusCode: 409, statusMessage: "Object with UUID=" + args.entity.uuid + " has invalid lifecycle: " + lc.lifecycle + " for operation: passivate"});
    }

    // Registration
    var registration = {};
    registration.id = uuid.v5(now);
    registration.user = {};
    registration.user.urn = "XXX:oauth2 cli integration missing";

    if (_.isUndefined(args.comment) == false) registration.comment = args.comment;

    registration.from = now;
    registration.operation = 'passivate';
    registration.lifecycle = 'passivated';

    args.entity.registrations.push(registration);

    return resolve({statusCode: 204, statusMessage: "Object with UUID=" + args.entity.uuid + " passivated", data: args.entity});
  });
};