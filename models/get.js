var _ = require('underscore');
var moment = require('moment');
var merge = require("merge");

exports.it = function(args) {
  return new Promise(function(resolve, reject) {

    // Validate args
    var ret = validate_args(args);
    if (ret) {
      reject(ret);
      return;
    }
    var result = {};
    
    // Check lifecycle
    var lc = args.entity.registrations[args.entity.registrations.length-1].lifecycle;
    if (lc !== 'local' && lc !== 'imported') {
      return reject({statusCode: 309, statusMessage: 'Object has lifecycle: ' + lc + ' that blockes it from the operation get'});
    }

    // Build output with type, uuid and lifecycle
    result['$schema'] = args.entity['$schema'];
    result.uuid = args.entity.uuid;

    // Loop thru registrations
    for (var i = 0; i < args.entity.registrations.length; i++) {
      var reg = args.entity.registrations[i];

      if ((reg.operation === 'create' || reg.operation === 'update') && 
        inInterval(args.registrationTime, reg.from, reg.to) &&
        inInterval(args.validTime, reg.validity.from, reg.validity.to)
       ) {
         if (args.entity['$schema'] === 'application/javascript') {
           // No merge when code
           result = reg.validity.input;
         } else {
           // XXX: this is not enough!
          result = merge.recursive(result, reg.validity.input);
         }
      }
    }

    return resolve({statusCode: 200, statusMessage: "get on object with UUID=" + result.uuid, data: result});
  });
};


function validate_args(args) {
  var ret = {};

  // If no valid time or registration time then set to now
  if (_.isUndefined(args.validTime) || _.isNull(args.validTime)) {
    args.validTime = moment();
  }
  if (_.isUndefined(args.registrationTime) || _.isNull(args.registrationTime)) {
    args.registrationTime = moment();
  }

  // check mandatory input
  if (_.isUndefined(args.entity) || _.isNull(args.entity)) {
    ret.statusCode = 500;
    ret.statusMessage = "No data supplied";

    return ret;
  }
  args.entity = JSON.parse(args.entity);

  return null;
}

function inInterval(time, from, to) {
  
  // moment(undefined).isValid === true ! undefined is interperted as NOW
  // -oo:oo
  if (_.isUndefined(from) === true && _.isUndefined(to) === true) {
    return true;
  }
  
  // -oo:to
  if (_.isUndefined(from) === true && 
      _.isUndefined(to) === false && moment(to).isValid() === true &&
      moment(time).isBefore(to)
     ) {
    return true;
  }

  // from:oo
  if (_.isUndefined(from) === false && moment(from).isValid() === true &&
      _.isUndefined(to) === true &&
      moment(time).isSameOrAfter(from)
     ) {
    return true;
  }

 // from:to
  if (_.isUndefined(from) === false && _.isUndefined(to) === false &&
      moment(from).isValid() == true && moment(to).isValid() === true &&
      moment(time).isSameOrAfter(from) && moment(time).isBefore(to)
     ) {
    return true;
  }

  return false;
}