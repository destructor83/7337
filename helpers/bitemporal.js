exports.insideInterval = function(interval, time) {
  if (time === null) {
    return true;
  }
  
  if (interval.from !== null && interval.from !== undefined && new Date(interval.from) >= time) {
    return false;
  }
  
  if (interval.to !== null && interval.to !== undefined && new Date(interval.to) < time) {
    return false;
  }
  
  return true;
};

exports.compareRegistrations = (a,b) => {
  var avalue = new Date(a.registration.from).getTime();
  var bvalue = new Date(b.registration.from).getTime();
  if (avalue < bvalue) {
    return -1;
  } if (avalue > bvalue) {
    return 1;
  }
  return 0;
};