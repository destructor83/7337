//
// Data Access Module
//
// For now just a wrapper around fs and using promises
// Only to be used for data stored in the workspace referenced by UUIDs
//

var config = require('config');
var fs = require("fs");


//
// args: {uuid: uuid of object}
//
// resolve(args + {input: the data read in})
// reject({statusCode, statusMessage, error});
//
exports.read = function(args) {
  return new Promise(function(resolve, reject) {
    fs.readFile(config.get('workspace_dir') + args.uuid, function (err, entity) {
      if (err) {
        return reject({statusCode: 500, statusMessage: "Error reading file for object with UUID: " + args.uuid, error: err});
      }
      args.input = entity;
      return resolve(args);
    });
  });
};

//
// args: {uuid: uuid of object, output: data to write}
//
// resolve(args)
// reject({statusCode, statusMessage, error});
//
exports.write = function(args) {
  return new Promise(function(resolve, reject) {
    fs.writeFile(config.get('workspace_dir') + args.uuid, args.output, (err) => {
      if (err) {
        return reject({statusCode: 500, statusMessage: "Error writing file for object with UUID: " + args.uuid, error: err});
      }
      return resolve(args);
    });
  });
};