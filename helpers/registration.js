var uuid = require('../helpers/uuid');

exports.add = function (entity, op, user) {
  var rid = uuid.generate();
  var now = new Date().toISOString();
  var reg = registration(rid, now, op, user);
  
  // Add registration to each data object
  for (var key in entity) {
    for (var i = 0; i < entity[key].length; i++) {
      entity[key][i].registration = reg;
    }
  }
  
  return reg;
};

exports.new = function(rid, ts, op, user) {
  return registration(rid, ts, op, user);
};

function registration(rid, ts, op, user) {
  var reg = {};
  reg.user = {};
  reg.user.reference = {};
  reg.reference = {};
  
  reg.rid = rid;
  if (user.lastIndexOf('urn', 0) === 0) {
    reg.user.reference.urn = user;
  } else {
    reg.user.reference.uuid = user;
  }
  reg.reference.type = '/user';
  reg.user = user;
  reg.from = ts;
  reg.operation = op;
  return reg;
}