var _ = require("underscore");
var Ajv = require('ajv');
var fs = require("fs");

var _get = require('../models/get');
var uuid = require('../helpers/uuid');

exports.validate = function(args) {
  return new Promise(function(resolve, reject) {
    args.rules = [];

    // Rules are javascript and is not validated
    if (args.schema === 'application/javascript') {
      return resolve(args);
    }
    
    var loadSchema = loadSchemaClosure(args);
    loadSchema(args.schema, function(err, schema) {
      if (err) {
        return reject({statusCode: 400, statusMessage: "Unable to load schema with uri: " + args.schema, error: err});
      }
      var ajv = new Ajv({ loadSchema: loadSchema, allErrors: true, verbose: true}); 
      ajv.compileAsync(schema, function (err, validate) {
        if (err) {
          return reject({statusCode: 400, statusMessage: "Unable to compile schema with uri: " + args.schema, error: err});
        } else if ( validate(args.input) ) {
          return resolve(args);
        } else {
          return reject({statusCode: 400, statusMessage: "Input does not validate against schema with uri: " + args.schema, error: validate.errors});
        }
      });
    });
  });
};

var loadSchemaClosure = function (args) {

  return function(uri, callback) {

    // Load schema definitions? They are included in ajv, so return an empty schema
    if (uri === "http://json-schema.org/draft-04/schema#") {
      return callback(null, {});
    }
  
    // The uri's look strange. ajv adds / in them, so stripping
    uri = uri.replace(/\//g, '');
    
    // Add uri to rules list - ignore definitions since they dont have any rules
    if (uri.indexOf("definitions") < 0 || uri === '7337.dk:definitions:businessobject') {
      args.rules.push(uri);
    }

    var _uuid = uuid.v5(uri);
    fs.readFile("/home/ubuntu/data/" + _uuid, function (err, entity) {
      if (err) {
        callback(err);
      }
  
      _get.it({entity: entity, validTime: args.validFrom}).then(
        function(success) {
          // Strip object uuid
          delete success.data.uuid;
          
          callback(null, success.data);
        }, function(fail) {
          callback(fail.error);
        }
      );
    });
  };
};