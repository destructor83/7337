var _ = require('underscore');

exports.isUndefined = function(params, keys) {
  var ret = {};
  
  if (_.isUndefined(params)) {
      ret.statusCode = 500;
      ret.statusMessage = "Invalid input";
      return ret;
  }
  for (var i in keys) {
    if (_.isUndefined(params[keys[i]])) {
      ret.statusCode = 500;
      ret.statusMessage = "Invalid input";
      return ret;
    }
  }
  
  return null;
}

exports.isEmpty = function(params, keys) {
  var ret = {};
  
  if (params == null || _.isEmpty(params)) {
      ret.statusCode = 500;
      ret.statusMessage = "Empty input";
      return ret;
  }
  for (var i in keys) {
    var e = params[keys[i]];
    if (e == null || _.isEmpty(e)) {
      ret.statusCode = 500;
      ret.statusMessage = "Cannot be empty: " + keys[i];
      return ret;
    }
  }
  
  return null;
}