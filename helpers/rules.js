var fs = require("fs");

var _get = require('../models/get');
var uuid = require('../helpers/uuid');
require('../helpers/flatten');

exports.validate = function(args) {
  return new Promise(function(resolve, reject) {

    // No rules for javascript or JSON schemas
    if (args.schema === 'application/javascript' || args.schema === 'http://json-schema.org/draft-04/schema#') {
      return resolve(args);
    }

    var iterations = [];
    for (var i = args.rules.length-1; i >= 0; i--) {
      iterations.push(loadRule(args.input, args.rules[i]));
    }
    Promise.all(iterations).then(function(result) {
      
      // Result is an array of rules.
      
      // Result functions from the loaded rule files
      // Call them in order to get the final object with all rules
      var rules = {};
      var types = {};
      for (var i = 0; i < result.length; i++) {
        rules = result[i].rules(rules);
        types = result[i].relation_types(types);
      }
      // Prepare to run type check and all rules async
      iterations = [checkRelationTypes(types, args.input)];
      for (var i = 0; i < Object.keys(rules).length; i++) {
        iterations.push(rules[Object.keys(rules)[i]](args.input, args.verbose));
      }

      console.log("DEBUG: 1 length=" + iterations.length);

      // Run all rules
      Promise.all(iterations).then(function(result) {
        console.log("DEBUG: 2 result.length=" + result.length);
        // All done
        return resolve(args);
      }, function(error) {
        console.log("DEBUG: 3 ");
        return reject(error);
      });
    }, function(result) {

      return reject({statusCode: 500, statusMessage: "Something is bad in rules", error: result});
    });
  });
};

// Load one rule and set doc
function loadRule(doc, rule_name) { 
  return new Promise(function(resolve, reject) {
    var _uuid = uuid.v5(rule_name);
    fs.readFile("/home/ubuntu/data/" + _uuid, function (err, rule_src) {
      if (err) {
        // no rule
        return resolve({});
      }
      
      // Setup rules
      _get.it({entity: rule_src}).then(function(success) {

        var rule = requireFromString(success.data);
        rule.doc = doc;
        return resolve(rule);

      }, function(error) {
        return reject(error); // Could not read rule
      });
    });
  });
}

// Dynamic read in module
function requireFromString(src, filename) {
  var m = new module.constructor();
  m.paths = module.paths;
  m._compile(src, filename);
  return m.exports;
}

// Load ref. object and check that schema is one of valid
function checkRelationTypes(types, doc) {
  return new Promise(function(resolve, reject) {
    // Find all UUID refs. in doc

    // Flatten
    var flat = JSON.flatten(doc);

    // Iterate over flatten object and process .uuid relations
    for (var i = 0; i < Object.keys(flat).length; i++) {
      var key = Object.keys(flat)[i];
      
      if (key.endsWith(".uuid")) {
        var _uuid = flat[key];
        for (var n = 0; n < Object.keys(types).length; n++) {
          var tk = Object.keys(types)[n];
          // use type key as regex
          if (key.match(tk)) {
            checkType(_uuid, key, tk, types, resolve, reject);
          }
        }
      }
    }
  });
}

var checkType = function(_uuid, key, tk, types, resolve, reject) {
  // Load objects and check schema type
  fs.readFile("/home/ubuntu/data/" + _uuid, function (err, data) {
    if (err) {
      // No object with ref. uuid found 
      return reject({statusCode: 404, statusMessage: "Refereneced object: " + _uuid + " from: " + key + " does not exist"});
    }
    // XXX: Get at vt
    _get.it({entity: data}).then(function(success) {
      var schema = success.data["$schema"];

      if (types[tk].indexOf(schema) >= 0) {
        return resolve({statusCode: 200});
      }
      if (types[tk].length > 1) {
        return reject({statusCode: 500, statusMessage: "Refereneced object: " + _uuid + " from: " + key + " has invalid type: " + schema + " allowed types are: " + types[tk]});
      }
      if (types[tk].length === 1) {
        return reject({statusCode: 500, statusMessage: "Refereneced object: " + _uuid + " from: " + key + " has invalid type: " + schema + " only allowed type is: " + types[tk]});
      }
      if (types[tk].length === 0) {
        return reject({statusCode: 500, statusMessage: "Referenece: " + key + " has no valid types so can only be used with urn"});
      }
    }, function(error) {
      return reject(error);
    });
    
    // XXX: find refering objects and run their rules
  });
};