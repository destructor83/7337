var amqp = require('amqplib/callback_api');
var log = require('bunyan').createLogger({name: 'amqp'});

var amqp_channel = null;

amqp.connect('amqp://localhost', function(err, conn) {
  if (err !== null) {
    log.error('Unable to connect to amqp: ' + err.stack);
    process.exit(1);
  }
  log.info("Connected to AMQP");
  conn.createChannel(function(err, ch) {
    amqp_channel = ch;
    amqp_channel.assertExchange("SOPA", 'headers', {durable: false});
  });
});

exports.publish = function(type, operation, entity) {
  amqp_channel.publish("SOPA", type + " " + operation, new Buffer(JSON.stringify(entity)));
};