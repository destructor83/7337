//
// Read all registration data files and return as a Promise
//
var _ = require('underscore');
var fs = require('fs');

exports.read = function (type, uuid) {
  return new Promise(function(resolve, reject) {
    var ret = {};
    var dirname = "~/" + type + "/" + uuid;

    // Get all registrations
    fs.readdir(dirname, function(err, filenames) {
      var files_to_go = filenames.slice(0, filenames.length);
      var data = [];

      if (err) {
        // Return object not found 404
        ret.statusCode = 404;
        ret.statusMessage = 'Object ' + dirname + " not found";
        reject(ret);
        return;
      }

      filenames.forEach(function(filename) {
        fs.readFile(dirname + filename, 'utf-8', function(err, content) {
          if (err) {
            // WHAT? cannot read file that is in the dir?
            ret.statusCode = 500;
            ret.statusMessage = 'Object ' + dirname + "/" + filename + " not found";
            reject(ret);
            return;
          }
          
          // Store file
          data.push(content);
          
          // Remove filename from to go list
          _.pull(files_to_go, filename);
          if (files_to_go.length === 0) {
            // Done !
            resolve(data);
          }
        });
      });
    });
  });
}
