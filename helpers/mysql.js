var mysql = require('mysql');
var log = require('bunyan').createLogger({name: 'mysql'});

var mysqlconnection = null;


mysqlconnection = mysql.createConnection({
  host     : 'localhost',
  user     : process.env.C9_USER,
  password : '',
  database : 'sopa'
});

mysqlconnection.connect(function(err) {
  if (err) {
    log.error('Unable to connect to mysql: ' + err.stack);
    process.exit(1);
  }

  log.info('Connected to MYSQL as id ' + mysqlconnection.threadId);
});

exports.addReferences = function(type, uuid, entity) {
  var post  = {};
  post.uuid = uuid;
  post.type = type;
  
  // Delete existing rows for uuid
  mysqlconnection.query("DELETE FROM relations WHERE `uuid`='" + uuid + "'", function(err, result) {
    if (err) {
       console.error("mysql delete failed: " + err);
    }
  });

  for (var key in entity) {
    if( Object.prototype.toString.call( entity[key] ) == '[object Array]' && key != "lifecycle_registrations") {
      var z = [];
      for (var i = 0; i < entity[key].length; i++) {
        if (entity[key][i].reference != null) {
          
          post.relation = key;
          post.registration_time = new Date(entity[key][i].registration.from).toISOString().replace(/T/, ' ').replace(/\..+/, '');
          post.validity_from = new Date(entity[key][i].validity.from).toISOString().replace(/T/, ' ').replace(/\..+/, '');
          post.validity_to = entity[key][i].validity.to == null ? '9999-12-31 23:59:59' : new Date(entity[key][i].validity.to).toISOString().replace(/T/, ' ').replace(/\..+/, '');
          post.insert = entity[key][i].validity.insert == null ? false : true;
          post.role = entity[key][i].reference.role;
          post.index = entity[key][i].validity.index;
          post.target_id = entity[key][i].reference.uuid == null ? entity[key][i].reference.urn : entity[key][i].reference.uuid;
          post.target_type = entity[key][i].reference.type;
          
          mysqlconnection.query('INSERT INTO relations SET ?', post, function(err, result) {
            if (err) {
              console.error("mysql insert failed: " + err);
            }
          });
        }
      }
    }
  }
};