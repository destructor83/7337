var _ = require('underscore');

exports.cleanup = function(entity, interval) {
  
  // Order: UUID, lifecycle, then keys in alphabetical order
  var elements = [];
  for (var key in entity) {
    if (key !== '_id' && key !== '_tenant' && key !== '_version' && key !== 'uuid' && key !== 'lifecycle' && key !== 'lifecycle_registrations') {
      elements.push([key, entity[key]]);
      delete entity[key];
    }
  }
  elements.sort(function(a, b) {
    return a[0].localeCompare(b[0]);
  });
  
  for (var i = 0; i < elements.length; i++) {
    // key arrays order by 'registration from' taking index into account
    elements[i][1].sort(function(a, b) {
      var index_a = '_';
      if (_.has(a.validity, 'index')) {
        index_a = a.validity.index;
      }
      
      var index_b = '_';
      if (_.has(b.validity, 'index')) {
        index_b = b.validity.index;
      }

      // First on index
      if (index_a !== index_b) {
        return index_a.localeCompare(index_b);
      }
      
      return new Date(a[interval].from) - new Date(b[interval].from);
    });

    // Rebuild entity
    entity[elements[i][0]] = elements[i][1];
  }
  
  return entity;
};

exports.is_404_422 = function(entity, lcArray) {
    var ret = {};
    if (entity === null) {
      ret.statusCode = 404;
      ret.statusMessage = "Object not found";
      return ret;
    }

    // Check lifecycle
    for (var i = 0; i < lcArray.length; i++) {
      if (entity.lifecycle === lcArray[i]) {
        ret.statusCode = 422;
        ret.statusMessage = "Object has invalid lifecycle '" + entity.lifecycle;
        return ret;
      }
    }
    return ret;
};

exports.addZ_and_null = function(entity) {
  
  for (var key in entity) {
    if( Object.prototype.toString.call( entity[key] ) === '[object Array]' && key !== 'lifecycle_registrations') {
      var z = [];
      for (var i = 0; i < entity[key].length; i++) {
        var index = '_';  // Used as default index if not specified in validity

        if (_.has(entity[key][i].validity, 'index')) {
          index = entity[key][i].validity.index;
        }
        if (!z[index]) {
          z[index] = 0;
        }
        entity[key][i]._z = z[index];
        z[index]++;
        
        // Insert null record ?
        if (_.has(entity[key][i].validity, 'to') && (!_.has(entity[key][i].validity, 'insert') || entity[key][i].validity.insert !== true)) {
          // Insert NULL record
          entity[key].splice(i+1, 0, _.clone(entity[key][i]));
          entity[key][i+1]._isnull = true;
          entity[key][i+1].validity = _.clone(entity[key][i].validity);
          entity[key][i+1].validity.from = entity[key][i+1].validity.to;
          delete entity[key][i+1].validity.to;
          i++;
        }        
      }
    }
  }
  
  return entity;
};