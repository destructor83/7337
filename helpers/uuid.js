const crypto = require('crypto');
var _ = require('underscore');

exports.v5 = exports.generate = function(data) {
  if (_.isUndefined(data)) {
    throw new Error("Cannot generate uuid without key, pass argument 'data' to uuid.v5 or uuid.generate.");
  }
  var out = crypto.createHash('sha1').update(data).digest();

  out[8] = out[8] & 0x3f | 0xa0; // set variant
  out[6] = out[6] & 0x0f | 0x50; // set version

  var hex = out.toString('hex', 0, 16);

  return [
    hex.substring( 0,  8),
    hex.substring( 8, 12),
    hex.substring(12, 16),
    hex.substring(16, 20),
    hex.substring(20, 32)
  ].join('-');
}