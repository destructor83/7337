exports.sendError = function(res, error) {
  res.statusCode = error.statusCode;
  res.statusMessage = error.statusMessage;

  res.send((typeof error.errors === 'undefined') ? "" : JSON.stringify(error.errors, null, 2));
};

exports.sendLocation = function(res, type, uuid) {
  var locations = [];
  locations[0] = 'PATCH' + ' /' + type + '/passivate/' + uuid;
  locations[1] = 'DELETE' + ' /' + type + '/delete/' + uuid;
  locations[2] = 'PATCH' + ' /' + type + '/update/' + uuid;
  locations[3] = 'GET' + ' /' + type + '/trace/' + uuid;
  locations[4] = 'GET' + ' /' + type + '/history/' + uuid + '/<registration time>';
  locations[5] = 'GET' + ' /' + type + '/evolution/' + uuid + '/<validation time>';
  locations[6] = 'GET' + ' /' + type + '/get/' + uuid;
  locations[7] = 'GET' + ' /' + type + '/get/' + uuid + '/<validation time>';
  locations[8] = 'GET' + ' /' + type + '/get/' + uuid + '/<validation time>/<registration time>';

  res.location(uuid);
  res.send(JSON.stringify(locations, null, 2));
};