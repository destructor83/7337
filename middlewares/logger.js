var _ = require('underscore');

var uuid = require('../helpers/uuid');

exports.start = function(log, req) {
  var dryrun = req.params.dryrun;
  var id = req.headers.req_id;
  
  if (_.isUndefined(id)) {
    // Not set by proxy so create one
    req.headers.req_id = uuid.generate();
  }
  
  log.info({req_id: req.headers.req_id}, (_.isUndefined(dryrun) ? "" : "DRYRUN: ") + req.url + " START");
  log.debug({req_id: req.headers.req_id, url: req.url, method: req.method, headers: req.headers, parameters: req.params, body: req.body});
}

exports.success = function(log, req, result) {
  var dryrun = req.params.dryrun;

  log.info({req_id: req.headers.req_id}, (_.isUndefined(dryrun) ? "" : "DRYRUN: ") + req.url + " SUCCESS " + (_.isUndefined(result.statusMessage) ? "" : result.statusMessage));
  log.debug({req_id: req.headers.req_id, result: result});
}

exports.error = function(log, req, error) {
  var dryrun = req.params.dryrun;

  log.error({req_id: req.headers.req_id}, (_.isUndefined(dryrun) ? "" : "DRYRUN: ") + req.url + " FAILED " + error.statusMessage);
  log.debug({req_id: req.headers.req_id, error: error});
};

exports.trace = function(log, params, msg) {
  log.trace({req_id: params.req_id}, msg);
}