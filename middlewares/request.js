var _ = require('underscore');

var uuid = require('../helpers/uuid');

exports.ensureId = function(req) {
  var id = req.headers.req_id;
  
  if (typeof id === 'undefined') {
    // Not set by proxy so create one
    req.headers.req_id = uuid.generate();
  }
};

exports.required = function(req, required) {
  return new Promise(function(resolve, reject) {
    var errors = {};
    var params = {};
   
    params.req_id = req.headers.req_id;

    // Optional parameters
    params.dryrun = false;
    if (typeof req.params.dryrun !== 'undefined') {
      params.dryrun = true;
    } 
    params.start = 0;
    if (typeof req.params.start !== 'undefined' && _.isNumber(req.params.start)) {
      params.start = req.params.start;
    } 
    params.size = 20;
    if (typeof req.params.size !== 'undefined' && _.isNumber(req.params.size)) {
      params.size = req.params.size;
    } 

    // check headers
    var errors = [];
    for (var i = 0; i < required.headers.length; i++) {
      if (req.headers[required.headers[i]] === null) {
        errors.push(required.headers[i]);
      } else {
        params[required.headers[i]] = req.headers[required.headers[i]];
      }
    }
    addErrors(errors, 'Missing headers: ', errors);

    // check parameters
    errors = [];
    for (var i = 0; i < required.params.length; i++) {
      if (req.params[required.params[i]] === null) {
        errors.push(required.params[i]);
      } else {
        params[required.params[i]] = req.params[required.params[i]];
      }
    }
    addErrors(errors, 'Missing parameters: ', errors);

    if (typeof errors.statusCode == 'undefined') {
      resolve(params);
    } else {
      reject(errors);
    }
  });
};

function addErrors(ret, message, errors) {
  if (errors.length === 0) return;
  
  if (typeof ret.statusCode == 'undefined') {
    ret.statusCode = 400;
    ret.statusMessage = "";
  }
  ret.statusMessage += message + errors.join();
}