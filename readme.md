7337 Platform 
=======================
7337 is a development platform that provides a distributed bitemporal object storage with a uniquely fast and dynamic data definition layer.

Data is defined both by schemas and rules written using the rules API. This provides a flexible data definition that evolve over time.

Features
--------
- versioning og data, both bitemporal, branching and local version.
- distributed storage, with peer to peer ability to allow fast reconcilliation across the application distribution.
- extensive API, both commandline and via REST that allows full control and easy integration.
- strong security, by requiring two-factor authentication on all layers and having revision log for ressource authorization.

Prerequisites
-------------
- [Node.js 6.0+](http://nodejs.org)
- [GIT](https://git-scm.com)

Getting Started
---------------
To get started with your very own distributed 7337 application, you first need to define some entity types.

Luckily, we have provided a boilerplate setup to get you started quickly.

Just pull the boilerplate project.


```
#!bash
$git pull http://public.7337.org/boilerplate myproject
$cd myproject
$rm -rf .gitignore
```

You can then commit it to your own SCM.

Now run the init script, this will initialize your 7337 repository and load a few example schemas.

```
#!bash
$7337 init
$7337 create -s 'schema..' -i 'simplething' simple_schema
$7337 create -s 'simplething' simple_thing
> some <uuid>
```

The first object is now in your data store, note the uuid returned by the create command.
Try the 'trace' command, this will show the entire state of the object.

```
#!bash
$7337 trace <uuid>
> outputs something
```

Update the object with the data from the file 'simple_thing_update'.
By inspecting this data, you will see that this contains a change of the objects state that is in the future.

Now try to 'trace' the state of the object again and try giving it a date far in the future.

```
#!bash
trace <the uuid> --datetime 2020-01-01
> output something
```

If other applications shared this repo, they would get the data.

Next up, pull the repository to some place else, perhaps another machine.