var async = require('async');
var assert = require('assert');
var request = require('supertest');
var fs = require('fs');
var _ = require('underscore')

describe('Test of SOPA services', function() {

  describe('calling create of person', function () {
    it('should return status code: 201', function (done) {
      var createData = JSON.parse(fs.readFileSync('tests/controllers/data/person-create.json', 'utf8'));
      request('http://localhost:8081/person/').post('create')
        .set('sopa-version', '1.0')
        .set('sopa-tenant', 'kattegat')
        .set('sopa-namespace', '.')
        .send(createData).expect(201).end(function(err, res) {
        if (err) {
          console.log(err);
          throw err;
        } else {
          var uuid = res.headers['location'];

          describe('calling passivate/uuid of person', function () {
            it('should return status code: 204', function (done) {
              request('http://localhost:8081/person/').patch('passivate/' + uuid).send({}).expect(204).end(function(err, res) {
                if (err) {
                  console.log(err);
                  throw err;
                } else {
                  
                  describe('calling trace/uuid of person', function () {
                    it('should return status code: 200', function (done) {
                      request('http://localhost:8081/person/').get('trace/' + uuid).expect(200).end(function(err, res) {
                        if (err) {
                          console.log(err);
                          throw err;
                        } else {

                          var data = JSON.parse(res.text);

                          assert(data.uuid, uuid);
                          assert(data.lifecycle, 'passivated');
                        }
                        done();
                        
                        describe('calling passivate/uuid of person', function () {
                          it('should return status code: 422', function (done) {
                            request('http://localhost:8081/person/').patch('passivate/' + uuid).send({}).expect(422).end(function(err, res) {
                              if (err) {
                                console.log(err);
                                throw err;
                              } else {
                              }
                              done();
                            });
                          });
                        });

                        describe('calling get/uuid of person', function () {
                          it('should return status code: 422', function (done) {
                            request('http://localhost:8081/person/').get('get/' + uuid).expect(422).end(function(err, res) {
                              if (err) {
                                console.log(err);
                                throw err;
                              } else {
                              }
                              done();
                            });
                          });
                        });

                        describe('calling update/uuid of person', function () {
                          it('should return status code: 422', function (done) {
                            request('http://localhost:8081/person/').patch('update/' + uuid).send({}).expect(422).end(function(err, res) {
                              if (err) {
                                console.log(err);
                                throw err;
                              } else {
                              }
                              done();
                            });
                          });
                        });

                        describe('calling delete/uuid of person', function () {
                          it('should return status code: 204', function (done) {
                            request('http://localhost:8081/person/').delete('delete/' + uuid).send({}).expect(204).end(function(err, res) {
                              if (err) {
                                console.log(err);
                                throw err;
                              } else {
                              }
                              done();
                            });
                          });
                        });

                      });
                    });
                  });
                }
                done();
                
              });
            });
          });
        }
        done();
        
      });
    });
  });
});