var async = require('async');
var should = require('should');
var request = require('supertest');

describe('Test of SOPA services', function() {

  describe('calling get with invalid type', function () {
    it('should return error code: 404', function (done) {
      request('http://localhost:8081/invalid/').get('get/00000000-0000-0000-0000-000000000000').expect(404).end(function(err, res) {
        if (err) {
          console.log(err);
          throw err;
        }
        done();
      });
    });
  });

  describe('calling trace with invalid type', function () {
    it('should return error code: 404', function (done) {
      request('http://localhost:8081/invalid/').get('trace/00000000-0000-0000-0000-000000000000').expect(404).end(function(err, res) {
        if (err) {
          console.log(err);
          throw err;
        }
        done();
      });
    });
  });

  describe('calling history with invalid type', function () {
    it('should return error code: 404', function (done) {
      request('http://localhost:8081/invalid/').get('history/00000000-0000-0000-0000-000000000000/26.02.2018').expect(404).end(function(err, res) {
        if (err) {
          console.log(err);
          throw err;
        }
        done();
      });
    });
  });

  describe('calling evolution with invalid type', function () {
    it('should return error code: 404', function (done) {
      request('http://localhost:8081/invalid/').get('evolution/00000000-0000-0000-0000-000000000000/26.02.2018').expect(404).end(function(err, res) {
        if (err) {
          console.log(err);
          throw err;
        }
        done();
      });
    });
  });

  describe('calling passivate with invalid type', function () {
    it('should return error code: 404', function (done) {
      request('http://localhost:8081/invalid/').put('passivate/00000000-0000-0000-0000-000000000000').expect(404).end(function(err, res) {
        if (err) {
          console.log(err);
          throw err;
        }
        done();
      });
    });
  });

  describe('calling delete with invalid type', function () {
    it('should return error code: 404', function (done) {
      request('http://localhost:8081/invalid/').delete('delete/00000000-0000-0000-0000-000000000000').expect(404).end(function(err, res) {
        if (err) {
          console.log(err);
          throw err;
        }
        done();
      });
    });
  });

  describe('calling remove with invalid type', function () {
    it('should return error code: 404', function (done) {
      request('http://localhost:8081/invalid/').patch('remove/00000000-0000-0000-0000-000000000000').expect(404).end(function(err, res) {
        if (err) {
          console.log(err);
          throw err;
        }
        done();
      });
    });
  });

  describe('calling update with invalid type', function () {
    it('should return error code: 400', function (done) {
      request('http://localhost:8081/invalid/').put('update/00000000-0000-0000-0000-000000000000').send({}).expect(400).end(function(err, res) {
        if (err) {
          console.log(err);
          throw err;
        }
        done();
      });
    });
  });

  describe('calling create with invalid type', function () {
    it('should return error code: 400', function (done) {
      request('http://localhost:8081/invalid/').post('create').send({}).expect(400).end(function(err, res) {
        if (err) {
          console.log(err);
          throw err;
        }
        done();
      });
    });
  });

});

