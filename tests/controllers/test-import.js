var async = require('async');
var assert = require('assert');
var request = require('supertest');
var fs = require('fs');
var _ = require('underscore')

describe('Test of SOPA services', function() {

  describe('calling import of person', function () {
    it('should return status code: 201', function (done) {
      var importData = JSON.parse(fs.readFileSync('tests/controllers/data/person-import.json', 'utf8'));
      request('http://localhost:8081/person/')
        .put('import')
        .set('sopa-version', '1.0')
        .set('sopa-tenant', 'kattegat')
        .set('sopa-namespace', '.')
        .send(importData).expect(201).end(function(err, res) {
        if (err) {
          console.log(err);
          throw err;
        } else {
          
          describe('calling get/uuid of person', function () {
            it('should return status code: 200', function (done) {
              request('http://localhost:8081/person/').get('get/' + importData.uuid).expect(200).end(function(err, res) {
                if (err) {
                  console.log(err);
                  throw err;
                } else {
                  
                  var getData = JSON.parse(res.text);
                  
                  assert(getData.uuid, importData.uuid);
                  assert(getData.lifecycle, 'imported');
                  
                  assert(getData.address.length, 2);
                  
                  var address_index1 = _.find(getData.address, function(obj) { return obj.validity.index === '1'});
                  assert(address_index1.reference.uuid === '448078fc-e9c2-11e5-9ce9-5e5517507c66');
                  assert(address_index1.reference.role === 'work');
                  assert(address_index1.registration.from === '2004-04-04T04:04:04.444Z');
                  assert(address_index1.registration.operation === 'update');
                  assert(address_index1.validity.from === '2004-04-04T04:04:04.444Z');

                  var address_index2 = _.find(getData.address, function(obj) { return obj.validity.index === '2'});
                  assert(address_index2.reference.uuid === 'ff0f7232-e9c1-11e5-9ce9-5e5517507c66');
                  assert(address_index2.reference.role === 'home');
                  assert(address_index2.registration.from === '2003-03-03T03:03:03.333Z');
                  assert(address_index2.registration.operation === 'create');
                  assert(address_index2.validity.from === '2003-03-03T03:03:03.333Z');
                  
                }
                done();
              });
            });
          });
          
          describe('calling get/uuid/validtime of person', function () {
            it('should return status code: 200', function (done) {
              request('http://localhost:8081/person/').get('get/' + importData.uuid + '/2007-07-07').expect(200).end(function(err, res) {
                if (err) {
                  console.log(err);
                  throw err;
                } else {
                  
                  var getData = JSON.parse(res.text);
                  
                  assert(getData.uuid, importData.uuid);
                  assert(getData.lifecycle, 'imported');
                  
                  assert(getData.address.length, 2);
                  
                  var address_index1 = _.find(getData.address, function(obj) { return obj.validity.index === '1'});
                  assert(address_index1.reference.uuid === '448078fc-e9c2-11e5-9ce9-5e5517507c66');
                  assert(address_index1.reference.role === 'work');
                  assert(address_index1.registration.from === '2004-04-04T04:04:04.444Z');
                  assert(address_index1.registration.operation === 'update');
                  assert(address_index1.validity.from === '2004-04-04T04:04:04.444Z');

                  var address_index2 = _.find(getData.address, function(obj) { return obj.validity.index === '2'});
                  assert(address_index2.reference.uuid === 'ff0f7232-e9c1-11e5-9ce9-5e5517507c66');
                  assert(address_index2.reference.role === 'home');
                  assert(address_index2.registration.from === '2003-03-03T03:03:03.333Z');
                  assert(address_index2.registration.operation === 'create');
                  assert(address_index2.validity.from === '2003-03-03T03:03:03.333Z');
                  
                  assert(getData.name[0].firstname === 'E');
                  assert(getData.name[0].surname === 'E');
                  assert(getData.name[0].validity.from === '2006-06-06T06:06:06.666Z');
                  assert(getData.name[0].validity.to === '2008-08-08T08:08:08.888Z');
                }
                done();
              });
            });
          });

          describe('calling evolution/uuid/validtime of person', function () {
            it('should return status code: 200', function (done) {
              request('http://localhost:8081/person/').get('evolution/' + importData.uuid + '/2007-07-08').expect(200).end(function(err, res) {
                if (err) {
                  console.log(err);
                  throw err;
                } else {
                  
                  var getData = JSON.parse(res.text);
                  
//console.log(JSON.stringify(getData, null, ' '));
                  
                  assert(getData.uuid, importData.uuid);
                  assert(getData.lifecycle, 'imported');
                  
                  assert(getData.name.length === 3);
                  assert(getData.name[0].firstname === 'A');
                  assert(getData.name[0].validity.from === "2002-02-02T02:02:02.222Z");
                  assert(getData.name[0].registration.from === "2003-03-03T03:03:03.333Z");
                  assert(getData.name[0].registration.to === "2006-06-06T06:06:06.666Z");
                  
                  assert(getData.name[1].firstname === 'B');
                  assert(getData.name[1].validity.from === '2006-06-06T06:06:06.666Z');
                  assert(getData.name[1].validity.to === '2008-08-08T08:08:08.888Z');
                  assert(getData.name[1].registration.from === "2006-06-06T06:06:06.666Z");
                  assert(getData.name[1].registration.to === "2008-08-08T08:08:08.888Z");

                  assert(getData.name[2].firstname === 'E');
                  assert(getData.name[2].validity.from === '2006-06-06T06:06:06.666Z');
                  assert(getData.name[2].validity.to === '2008-08-08T08:08:08.888Z');
                  assert(getData.name[2].registration.from === "2012-12-12T12:12:12.012Z");
                }
                done();
              });
            });
          });

          describe('calling evolution/uuid/validtime of person', function () {
            it('should return status code: 200', function (done) {
              request('http://localhost:8081/person/').get('evolution/' + importData.uuid + '/2006-06-07').expect(200).end(function(err, res) {
                if (err) {
                  console.log(err);
                  throw err;
                } else {
                  
                  var getData = JSON.parse(res.text);
                  
//console.log(JSON.stringify(getData, null, ' '));
                  
                  assert(getData.uuid, importData.uuid);
                  assert(getData.lifecycle, 'imported');
                  
                  assert(getData.name.length === 4);
                  assert(getData.name[0].firstname === 'A');
                  assert(getData.name[0].validity.from === "2002-02-02T02:02:02.222Z");
                  assert(getData.name[0].registration.from === "2003-03-03T03:03:03.333Z");
                  assert(getData.name[0].registration.to === "2006-06-06T06:06:06.666Z");
                  
                  assert(getData.name[1].firstname === 'B');
                  assert(getData.name[1].validity.from === '2006-06-06T06:06:06.666Z');
                  assert(getData.name[1].validity.to === '2008-08-08T08:08:08.888Z');
                  assert(getData.name[1].registration.from === "2006-06-06T06:06:06.666Z");
                  assert(getData.name[1].registration.to === "2008-08-08T08:08:08.888Z");

                  assert(getData.name[2].firstname === 'C');
                  assert(getData.name[2].validity.from === '2005-05-05T05:05:05.555Z');
                  assert(getData.name[2].validity.to === '2007-07-07T07:07:07.777Z');
                  assert(getData.name[2].registration.from === "2008-08-08T08:08:08.888Z");
                  assert(getData.name[2].registration.to === "2012-12-12T12:12:12.012Z");

                  assert(getData.name[3].firstname === 'E');
                  assert(getData.name[3].validity.from === '2006-06-06T06:06:06.666Z');
                  assert(getData.name[3].validity.to === '2008-08-08T08:08:08.888Z');
                  assert(getData.name[3].registration.from === "2012-12-12T12:12:12.012Z");
                }
                done();
              });
            });
          });

          describe('calling history/uuid/registrationtime of person', function () {
            it('should return status code: 200', function (done) {
              request('http://localhost:8081/person/').get('history/' + importData.uuid + '/2007-07-07').expect(200).end(function(err, res) {
                if (err) {
                  console.log(err);
                  throw err;
                } else {
                  
                  var getData = JSON.parse(res.text);
                  
                  assert(getData.uuid, importData.uuid);
                  assert(getData.lifecycle, 'imported');
                  
                  assert(getData.name.length, 3);
                  assert(getData.name[0].firstname === 'A');
                  assert(getData.name[0].validity.from === "2002-02-02T02:02:02.222Z");
                  assert(getData.name[0].validity.to === "2006-06-06T06:06:06.666Z");
                  
                  assert(getData.name[1].firstname === 'B');
                  assert(getData.name[1].validity.from === "2006-06-06T06:06:06.666Z");
                  assert(getData.name[1].validity.to === "2008-08-08T08:08:08.888Z");

                  assert(getData.name[2].firstname === 'A');
                  assert(getData.name[2].validity.from === "2008-08-08T08:08:08.888Z");
                  assert(typeof getData.name[2].validity.to === "undefined");                  
                  
                }
                done();
              });
            });
          });

          describe('calling history/uuid/registrationtime of person', function () {
            it('should return status code: 200', function (done) {
              request('http://localhost:8081/person/').get('history/' + importData.uuid + '/2011-11-11').expect(200).end(function(err, res) {
                if (err) {
                  console.log(err);
                  throw err;
                } else {
                  
                  var getData = JSON.parse(res.text);
//console.log(JSON.stringify(getData, null, ' '));
                  
                  assert(getData.uuid, importData.uuid);
                  assert(getData.lifecycle === 'imported');
                  
                  console.log("DEBUG: getData.address.length=" + getData.name.length);
                  assert(getData.name.length === 3);
                  assert(getData.name[0].firstname === 'A');
                  assert(getData.name[0].validity.from === "2002-02-02T02:02:02.222Z");
                  assert(getData.name[0].validity.to === "2005-05-05T05:05:05.555Z");
                  
                  assert(getData.name[1].firstname === 'C');
                  assert(getData.name[1].validity.from === "2005-05-05T05:05:05.555Z");
                  assert(getData.name[1].validity.to === "2007-07-07T07:07:07.777Z");

                  assert(getData.name[2].firstname === 'D');
                  assert(getData.name[2].validity.from === "2012-12-12T12:12:12.012Z");
                  assert(typeof getData.name[2].validity.to === "undefined");                  

                }
                done();
              });
            });
          });

        }
        done();
      });
    });
  });
});