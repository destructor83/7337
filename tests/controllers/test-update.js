var async = require('async');
var assert = require('assert');
var request = require('supertest');
var fs = require('fs');
var _ = require('underscore')

describe('Test of SOPA services', function() {

  describe('calling create of person', function () {
    it('should return status code: 201', function (done) {
      var createData = JSON.parse(fs.readFileSync('tests/controllers/data/person-create.json', 'utf8'));
      request('http://localhost:8081/person/').post('create')
        .set('sopa-version', '1.0')
        .set('sopa-tenant', 'kattegat')
        .set('sopa-namespace', '.')
        .send(createData).expect(201).end(function(err, res) {
        if (err) {
          console.log(err);
          throw err;
        } else {
          var uuid = res.headers['location'];

          describe('calling update/uuid of person', function () {
            it('should return status code: 204', function (done) {
              var updateData = JSON.parse(fs.readFileSync('tests/controllers/data/person-update.json', 'utf8'));
              request('http://localhost:8081/person/').patch('update/' + uuid).send(updateData).expect(204).end(function(err, res) {
                if (err) {
                  console.log(err);
                  throw err;
                } else {
                  
                  describe('calling get/uuid/validtime of person', function () {
                    it('should return status code: 200', function (done) {
                      request('http://localhost:8081/person/').get('get/' + uuid + '/2005-05-05').expect(200).end(function(err, res) {
                        if (err) {
                          console.log(err);
                          throw err;
                        } else {

                          var getData = JSON.parse(res.text);

                          assert(getData.uuid, uuid);
                          assert(getData.lifecycle, 'local');

                          assert(getData.address.length, 2);

                          var address_index1 = _.find(getData.address, function(obj) { return obj.validity.index === '1'});
                          assert(address_index1.reference.uuid, '448078fc-e9c2-11e5-9ce9-5e5517507c66');
                          assert(address_index1.reference.role, 'work');
                          assert(address_index1.registration.operation, 'create');
                          assert(address_index1.validity.from, '2004-04-04T04:04:04.444Z');

                        }
                        done();
                      });
                    });
                  });

                  describe('calling get/uuid/validtime of person', function () {
                    it('should return status code: 200', function (done) {
                      request('http://localhost:8081/person/').get('get/' + uuid + '/2006-06-06').expect(200).end(function(err, res) {
                        if (err) {
                          console.log(err);
                          throw err;
                        } else {
                          var getData = JSON.parse(res.text);

                          assert(getData.uuid, uuid);
                          assert(getData.lifecycle, 'local');

                          assert(getData.address.length, 2);

                          var address_index1 = _.find(getData.address, function(obj) { return obj.validity.index === '1'});
                          assert(address_index1.reference.uuid, '77777777-7777-7777-7777-777777777777');
                          assert(address_index1.reference.role, 'work');
                          assert(address_index1.registration.operation, 'update');
                          assert(address_index1.validity.from, '2005-05-05T05:05:05.555Z');
                          assert(address_index1.validity.to, '2007-07-07T07:07:07.777Z');
                          assert(address_index1.validity.insert, true);

                        }
                        done();
                      });
                    });
                  });
                  
                  describe('calling get/uuid/validtime of person', function () {
                    it('should return status code: 200', function (done) {
                      request('http://localhost:8081/person/').get('get/' + uuid + '/2008-08-08').expect(200).end(function(err, res) {
                        if (err) {
                          console.log(err);
                          throw err;
                        } else {

                          var getData = JSON.parse(res.text);

                          assert(getData.uuid, uuid);
                          assert(getData.lifecycle, 'local');

                          assert(getData.address.length, 2);

                          var address_index1 = _.find(getData.address, function(obj) { return obj.validity.index === '1'});
                          assert(address_index1.reference.uuid, '448078fc-e9c2-11e5-9ce9-5e5517507c66');
                          assert(address_index1.reference.role, 'work');
                          assert(address_index1.registration.operation, 'create');
                          assert(address_index1.validity.from, '2004-04-04T04:04:04.444Z');

                        }
                        done();
                      });
                    });
                  });

                  describe('calling get/uuid/validtime of person', function () {
                    it('should return status code: 200', function (done) {
                      request('http://localhost:8081/person/').get('get/' + uuid + '/2009-09-09T09:09:09.999Z').expect(200).end(function(err, res) {
                        if (err) {
                          console.log(err);
                          throw err;
                        } else {
                          var getData = JSON.parse(res.text);

                          assert(getData.uuid, uuid);
                          assert(getData.lifecycle, 'local');

                          assert(getData.address.length, 2);

                          var address_index1 = _.find(getData.address, function(obj) { return obj.validity.index === '1'});
                          assert(address_index1.reference.uuid, '99999999-9999-9999-9999-999999999999');
                          assert(address_index1.reference.role, 'work');
                          assert(address_index1.registration.operation, 'update');
                          assert(address_index1.validity.from, '2009-09-09T09:09:09.999Z');

                        }
                        done();
                      });
                    });
                  });

                  
                }
                done();
                
              });
            });
          });
        }
        done();
        
      });
    });
  });
});