var async = require('async');
var assert = require('assert');
var request = require('supertest');
var fs = require('fs');
var _ = require('underscore');

describe('Test of SOPA meta services', function() {

  describe('calling lookup create/update', function () {
    it('should return status code: 201', function (done) {
      request('http://localhost:8082/lookup/a,b,c')
        .post('lookup')
        .set('sopa-tenant', 'kattegat')
        .send('').expect(201).end(function(err, res) {
        if (err) {
          console.log(err);
          throw err;
        } else {
          var uuid = res.headers['location'];
          
          describe('calling get/uuid of person', function () {
            it('should return status code: 200', function (done) {
              request('http://localhost:8081/person/').get('get/' + uuid).expect(200).end(function(err, res) {
                if (err) {
                  console.log(err);
                  throw err;
                } else {
                  
                  var getData = JSON.parse(res.text);
                  
                  assert(getData.lifecycle, 'local');
                  
                  assert(getData.address.length, 2);
                  
                  var address_index1 = _.find(getData.address, function(obj) { return obj.validity.index === '1'});
                  assert(address_index1.reference.uuid, '448078fc-e9c2-11e5-9ce9-5e5517507c66');
                  assert(address_index1.reference.role, 'work');
                  assert(address_index1.registration.from, '2003-03-03T03:03:03.333Z');
                  assert(address_index1.registration.operation, 'update');
                  assert(address_index1.validity.from, '2004-04-04T04:04:04.444Z');

                  var address_index2 = _.find(getData.address, function(obj) { return obj.validity.index === '2'});
                  assert(address_index2.reference.uuid, 'ff0f7232-e9c1-11e5-9ce9-5e5517507c66');
                  assert(address_index2.reference.role, 'home');
                  assert(address_index2.registration.from, '2003-03-03T03:03:03.333Z');
                  assert(address_index2.registration.operation, 'create');
                  assert(address_index2.validity.from, '2003-03-03T03:03:03.333Z');
                  
                }
                done();
              });
            });
          });
          
          describe('calling get/uuid/validtime of person', function () {
            it('should return status code: 200', function (done) {
              request('http://localhost:8081/person/').get('get/' + uuid + '/2007-07-07').expect(200).end(function(err, res) {
                if (err) {
                  console.log(err);
                  throw err;
                } else {
                  
                  var getData = JSON.parse(res.text);
                  
                  assert(getData.uuid, uuid);
                  assert(getData.lifecycle, 'local');
                  
                  assert(getData.address.length, 2);
                  
                  var address_index1 = _.find(getData.address, function(obj) { return obj.validity.index === '1'});
                  assert(address_index1.reference.uuid, '448078fc-e9c2-11e5-9ce9-5e5517507c66');
                  assert(address_index1.reference.role, 'work');
                  assert(address_index1.registration.from, '2003-03-03T03:03:03.333Z');
                  assert(address_index1.registration.operation, 'update');
                  assert(address_index1.validity.from, '2004-04-04T04:04:04.444Z');

                  var address_index2 = _.find(getData.address, function(obj) { return obj.validity.index === '2'});
                  assert(address_index2.reference.uuid, 'ff0f7232-e9c1-11e5-9ce9-5e5517507c66');
                  assert(address_index2.reference.role, 'home');
                  assert(address_index2.registration.from, '2003-03-03T03:03:03.333Z');
                  assert(address_index2.registration.operation, 'create');
                  assert(address_index2.validity.from, '2003-03-03T03:03:03.333Z');
                  
                  assert(getData.name[0].firstname, 'E');
                  assert(getData.name[0].surname, 'E');
                  assert(getData.name[0].validity.from, '2006-06-06T03:06:06.666Z');
                  assert(getData.name[0].validity.to, '2008-08-08T03:08:08.888Z');
                }
                done();
              });
            });
          });

        }
        done();
      });
    });
  });
});