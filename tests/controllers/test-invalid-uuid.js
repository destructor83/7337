var async = require('async');
var should = require('should');
var request = require('supertest');

describe('Test of SOPA services', function() {

  describe('calling get with invalid uuid', function () {
    it('should return error code: 404', function (done) {
      request('http://localhost:8081/person/').get('get/00000000-0000-0000-0000-000000000000').expect(404).end(function(err, res) {
        if (err) {
          console.log(err);
          throw err;
        }
        done();
      });
    });
  });

  describe('calling trace with invalid uuid', function () {
    it('should return error code: 404', function (done) {
      request('http://localhost:8081/person/').get('trace/00000000-0000-0000-0000-000000000000').expect(404).end(function(err, res) {
        if (err) {
          console.log(err);
          throw err;
        }
        done();
      });
    });
  });

  describe('calling history with invalid uuid', function () {
    it('should return error code: 404', function (done) {
      request('http://localhost:8081/person/').get('history/00000000-0000-0000-0000-000000000000/26.02.2018').expect(404).end(function(err, res) {
        if (err) {
          console.log(err);
          throw err;
        }
        done();
      });
    });
  });

  describe('calling evolution with invalid uuid', function () {
    it('should return error code: 404', function (done) {
      request('http://localhost:8081/person/').get('evolution/00000000-0000-0000-0000-000000000000/26.02.2018').expect(404).end(function(err, res) {
        if (err) {
          console.log(err);
          throw err;
        }
        done();
      });
    });
  });

  describe('calling passivate with invalid uuid', function () {
    it('should return error code: 404', function (done) {
      request('http://localhost:8081/person/').patch('passivate/00000000-0000-0000-0000-000000000000').expect(404).end(function(err, res) {
        if (err) {
          console.log(err);
          throw err;
        }
        done();
      });
    });
  });

  describe('calling delete with invalid uuid', function () {
    it('should return error code: 404', function (done) {
      request('http://localhost:8081/person/').delete('delete/00000000-0000-0000-0000-000000000000').expect(404).end(function(err, res) {
        if (err) {
          console.log(err);
          throw err;
        }
        done();
      });
    });
  });

  describe('calling remove with invalid uuid', function () {
    it('should return error code: 404', function (done) {
      request('http://localhost:8081/person/').patch('remove/00000000-0000-0000-0000-000000000000').expect(404).end(function(err, res) {
        if (err) {
          console.log(err);
          throw err;
        }
        done();
      });
    });
  });
  
  describe('calling update with invalid uuid', function () {
    it('should return error code: 404', function (done) {

      var updateTestData = {
          "name": [{
            "validity": {
            "from": "2002-02-02T03:02:02.222Z",
            "note": "",
            "actor": {
            "reference": {
              "urn": "urn:kommune:skanderborg",
              "type": "/businessobjects/organisation"
            }
          }
        },
        "firstname": "Test",
        "middelname": "Test",
        "surname": "Test"
        }]
      };

      request('http://localhost:8081/person/').put('update/00000000-0000-0000-0000-000000000000').send(updateTestData).expect(404).end(function(err, res) {
        if (err) {
          console.log(err);
          throw err;
        }
        done();
      });
    });
  });

});

