var assert = require('chai').assert;
var _ = require('underscore');

var lookup = require('../../../models/schema/lookup');

describe('Test of model lookup', function() {

  describe('#update', function () {
    it('should return 500 - "Invalid input" when no input', function () {
      return lookup.update().then(function(result) {

        assert(false, 'should have been rejected');
      }, function(error) {
        assert.strictEqual(error.statusCode, 500);
        assert.strictEqual(error.statusMessage, 'Invalid input');
      });
    });

    it('should return 500 - "Invalid input" when input is wrong', function () {
      return lookup.update("invalid").then(function(result) {

        assert(false, 'should have been rejected');
      }, function(error) {
        assert.strictEqual(error.statusCode, 500);
        assert.strictEqual(error.statusMessage, 'Invalid input');
      });
    });

    it('should return 500 - "Tenant cannot be empty" when input has empty tenant', function () {
      var params = {tenant: '', path: '.'};
      
      return lookup.update(params).then(function(result) {

        assert(false, 'should have been rejected');
      }, function(error) {
        assert.strictEqual(error.statusCode, 500);
        assert.strictEqual(error.statusMessage, 'tenant cannot be empty');
      });
    });
    
    it('should return 500 - "Type cannot be empty" when input has empty type', function () {
      var params = {tenant: 'test', path: '.', type: ''};
      
      return lookup.update(params).then(function(result) {

        assert(false, 'should have been rejected');
      }, function(error) {
        assert.strictEqual(error.statusCode, 500);
        assert.strictEqual(error.statusMessage, 'type cannot be empty');
      });
    });
    
    it('should return 500 - "Path cannot be empty" when input has empty path', function () {
      var params = {tenant: 'test', path: ''};
      
      return lookup.update(params).then(function(result) {

        assert(false, 'should have been rejected');
      }, function(error) {
        assert.strictEqual(error.statusCode, 500);
        assert.strictEqual(error.statusMessage, 'path cannot be empty');
      });
    });

    it('should return 200 - "Default path set to <path>" with valid data and undefine type', function () {
      var params = {tenant: 'test', path: 'a,b'};
      
      return lookup.update(params).then(function(result) {

        assert.strictEqual(result.statusCode, 200);
        assert.strictEqual(result.statusMessage, 'Default path set to: ' + params.path);
      }, function(error) {
        assert(false, 'should not been rejected');
      });
    });

    it('should return 200 - "Path for <type> set to <path>" with valid data', function () {
      var params = {tenant: 'test', type: 'person', path: 'c,d'};
      
      return lookup.update(params).then(function(result) {

        assert.strictEqual(result.statusCode, 200);
        assert.strictEqual(result.statusMessage, 'Lookup data updated for type: ' + params.type);
      }, function(error) {
        assert(false, 'should not been rejected');
      });
    });

    it('should return 200 - "Path for <type> set to <path>" with valid data - update', function () {
      var params = {tenant: 'test', type: 'person', path: '7337.it,megatron.io', schema: '7337.it:businessobject.person', rules: '7337.it:hasToBeFemale,kl.dk:ageUnder(35),kl.dk:danishCitizen'};
      
      return lookup.update(params).then(function(result) {

        assert.strictEqual(result.statusCode, 200);
        assert.strictEqual(result.statusMessage, 'Lookup data updated for type: ' + params.type);
      }, function(error) {
        assert(false, 'should not been rejected');
      });
    });

  });

  describe('#get', function () {
    it('should return 500 - "Invalid input" when no input', function () {
      return lookup.get().then(function(result) {

        assert(false, 'should have been rejected');
      }, function(error) {
        assert.strictEqual(error.statusCode, 500);
        assert.strictEqual(error.statusMessage, 'Invalid input');
      });
    });

    it('should return 500 - "Invalid input" when input is wrong', function () {
      return lookup.get("invalid").then(function(result) {

        assert(false, 'should have been rejected');
      }, function(error) {
        assert.strictEqual(error.statusCode, 500);
        assert.strictEqual(error.statusMessage, 'Invalid input');
      });
    });

    it('should return 500 - "Tenant cannot be empty" when input has empty tenant', function () {
      var params = {tenant: ''};
      
      return lookup.get(params).then(function(result) {

        assert(false, 'should have been rejected');
      }, function(error) {
        assert.strictEqual(error.statusCode, 500);
        assert.strictEqual(error.statusMessage, 'tenant cannot be empty');
      });
    });
    
    it('should return 500 - "Type cannot be empty" when input has empty type', function () {
      var params = {tenant: 'test', type: ''};
      
      return lookup.get(params).then(function(result) {

        assert(false, 'should have been rejected');
      }, function(error) {
        assert.strictEqual(error.statusCode, 500);
        assert.strictEqual(error.statusMessage, 'type cannot be empty');
      });
    });
    
    it('should return 404 - "Unknown tenant', function () {
      var params = {tenant: 'unknown'};
      
      return lookup.get(params).then(function(result) {

        assert(false, 'should not be resolved');
      }, function(error) {
        assert.strictEqual(error.statusCode, 404);
        assert.strictEqual(error.statusMessage, 'No lookup data found');
      });
    });

    it('should return 200 - "Get default path', function () {
      var params = {tenant: 'test'};
      
      return lookup.get(params).then(function(result) {

        assert.strictEqual(result.statusCode, 200);
        assert.strictEqual(result.entity.path, 'a,b');
      }, function(error) {
        assert(false, 'should not be rejected');
      });
    });

    it('should return 200 - "Get specific path', function () {
      var params = {tenant: 'test', type: 'person'};
      
      return lookup.get(params).then(function(result) {

        assert.strictEqual(result.statusCode, 200);
        assert.strictEqual(result.entity.path, '7337.it,megatron.io');
      }, function(error) {
        assert(false, 'should not be rejected');
      });
    });
});

});
