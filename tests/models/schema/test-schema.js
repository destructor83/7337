var assert = require('chai').assert;
var _ = require('underscore');
var rewire = require('rewire');

var schema = require('../../../models/schema/schema');
var _schema = rewire('../../../models/schema/schema');

describe('Test of model: schema', function() {

  describe('#update', function () {
    
    it('should return 500 - "Invalid input" when no input', function () {
      return schema.update().then(function(result) {

        assert(false, 'should have been rejected');
      }, function(error) {
        assert.strictEqual(error.statusCode, 500);
        assert.strictEqual(error.statusMessage, 'Invalid input');
      });
    });

    it('should return 500 - "Invalid input" when input is wrong', function () {
      return schema.update("invalid", null).then(function(result) {

        assert(false, 'should have been rejected');
      }, function(error) {
        assert.strictEqual(error.statusCode, 500);
        assert.strictEqual(error.statusMessage, 'Invalid input');
      });
    });

    it('should return 500 - "Cannot be empty: tenant" when input has empty tenant', function () {
      var params = {tenant: '', path: '.', namespace: '', version: ''};
      
      return schema.update(params, null).then(function(result) {

        assert(false, 'should have been rejected');
      }, function(error) {
        assert.strictEqual(error.statusCode, 500);
        assert.strictEqual(error.statusMessage, 'Cannot be empty: tenant');
      });
    });
    
    it('should return 500 - "Cannot be empty: namespace" when input has empty namespace', function () {
      var params = {tenant: 'test', path: '.', namespace: '', version: ''};
      
      return schema.update(params, null).then(function(result) {

        assert(false, 'should have been rejected');
      }, function(error) {
        assert.strictEqual(error.statusCode, 500);
        assert.strictEqual(error.statusMessage, 'Cannot be empty: namespace');
      });
    });
    
    it('should return 500 - "Cannot be empty: path" when input has empty path', function () {
      var params = {tenant: 'test', namespace: '.', path: '', version: ''};
      
      return schema.update(params, null).then(function(result) {

        assert(false, 'should have been rejected');
      }, function(error) {
        assert.strictEqual(error.statusCode, 500);
        assert.strictEqual(error.statusMessage, 'Cannot be empty: path');
      });
    });

    it('should return 500 - "Cannot be empty: version" when input has empty version', function () {
      var params = {tenant: 'test', namespace: '.', path: '.', version: ''};
      
      return schema.update(params, null).then(function(result) {

        assert(false, 'should have been rejected');
      }, function(error) {
        assert.strictEqual(error.statusCode, 500);
        assert.strictEqual(error.statusMessage, 'Cannot be empty: version');
      });
    });

    it('should return 200', function () {
      var params = {tenant: 'test', namespace: '7337.eu', path: 'definitions.validity', version: '1.0.0'};
      
      var body = 
{ "id": "/definitions/validity",
  "type": "object",
  "properties": {
    "from":  { "type": "string", "format": "date-time" },
    "to":    { "type": "string", "format": "date-time" },
    "index": { "type": "string" },
    "insert":{ "type": "boolean" },
    "note":  { "type": "string" },
    "actor": { "$ref": "7337.io:definitions.actor"}
  },
  "additionalProperties": false,
  "required": ["from", "actor"]
};
      
      return schema.update(params, body).then(function(result) {

        assert.strictEqual(result.statusCode, 200);
        assert.strictEqual(result.statusMessage, "Schema: " + params.namespace + ":" + params.path + ":" + params.version + " updated by tenant: " + params.tenant);
      }, function(error) {
        assert(false, 'should not been rejected, cause: ' + error.statusMessage);
      });
    });

    it('should return 200 - "Path for <type> set to <path>" with valid data', function () {
      var params = {tenant: 'test', type: 'person', path: 'c,d'};
      
      return schema.update(params).then(function(result) {

        assert.strictEqual(result.statusCode, 200);
        assert.strictEqual(result.statusMessage, 'Path for: ' + params.type + ' set to: ' + params.path);
      }, function(error) {
        assert(false, 'should not been rejected');
      });
    });

    it('should return 200 - "Path for <type> set to <path>" with valid data - update', function () {
      var params = {tenant: 'test', type: 'person', path: '1,2'};
      
      return schema.update(params).then(function(result) {

        assert.strictEqual(result.statusCode, 200);
        assert.strictEqual(result.statusMessage, 'Path for: ' + params.type + ' set to: ' + params.path);
      }, function(error) {
        assert(false, 'should not been rejected');
      });
    });

  });

  describe('#get', function () {

    it('should return 500 - "Invalid input" when no input', function () {
      return schema.get().then(function(result) {

        assert(false, 'should have been rejected');
      }, function(error) {
        assert.strictEqual(error.statusCode, 500);
        assert.strictEqual(error.statusMessage, 'Invalid input');
      });
    });

    it('should return 500 - "Invalid input" when input is wrong', function () {
      return schema.get("invalid").then(function(result) {

        assert(false, 'should have been rejected');
      }, function(error) {
        assert.strictEqual(error.statusCode, 500);
        assert.strictEqual(error.statusMessage, 'Invalid input');
      });
    });

    it('should return 500 - "Tenant cannot be empty" when input has empty tenant', function () {
      var params = {tenant: ''};
      
      return schema.get(params).then(function(result) {

        assert(false, 'should have been rejected');
      }, function(error) {
        assert.strictEqual(error.statusCode, 500);
        assert.strictEqual(error.statusMessage, 'Invalid input');
      });
    });
    
    it('should return 500 - "Type cannot be empty" when input has empty type', function () {
      var params = {tenant: 'test', type: ''};
      
      return schema.get(params).then(function(result) {

        assert(false, 'should have been rejected');
      }, function(error) {
        assert.strictEqual(error.statusCode, 500);
        assert.strictEqual(error.statusMessage, 'Invalid input');
      });
    });
    
    it('should return 200 - "Get schema', function () {
      var params = {tenant: 'test', namespace: '7337.eu', path: 'definitions.validity', version: '1.0.0'};
      
      return schema.get(params).then(function(result) {
        assert.strictEqual(result.statusCode, 200);
        assert.strictEqual(JSON.parse(result.entity).id, '7337.eu:definitions.validity:1.0.0');
      }, function(error) {
        assert(false, 'should not be rejected');
      });
    });
  });

  describe('#getLatestVersion', function () {
    
    it('should return 2_2_1', function () {
      var entity = {version1_0_0: 1, version1_0_1: 1.01, version1_1_0: 1.1, version2_0_0: 2, version2_1_0: 2.1, version2_2_1: 2.21};
      var latest = _schema.__get__('getLatestVersion')(entity);
      assert('2_2_1', latest);
    });
  });

  describe('#getNearestVersion', function () {
    
    it('should return 1_0_1', function () {
      var entity = {version1_0_0: 1, version1_0_1: 1.01, version1_1_0: 1.1, version2_0_0: 2, version2_1_0: 2.1, version2_2_1: 2.21};
      var version = _schema.__get__('getNearestVersion')(entity, '1_0');
      assert('1_0_1', version);
    });

    it('should return 1_1_0', function () {
      var entity = {version1_0_0: 1, version1_0_1: 1.01, version1_1_0: 1.1, version1_1_1: 1.11, version2_0_0: 2, version2_1_0: 2.1, version2_2_1: 2.21};
      var version = _schema.__get__('getNearestVersion')(entity, '1_1_0');
      assert('1_1_0', version);
    });

    it('should return null', function () {
      var entity = {version1_0_0: 1, version1_0_1: 1.01, version1_1_0: 1.1, version1_1_1: 1.11, version2_0_0: 2, version2_1_0: 2.1, version2_2_1: 2.21};
      var version = _schema.__get__('getNearestVersion')(entity, '2_1_1');
      assert.isNull(version);
    });
  });
  
  describe('#list', function () {
      var params = {tenant: 'test'};
    
    it('should return all', function () {
      schema.list(params);
    });

  });
});
